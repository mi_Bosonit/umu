# Python
import os
from datetime import datetime

from pandas import DataFrame, concat, read_sql

from components.listas import meses
# Aplicacion
from components.utils import conection, mes_dia_hora, time_instant_to_date
from configuracion import tabla_bbdd

# Depuradora
edarmonitoring_last_24 = f'''SELECT id, ocb_id , time_instant,
                             cond, o2, t, turb, turbs, ph
                             FROM {tabla_bbdd}.edarmonitoring
                             WHERE time_instant > now() - INTERVAL '12' HOUR'''
# Consumo de Agua
latitudes = f'''SELECT ocb_id AS controlled_asset, description , lat, long
               FROM {tabla_bbdd}.building '''


def calc_consumo(x, y):
    if x <= y:
        return 0
    return x - y


def tratar_df(df):

    df = time_instant_to_date(df, 'time_instant', 'time_instant')
    df = mes_dia_hora(df, 'time_instant')
    df['Consumo'] = [
        calc_consumo(x, df.iloc[i - 1]['counter1'])
        for i, x in enumerate(df['counter1'])
    ]
    return df


def obtener_ultimo_mes():
    mes = datetime.now().date().month
    anio = datetime.now().date().year
    anio_anterior = anio
    anio_posterior = anio
    mes_anterior = mes - 1
    mes_posterior = mes + 1

    if mes == 1:
        mes_anterior = 12
        anio_anterior = anio_anterior - 1
    if mes == 12:
        mes_posterior = 1
        anio_posterior = anio_posterior + 1

    for localizacion in ('building-facultad-veterinaria',
                         'building-facultad-quimica',
                         'building-facultad-psicologia'):

        engine = conection()

        query = f'''SELECT ocb_id, controlled_asset,
                           max(time_instant) AS time_instant,
                           max(counter1) AS counter1
                    FROM {tabla_bbdd}.buildingmonitoring_watermeter
                    WHERE time_instant >= '{anio_anterior}-{mes_anterior}-01'
                          AND time_instant < '{anio}-{mes}-01'
                          AND controlled_asset = '{localizacion}'
                    GROUP BY ocb_id, controlled_asset '''
        df_aux = read_sql(query, engine)

        data = {
            'id': [0],
            'ocb_id': [df_aux['ocb_id'].iloc[0]],
            'time_instant': [df_aux['time_instant'].iloc[0]],
            'counter1': [df_aux['counter1'].iloc[0]],
            'area_served': [''],
            'controlled_asset': [df_aux['controlled_asset'].iloc[0]]
        }
        df = DataFrame(data)

        path = 'Data/Consumo_de_agua/Historico/'
        path_anio = path + str(anio)
        path_localizacion = f'{path_anio}/{localizacion}'
        path_mes = f'{path_localizacion}/{meses[mes - 1]}.csv'

        if os.path.exists(path_anio) == False:
            os.mkdir(path_anio)
        if os.path.exists(path_localizacion) == False:
            os.mkdir(path_localizacion)

        query = f'''SELECT * FROM {tabla_bbdd}.buildingmonitoring_watermeter
                    WHERE time_instant >= '{anio}-{mes}-01'
                          AND time_instant < '{anio_posterior}-{mes_posterior}-01'
                          AND controlled_asset = '{localizacion}' 
                    ORDER BY time_instant'''

        df_final = tratar_df(concat([df, read_sql(query, engine)]))[1:]
        df_final = df_final[df_final['Consumo'] > 0]
        df_final.sort_values(by='time_instant').to_csv(path_mes, index=False)

        engine.dispose()
