from datetime import datetime

from pandas import concat, read_csv, read_json

from components.listas import dict_controlled_asset, meses
from components.templates import boton, div_comun

# Consumo De Agua
color_boton = "#add8e6"
style = {
    "color": "#FE0804",
    "float": "left",
    "margin-right": "3px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "1rem",
}
s_aviso = {
    "margin-left": "6px",
    "margin-top": "5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "0.9rem",
    "color": "red",
}
s_aviso_normal = {
    "margin-left": "6px",
    "margin-top": "5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "0.9rem",
}
s_tiempo = {
    "margin-left": "6px",
    "margin-top": "5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "0.9rem",
}
s_aviso_depuradora = {
    "margin-left": "22%",
    "margin-top": "5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "1.1rem",
    "color": "red",
}
s_tiempo_depuradora = {
    "margin-left": "15%",
    "margin-top": "5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "1.1rem",
}
s_titulo_alerta = {
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "1.5rem",
    "text-align": "center",
}
s_boton = {
    "width": "95%",
    "height": "95%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": """Roboto, Helvetica,
                      Arial, sans-serif""",
    "font-size": "1rem",
    "border": f"0.1px solid {color_boton}",
}


def obtener_df_consumo_agua():
    lista = list(dict_controlled_asset.keys())
    df = read_csv(f"""Data/Consumo_de_agua/Historico/{
            datetime.now().year}/{lista[0]}/{
                meses[datetime.now().month - 1]}.csv""")    
    df = df[df.shape[0] - 1:df.shape[0]]

    for disp in lista[1:]:
        df_aux = read_csv(f"""Data/Consumo_de_agua/Historico/{
                datetime.now().year}/{disp}/{
                    meses[datetime.now().month - 1]}.csv""")
        df_aux = df_aux[df_aux.shape[0] - 1:df_aux.shape[0]]
        df = concat([df, df_aux])
    df["description"] = [
        dict_controlled_asset[i] for i in df["controlled_asset"]
    ]

    return df


def alertas_por_tiempo_consumo_agua(df, obj_tiempo):

    df_aux = df[df["Consumo"] == -1]
    contador = 0

    for i in range(len(df)):
        dispositivo = dict_controlled_asset[df.iloc[i]["controlled_asset"]]
        periodo_meses = datetime.now().month - int(df.iloc[i]["Mes"])
        periodo_dias = datetime.now().day - int(df.iloc[i]["Dia"])

        if periodo_meses > 0:
            periodo = f" por {periodo_meses} mes"
            if periodo_meses > 1:
                periodo = f" por {periodo_meses} meses"
            obj_tiempo.append(
                div_comun(
                    {"margin-top": "10px"},
                    "",
                    [
                        boton(
                            f"alerta_dispositivo_{dispositivo}_{id}",
                            dispositivo,
                            s_boton,
                            hidden=False,
                        ),
                        div_comun(
                            {"margin-left": "6px"},
                            "",
                            [
                                div_comun(style, "", "Sin Datos "),
                                div_comun(s_aviso, "", periodo),
                            ],
                        ),
                    ],
                ))
            continue
        elif periodo_dias > 0:
            periodo = f" por {periodo_dias} día"
            if periodo_dias > 1:
                periodo = f"por {periodo_dias} días"
            obj_tiempo.append(
                div_comun(
                    {"margin-top": "10px"},
                    "",
                    [
                        boton(
                            f"alerta_dispositivo_{dispositivo}_{id}",
                            dispositivo,
                            s_boton,
                            hidden=False,
                        ),
                        div_comun(
                            {"margin-left": "6px"},
                            "",
                            [
                                div_comun(style, "", "Sin Datos "),
                                div_comun(s_aviso, "", periodo),
                            ],
                        ),
                    ],
                ))
            continue
        else:
            df_aux.loc[contador] = df.iloc[i]
            contador += 1

    return df_aux, obj_tiempo


def alertas_por_valores_consumo_agua(df,
                                     df_valores,
                                     obj_valores,
                                     avisos=False):
    lista_de_avisos = []
    for i in range(len(df)):
        dispositivo = df.iloc[i]["description"]
        if df.iloc[i]["Consumo"] >= df_valores.iloc[i]["Max"]:
            aviso = div_comun(
                s_aviso,
                "",
                f"""Consumidos {
                    round(df.iloc[i]["Consumo"]
                          - df_valores.iloc[i]["Max"], 2)
                          } metros cúbicos por encima del límite""",
            )
            lista_de_avisos.append(([
                "Consumo Agua",
                dispositivo,
                f"""Consumidos {
                            round(df.iloc[i]["Consumo"]
                                  - df_valores.iloc[i]["Max"], 2)
                                  } metros cúbicos por encima del límite""",
            ]))
        else:
            aviso = div_comun(s_aviso_normal, "",
                              f"""Consumo dentro del límite establecido""")
        obj_valores.append(
            div_comun(
                {"margin-top": "10px"},
                "",
                [
                    boton(f"alerta_dispositivo_{dispositivo}_{id}",
                          dispositivo, s_boton),
                    aviso,
                ],
            ))

    if avisos:
        return lista_de_avisos
    return obj_valores


def consumo_agua(avisos=False):
    obj_tiempo = [div_comun(s_titulo_alerta, "", "Alertas de Tiempo")]
    obj_valores = [div_comun(s_titulo_alerta, "", "Alertas de Valores")]
    df = obtener_df_consumo_agua()
    df_valores = read_csv("Data/valores_alertas/consumo_de_agua.csv")[[
        "Dispositivo", "Max"
    ]]
    df, obj_tiempo = alertas_por_tiempo_consumo_agua(df, obj_tiempo)

    if df.shape[0] > 0:
        if avisos:
            return alertas_por_valores_consumo_agua(df, df_valores,
                                                    obj_valores, True)
        obj_valores = alertas_por_valores_consumo_agua(df, df_valores,
                                                       obj_valores)
    else:
        if avisos:
            return []
    return [obj_tiempo, obj_valores]


# Depuradora
def obtener_df_depuradora(data):

    df = read_json(data["edarmonitoring"]).sort_values("time_instant",
                                                       ascending=False)
    df = df[:1]
    df["time_instant"] = [
        datetime.fromtimestamp(i / 1000) for i in df["time_instant"]
    ]
    df["Mes"] = [i.date().month for i in df["time_instant"]]
    df["Dia"] = [i.date().day for i in df["time_instant"]]
    df["Hora"] = [i.time().hour for i in df["time_instant"]]
    return df


def alertas_por_valores_depuradora(df, df_alertas, obj_valores, avisos=False):
    alertas = {
        "PH": "ph",
        "Temperatura": "t",
        "O2": "o2",
        "Turbidez": "turbs",
        "Cond": "cond",
    }
    lista_de_avisos = []
    for i in range(df_alertas.shape[0]):
        alerta = alertas[df_alertas.iloc[i]["Alerta"]]
        min = df_alertas.iloc[i]["Min"]
        max = df_alertas.iloc[i]["Max"]
        if df[alerta].iloc[0] < min or df[alerta].iloc[0] > max:
            obj_valores.append(
                div_comun(
                    {"margin-top": "10px"},
                    "",
                    div_comun(
                        s_aviso_depuradora,
                        "",
                        f"""{df_alertas.iloc[i]["Alerta"]} = {
                            df[alerta].iloc[0]
                            } Fuera de rango ({min} - {max})""",
                    ),
                ))
            lista_de_avisos.append([
                "Depuradora",
                "Depuradora",
                f"""{df_alertas.iloc[i]["Alerta"]} = {
                        df[alerta].iloc[0]
                        } Fuera de rango ({min} - {max})""",
            ])
    if avisos:
        return lista_de_avisos
    return obj_valores


def depuradora(data):
    obj_tiempo = [div_comun(s_titulo_alerta, "", "Último Registro")]
    obj_valores = [div_comun(s_titulo_alerta, "", "Alertas")]
    df = obtener_df_depuradora(data)

    if df.shape[0] > 0:

        df_alertas = read_csv("Data/valores_alertas/depuradora.csv")

        fecha = df["time_instant"].iloc[0].date()
        hora = f"""{df["time_instant"].iloc[0].time().hour}:{
            df["time_instant"].iloc[0].time().minute
            }:{df["time_instant"].iloc[0].time().second}"""

        obj_tiempo.append(
            div_comun(
                {"margin-top": "10px"},
                "",
                div_comun(s_tiempo_depuradora, "",
                          f"""Día: {fecha} Hora: {hora} """),
            ))

        obj_valores = alertas_por_valores_depuradora(df, df_alertas,
                                                     obj_valores)

    else:
        obj_tiempo.append(div_comun({"marginLeft": "45%"}, "", "---"))
        obj_valores.append(
            div_comun({"marginLeft": "30%"}, "",
                      "Sin registros en las últimas 24 horas"))

    return [obj_tiempo, obj_valores]
