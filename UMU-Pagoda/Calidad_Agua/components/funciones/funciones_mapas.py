import plotly.graph_objects as go


def mapa(df, mode, height, size, text, zoom=13, lat=0, lon=0):
    if lat == 0:
        lat = df["lat"].iloc[0]
    if lon == 0:
        lon = df["long"].iloc[0]
    fig = go.Figure(
        go.Scattermapbox(
            lat=df["lat"],
            lon=df["long"],
            mode=mode,
            marker=go.scattermapbox.Marker(size=size, color="green"),
            text=df[text],
        )
    )
    fig.update_layout(
        hovermode="closest",
        mapbox_style="open-street-map",
        mapbox={"zoom": zoom, "center": {"lon": lon, "lat": lat}},
        autosize=True,
        height=height,
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
    )
    return fig
