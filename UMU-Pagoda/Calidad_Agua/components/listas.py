meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
]

# Consumo de agua
dict_localizacion = {
    "Facultad de Química": "building-facultad-quimica",
    "Facultad de Psicología": "building-facultad-psicologia",
    "Facultad de Veterinaria": "building-facultad-veterinaria",
}

dict_controlled_asset = {
    "building-facultad-quimica": "Facultad de Química",
    "building-facultad-psicologia": "Facultad de Psicología",
    "building-facultad-veterinaria": "Facultad de Veterinaria",
}

columns_consumo = [
    dict(id="Dispositivo", name="Dispositivo", editable=False),
    dict(id="Max", name="Max", type="numeric"),
]

style_data_conditional_consumo = [
    {
        "if": {"column_id": ["Dispositivo"], "column_id": ["Dispositivo"]},
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1rem",
        "width": "50%",
    },
    {"if": {"column_id": ["Max"], "column_id": ["Max"]}, "width": "25%"},
]

# Depuradora
columns_depuradora = [
    dict(id="Alerta", name="Alerta", editable=False),
    dict(id="Min", name="Min", type="numeric"),
    dict(id="Max", name="Max", type="numeric"),
]

style_data_conditional_depuradora = [
    {
        "if": {"column_id": ["Alerta"], "column_id": ["Alerta"]},
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1rem",
        "width": "50%",
    },
    {"if": {"column_id": ["Min"], "column_id": ["Min"]}, "width": "25%"},
    {"if": {"column_id": ["Max"], "column_id": ["Max"]}, "width": "25%"},
]
