import dash_bootstrap_components as dbc
import plotly.express as px
from dash import dash_table, dcc, html


def div_comun(style, id="", children="", hidden=False):
    return html.Div(style=style, children=children, id=id, hidden=hidden)


def boton(id, nombre, style, hidden=False, disabled=False, n_clicks=0):
    return html.Button(
        style=style,
        id=id,
        n_clicks=n_clicks,
        hidden=hidden,
        children=nombre,
        disabled=disabled,
    )


def selector(id, style, options=[], value="", placeholder=""):
    return dcc.Dropdown(
        style=style, id=id, options=options, value=value,
        placeholder=placeholder
    )


def tabla(id, df, columns, style_data_conditional):
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columns,
                id=f"tabla_{id}",
                page_size=10,
                style_table={
                    "box-sizing": "inherit",
                    "letter-spacing": "0.0001em",
                    "font-family": "Roboto, Helvetica, Arial, sans-serif",
                    "font-size": "1rem",
                },
                style_data={
                    "width": "33%",
                    "padding": "6px",
                    "align-items": "center",
                    "border-right": "0.1px solid white",
                    "border-left": "0.1px solid white",
                    "height": "50px",
                    "color": "#44535A",
                },
                style_header={
                    "padding": "6px",
                    "color": "white",
                    "background": "#4189AD",
                    "font-family": "Roboto, Helvetica, Arial, sans-serif",
                    "font-size": "1rem",
                    "height": "55px",
                    "border-botton": "0.1px solid #4189AD",
                    "border-right": "0.1px solid #4189AD",
                },
                style_cell={"textAlign": "center"},
                style_data_conditional=style_data_conditional,
                editable=True,
            )
        ]
    )


def bar(df, x, y, text="", title="", yaxis_title="", xaxis_title="",
        height=240):

    fig = px.bar(df, x=x, y=y, text=text, title=title)
    fig.update_layout(
        autosize=True,
        height=height,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        xaxis={"showgrid": False},
        yaxis={"showgrid": False},
        plot_bgcolor="white",
        margin=dict(t=10, l=0, b=0, r=0),
    )
    fig.update_traces(textposition="outside", cliponaxis=False,
                      textfont_size=14)
    return fig


# line
def line(df, x, y, title="", yaxis_title="", xaxis_title="", mode="lines"):
    fig = px.line(
        df,
        x=x,
        y=y,
        title=title,
        color_discrete_sequence=["#4189AD", "#88dd44", "#CE7B54"],
    )
    fig.update_layout(
        title={
            "font": {
                "family": "Roboto, Helvetica, Arial, sans-serif",
                "color": "black",
                "size": 25,
            }
        },
        autosize=True,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        xaxis={"showgrid": False},
        yaxis={"showgrid": False},
        plot_bgcolor="white",
        margin=dict(t=60, l=0, b=0, r=0),
    )
    fig.update_traces(mode=mode)
    return fig
