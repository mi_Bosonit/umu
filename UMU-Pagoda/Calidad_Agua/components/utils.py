# Python
from sqlalchemy import create_engine
from datetime import datetime
from pytz import timezone
# Aplicacion
from configuracion import bbdd, bbdd2


def conection():
    return create_engine(bbdd)

def conection_2():
    return create_engine(bbdd2)

def cambio_fecha_hora_utc(fecha):
    time_zone = timezone("Europe/Madrid")
    return fecha.astimezone(time_zone)


def time_instant_to_date(df, col_to, col_from):

    df[col_to] = list(
                    map(lambda x: cambio_fecha_hora_utc(
                                            x,
                                            ).replace(tzinfo=None),
                        df[col_from])
                    )
    return df


def time_instant_to_date_sin_utc(df, col_to, col_from):

    df[col_to] = list(
                    map(lambda x: datetime.fromtimestamp(
                                               x/1000
                                               ).replace(tzinfo=None),
                        df[col_from])
                       )
    return df


def mes_dia_hora(df, col_from):

    df['Mes'] = list(
                    map(lambda x: x.date().month,
                        df[col_from])
                        )

    df['Dia'] = list(
                map(lambda x: x.date().day,
                    df[col_from])
                    )

    df['Hora'] = list(
                map(lambda x: x.time(),
                    df[col_from])
                    )

    return df
