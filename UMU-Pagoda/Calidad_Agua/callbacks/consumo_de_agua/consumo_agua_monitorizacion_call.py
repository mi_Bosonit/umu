import glob
from contextlib import suppress
from datetime import datetime

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import concat, read_csv, read_json

from components.funciones.funciones_mapas import mapa
from components.listas import dict_localizacion, meses
from components.templates import bar, div_comun

s_titulo = {
    "height": "10%",
    "font-size": "1.6vmax",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

global dispositivo_csv


@callback(
    Output("div_selector", "hidden"), Input("store", "data"),
    prevent_initial_call=True
)
def sel_ho(data):
    return False


@callback(
    Output("consumo_agua_diario", "children"),
    [
        Input("consumo_agua_selector", "value"),
        Input("store", "data")
    ],
    prevent_initial_call=True,
)
def sel_ho(selector, b_data):

    obj = ""

    if selector is not None:
        global dispositivo_csv
        obj = f"No hay datos para {meses[datetime.now().month - 1]}"
        mes_csv = meses[datetime.now().month - 1]
        dispositivo_csv = dict_localizacion[selector]
        path = "Data/Consumo_de_agua/Historico/"
        path = path + f"{datetime.now().year}/{dispositivo_csv}/{mes_csv}.csv"
        df = read_csv(path)[["Mes", "Dia", "Consumo"]]
        df = df[df["Mes"] == datetime.now().month]

        if df.shape[0] > 0:
            df = df.groupby(["Dia"]).sum("Consumo")
            df.reset_index(
                level=None, drop=False, inplace=True, col_level=0,
                col_fill=["Dia"]
            )

            df["Consumo"] = df["Consumo"].apply(lambda x: round(x, 2))
            fecha = f"{datetime.now().year}-{datetime.now().month}-"
            df["Dia"] =\
                df["Dia"].apply(
                    lambda x: datetime.strptime(fecha + f"{x}", "%Y-%m-%d"))

            titulo = "Consumo de agua Diario en"
            titulo += f" {meses[datetime.now().date().month - 1]}"
            obj = [
                div_comun(s_titulo, "", titulo),
                dcc.Graph(
                    figure=bar(
                        df,
                        "Dia",
                        "Consumo",
                        df["Consumo"],
                        "",
                        "Metros Cúbicos",
                        height=250,
                    )
                ),
            ]
    return obj


@callback(
    Output("consumo_agua_mensual", "children"),
    Input("consumo_agua_diario", "children"),
    prevent_initial_call=True,
)
def sel_ho(consumo_mes):

    obj = ""

    if consumo_mes is not None:
        global dispositivo_csv
        while True:
            with suppress(Exception):
                disp_csv = dispositivo_csv
                break

        obj = ""
        path = "Data/Consumo_de_agua/Historico/"
        path += f"{datetime.now().year}/{disp_csv}"
        files = glob.glob(path + "/*.csv")
        content = [read_csv(filename, index_col=None) for filename in files]
        df_anio = concat(content)
        df_anio = df_anio.groupby(["Mes"]).sum("Consumo")
        df_anio.reset_index(
            level=None, drop=False, inplace=True, col_level=0, col_fill=["Mes"]
        )
        df_anio["Consumo"] = [int(i) for i in df_anio["Consumo"]]
        df_anio["Mes"] = [int(i) for i in df_anio["Mes"]]
        df_anio["Mes"] = [meses[i - 1] for i in df_anio["Mes"]]
        titulo = f"Consumo de agua Mensual en {datetime.now().date().year}"
        obj = div_comun(
            {},
            "",
            [
                div_comun(s_titulo, "", titulo),
                dcc.Graph(
                    figure=bar(
                        df_anio,
                        "Mes",
                        "Consumo",
                        df_anio["Consumo"],
                        "",
                        "Metros Cúbicos",
                        height=250,
                    )
                ),
            ],
        )
        return obj


@callback(
    Output("consumo_agua_mapa", "children"),
    Input("consumo_agua_diario", "children"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(consumo_mes, data):

    obj = ""
    global dispositivo_csv
    while True:
        with suppress(Exception):
            disp_csv = dispositivo_csv
            break

    if consumo_mes is not None:
        df_lat = read_json(data["latitudes"])
        df_lat = df_lat[df_lat["controlled_asset"] == disp_csv]
        obj = dcc.Graph(figure=mapa(df_lat, "markers", 510, 13, "description"))

    return obj
