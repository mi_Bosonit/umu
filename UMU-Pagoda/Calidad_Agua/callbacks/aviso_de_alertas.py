from dash import callback
from dash.dependencies import Input, Output
from pandas import read_csv

from components.funciones.funciones_alertas import (
    alertas_por_valores_depuradora, consumo_agua)
from components.queries import *
from components.utils import conection, conection_2


@callback(
    [Output("interval", "n_intervals")],
    [Input("interval", "n_intervals")],
    prevent_initial_call=True,
)
def display_page(interval):
    engine = conection()
    # consumo agua
    lista_alertas_consumo = consumo_agua(avisos=True)

    # depuradora
    df = read_sql(
        """SELECT id, ocb_id , time_instant, cond, o2, t, turb, turbs, ph
                     FROM openiotv2.edarmonitoring
                     WHERE time_instant > now() - INTERVAL '24' HOUR """,
        engine,
    )

    lista_alertas_depuradora = []

    if df.shape[0] > 0:
        df_alertas = read_csv("Data/valores_alertas/depuradora.csv")
        lista_alertas_depuradora = alertas_por_valores_depuradora(df,
                                                                  df_alertas,
                                                                  [],
                                                                  avisos=True)
    engine.dispose()
    lista_total = lista_alertas_consumo + lista_alertas_depuradora

    # Guardar Alertas en BBDD Iot 
    engine = conection_2()
    engine.execute("""DELETE FROM openiotv2.water_warnings """)    
    
    if len(lista_total) > 0:
        
        for alerta in lista_total:
            alerta[2] = alerta[2].replace("%", "")
            engine.execute(f"""INSERT INTO openiotv2.water_warnings
                           (tipo_Alerta, localizacion, alerta)
                    VALUES ('{alerta[0]}', '{alerta[1]}', '{alerta[2]}') """)
    engine.dispose()
    return [0]
