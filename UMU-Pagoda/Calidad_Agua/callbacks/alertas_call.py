from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import DataFrame, read_json

from components.funciones.funciones_alertas import consumo_agua, depuradora
from components.funciones.funciones_mapas import mapa
from components.listas import dict_controlled_asset


# Funcion para callbacks de paginas de Alertas
def alertas(id):

    if id in ("consumo_agua",):
        # Alertas
        @callback(
            [
                Output(f"alertas_{id}_tiempo", "children"),
                Output(f"alertas_{id}_distance", "children"),
            ],
            Input("store", "data"),
            State("store", "data"),
        )
        def sel_hora(click, data):
            return consumo_agua()

        # Mapa
        @callback(
            Output(f"mapa_alertas_{id}", "children"),
            Input(f"alertas_{id}_tiempo", "children"),
            State("store", "data"),
            prevent_initial_call=True,
        )
        def sel_ho(load, data):
            lista = list(dict_controlled_asset.keys())
            df = DataFrame(columns=["Dispositivo", "lat", "long"])
            df_lat = read_json(data["latitudes"])

            for i in range(df_lat.shape[0]):
                if df_lat.iloc[i]["controlled_asset"] in lista:
                    df.loc[i] = [
                        df_lat.iloc[i]["description"],
                        df_lat.iloc[i]["lat"],
                        df_lat.iloc[i]["long"],
                    ]
                    continue

            return dcc.Graph(figure=mapa(df, "markers", 250, 13,
                             "Dispositivo"))

    if id in ("depuradora",):

        # Alertas
        @callback(
            [
                Output(f"alertas_{id}_tiempo", "children"),
                Output(f"alertas_{id}_distance", "children"),
            ],
            Input("store", "data"),
            State("store", "data"),
        )
        def sel_hora(click, data):
            return depuradora(data)

        # Mapa
        @callback(
            Output(f"mapa_alertas_{id}", "children"),
            Input(f"alertas_{id}_tiempo", "children"),
            prevent_initial_call=True,
        )
        def sel_ho(load):
            df = DataFrame(
                {"description": ["Depuradora"],
                 "lat": [38.019514],
                 "long": [-1.170324]}
            )
            return dcc.Graph(figure=mapa(df, "markers", 250, 13,
                                         "description"))
