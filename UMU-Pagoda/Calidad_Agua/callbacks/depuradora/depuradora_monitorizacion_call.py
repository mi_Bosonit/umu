from contextlib import suppress

import pandasql as ps
from dash import callback, dcc, html
from dash.dependencies import Input, Output, State
from pandas import read_json

from components.templates import boton, line, div_comun
from components.utils import time_instant_to_date_sin_utc

s_boton = {
    "width": "100%",
    "height": "100%",
    "border-radius": "20px",
    "color": "white",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_boton_ph = {
    "background": "#2FA218",
    "border": "0.1px solid black",
    "font-size": "4vmax",
}
s_boton_temp = {
    "background": "#FF4500",
    "border": "0.1px solid black",
    "font-size": "4vmax",
}
s_boton_o2 = {
    "background": "#6CC6B9",
    "border": "0.1px solid black",
    "font-size": "4vmax",
}
s_boton_cond = {
    "background": "#3689F1",
    "border": "0.1px solid black",
    "font-size": "4vmax",
}
s_boton_turb = {
    "background": "#EA598C",
    "border": "0.1px solid black",
    "font-size": "3.8vmax",
}
s_boton_ph.update(s_boton)
s_boton_temp.update(s_boton)
s_boton_o2.update(s_boton)
s_boton_cond.update(s_boton)
s_boton_turb.update(s_boton)
s_boton_reg = {
    "width": "130%",
    "height": "100%",
    "padding": "2px 2px 0px 2px",
    "border-radius": "22% 22% 22% 22%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "2.5vmax",
    "border": "0.001px solid lightgrey",
}


@callback(
    [
        Output("div_boton_ph", "children"),
        Output("div_boton_temperatura", "children"),
        Output("div_boton_o2", "children"),
        Output("div_boton_cond", "children"),
        Output("div_boton_turbidez", "children"),
        Output("div_ultimo_registro", "children"),
    ],
    Input("store", "data"),
    State("store", "data"),
    prevent_initial_call=True,
)
def display_page(store, data):

    df_monitorizacion = read_json(data["edarmonitoring_last_24"]).sort_values(
        "time_instant", ascending=False)

    if df_monitorizacion.shape[0] > 0:

        df = time_instant_to_date_sin_utc(df_monitorizacion[0:1].copy(),
                                          "time_instant", "time_instant")

        df["time_instant"] =\
            df["time_instant"].apply(lambda x: str(x).split(".")[0])

        fecha = df["time_instant"].iloc[0].split(" ")[0]
        hora = df["time_instant"].iloc[0].split(" ")[1].split(".")[0]
        df = df.iloc[0]

        return [
            html.Abbr(boton("boton_ph", f'{df["ph"]}', s_boton_ph, n_clicks=1),
                      title="Ph"),
            html.Abbr(
                boton("boton_temperatura", f'{df["t"]}', s_boton_temp),
                title="Temperatura (ºC)",
            ),
            html.Abbr(
                boton("boton_o2", f'{df["o2"]}', s_boton_o2),
                title="Oxígeno Disuelto (mg/L)",
            ),
            html.Abbr(
                boton("boton_cond", f'{df["cond"]}', s_boton_cond),
                title="Conductividad Eléctrica (µS/cm)",
            ),
            html.Abbr(
                boton("boton_turbidez", f'{df["turbs"]}', s_boton_turb),
                title="Turbidez Sólidos en Suspensión (mg/L)",
            ),
            boton("boton_ultimo_registro", f"{fecha} {hora}", s_boton_reg),
        ]

    return ['', '', '', '', '', '']


@callback(
    [
        Output("div_grafico", "children"),
        Output("div_boton_ph", "n_clicks"),
        Output("div_boton_temperatura", "n_clicks"),
        Output("div_boton_o2", "n_clicks"),
        Output("div_boton_cond", "n_clicks"),
        Output("div_boton_turbidez", "n_clicks"),
        Output("div_boton_ph", "style"),
        Output("div_boton_temperatura", "style"),
        Output("div_boton_o2", "style"),
        Output("div_boton_cond", "style"),
        Output("div_boton_turbidez", "style"),
    ],
    [
        Input("div_boton_ph", "n_clicks"),
        Input("div_boton_temperatura", "n_clicks"),
        Input("div_boton_o2", "n_clicks"),
        Input("div_boton_cond", "n_clicks"),
        Input("div_boton_turbidez", "n_clicks"),
        Input("div_boton_ph", "children"),
    ],
    [
        State("div_boton_ph", "style"),
        State("div_boton_temperatura", "style"),
        State("div_boton_o2", "style"),
        State("div_boton_cond", "style"),
        State("div_boton_turbidez", "style"),
        State("store", "data")
    ],
    prevent_initial_call=True,
)
def display_page(
    bot_ph,
    bot_temp,
    bot_o2,
    bot_cond,
    bot_turb,
    bot_data,
    ph_style,
    temp_style,
    o2_style,
    cond_style,
    turb_style,
    data
):

    if [bot_ph, bot_temp, bot_o2, bot_cond, bot_turb] ==\
           [None, None, None, None, None]:
        bot_ph = 1

    df = read_json(data["edarmonitoring_last_24"]).sort_values(
        "time_instant", ascending=False)

    if df.shape[0] > 0:

        df = time_instant_to_date_sin_utc(df, "time_instant", "time_instant")
        df["Dia"] = df["time_instant"].apply(lambda x: x.date().day)
        df["Hora"] = df["time_instant"].apply(lambda x: x.time().hour)

        ph_style["border"] = "1px solid white"
        temp_style["border"] = "1px solid white"
        o2_style["border"] = "1px solid white"
        cond_style["border"] = "1px solid white"
        turb_style["border"] = "1px solid white"
        columna = ""
        lista_stados = [ph_style, temp_style, o2_style, cond_style, turb_style]
        columnas_query = ["ph", "t", "o2", "cond", "turbs"]
        columnas = ["Ph", "Temperatura", "O2", "Cond", "Turbs"]

        for i, bot in enumerate([bot_ph, bot_temp, bot_o2, bot_cond,
                                 bot_turb]):
            if bot:
                lista_stados[i]["border"] = "2.5px solid black"
                columna = columnas[i]
                query = f"""SELECT Dia, Hora, time_instant,
                                max({columnas_query[i]}) AS '{columna}'
                            FROM df
                            GROUP BY Dia, Hora"""
                df = ps.sqldf(query, locals())
                break

        columnas = {
            "Ph": "Ph",
            "Temperatura": "Temperatura (ºC)",
            "O2": "Oxígeno Disuelto (mg/L)",
            "Cond": "Conductividad Eléctrica (µS/cm)",
            "Turbs": "Turbidez Sólidos en Suspensión (mg/L)",
        }
        titulo = "Valores máximos recogidos en las últimas 12 horas"

        return [
            dcc.Graph(figure=line(
                df,
                "time_instant",
                columna,
                titulo,
                columnas[columna],
                mode="lines + markers",
            )),
            0,
            0,
            0,
            0,
            0,
            ph_style,
            temp_style,
            o2_style,
            cond_style,
            turb_style,
        ]

    return [
        div_comun(
            {
                'position': 'absolute',
                'top': '30%',
                'left': '15%',
                "font-size": "2vmax",
            }, '', 'No hay registros en la últimas 12 horas'),
        0,
        0,
        0,
        0,
        0,
        ph_style,
        temp_style,
        o2_style,
        cond_style,
        turb_style,
    ]
