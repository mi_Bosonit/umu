color_boton = "#add8e6"
s_loading = {"position": "absolute", "top": "20%", "left": "40%"}
s_panel_principal = {"height": "95%"}
s_selector = {
    "margin-top": "1%",
    "margin-left": "2%",
    "width": "20%",
    "height": "6.5%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.4vmax",
    "background": color_boton,
}
s_panel_graficos = {"margin-left": "2%", "margin-top": "1%", "width": "98%"}
s_panel_bar = {"float": "left", "width": "65%"}
s_diario = {"height": "50%", "width": "100%"}
s_mensual = {"margin-top": "2%", "height": "50%", "width": "100%"}
s_mapa = {"float": "left", "margin-left": "3%", "height": "80%",
          "width": "26%"}
