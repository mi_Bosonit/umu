color_boton = "#add8e6"
s_div_principal = {"margin-top": "2%", "width": "100%", "height": "100%"}
# Titulos
s_div_titulos = {
    "margin-left": "15%",
    "width": "44%",
    "height": "7%",
    "background": "grey",
}
s_div_alerta = {"float": "left", "width": "63%"}
s_alerta = {"color": "white", "margin-left": "20%"}
s_div_min = {"float": "left", "width": "5%", "color": "white"}
s_div_actualizar = {"float": "left", "margin-left": "11%", "color": "white"}

# Columnas
s_div_columnas = {"margin-left": "13%", "width": "45%"}
s_div_col_alerta = {"float": "left", "width": "50%"}
s_div_col_min = {"float": "left", "width": "25%"}
s_div_editar = {"float": "left", "width": "25%"}

s_obj = {
    "margin-top": "4px",
    "margin-left": "15%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}
s_obj2 = {
    "margin-top": "4px",
    "margin-left": "68%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}
s_obj3 = {"margin-top": "3.8px", "width": "200px", "margin-left": "45%"}
s_bot = {"border": "0px", "background": "white"}

# Pop Up
s_div_pop_up = {
    "position": "absolute",
    "left": "500px",
    "top": "200px",
    "height": "210px",
    "width": "350px",
    "border-radius": "40px",
    "background": "white",
    "border": "1px solid grey",
}
s_cambiar_umbral = {
    "margin-left": "20px",
    "color": "blue",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}
s_localizacion = {
    "margin-left": "15px",
    "margin-top": "40px",
    "color": "black",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}

s_selector = {
    "margin-left": "10px",
    "margin-top": "-15px",
    "width": "230px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "background": color_boton,
}
s_aceptar = {
    "float": "left",
    "margin-left": "50%",
    "margin-top": "30px",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": "1px solid #8380F7",
}
s_cancelar = {
    "float": "left",
    "margin-left": "10px",
    "margin-top": "30px",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": "1px solid #8380F7",
}
