s_panel_principal = {"margin-left": "4%", "width": "96%", "height": "100%"}
s_titulo = {
    "margin-left": "30%",
    "margin-top": "2%",
    "width": "50%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "2.3vmax",
}
s_panel_botones = {"margin-left": "3%", "margin-top": "2%", "height": "25%"}
s_panel_kpi = {
    "float": "left",
    "height": "100%",
    "width": "15%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.8vmax",
}
s_titulo_kpi = {"text-align": "center"}
s_titulo_ult_reg = {"margin-left": "26%", "width": "100%"}
s_boton = {"float": "left", "height": "100%", "width": "95%",
           "border-radius": "25px"}
s_div_grafico = {"margin-top": "6%", "height": "20%", "width": "100%"}
