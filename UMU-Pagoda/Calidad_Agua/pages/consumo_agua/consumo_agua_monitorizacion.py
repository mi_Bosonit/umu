import callbacks.consumo_de_agua.consumo_agua_monitorizacion_call
from assets.estilos.css_monitorizacion_consumo_agua import *
from components.templates import div_comun, selector

layout = [
    div_comun(
        s_panel_principal,
        "",
        [
            div_comun(
                s_selector,
                "div_selector",
                selector(
                    "consumo_agua_selector",
                    {},
                    options=[
                        "Facultad de Psicología",
                        "Facultad de Química",
                        "Facultad de Veterinaria",
                    ],
                    value="Facultad de Psicología",
                ),
            ),
            div_comun(
                s_panel_graficos,
                "",
                [
                    div_comun(
                        s_panel_bar,
                        "",
                        [
                            div_comun(s_diario, "consumo_agua_diario"),
                            div_comun(s_mensual, "consumo_agua_mensual"),
                        ],
                    ),
                    div_comun(s_mapa, "consumo_agua_mapa"),
                ],
            ),
        ],
    )
]
