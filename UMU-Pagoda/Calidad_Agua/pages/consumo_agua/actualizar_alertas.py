from components.funciones.funciones_actualizar_alertas import \
    layout_actualizar_alertas
from components.listas import columns_consumo, style_data_conditional_consumo

layout = layout_actualizar_alertas(
    "consumo_de_agua", columns_consumo, style_data_conditional_consumo
)
