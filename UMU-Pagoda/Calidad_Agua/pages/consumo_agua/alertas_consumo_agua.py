from callbacks.alertas_call import alertas
from components.templates import div_comun

s_panel_alertas = {
    "float": "left",
    "margin-left": "2%",
    "margin-top": "2%",
    "width": "50%",
}

s_div_alerta = {
    "float": "left",
    "margin-top": "2%",
    "margin-left": "2%",
    "width": "48%",
    "height": "100%",
    "background": "#F1F5F7",
    "border-radius": "35px 80px 7px 7px",
}

s_div_mapa = {
    "float": "left",
    "margin-left": "2%",
    "margin-top": "2.5%",
    "width": "40%",
    "height": "48%",
    "background": "#F1F5F7",
    "border-radius": "35px",
}

s_mapa = {
    "margin-left": "5%",
    "margin-top": "5%",
    "width": "90%",
    "height": "50vmax%"}

layout = [
    div_comun(
        s_panel_alertas,
        "",
        [
            div_comun(s_div_alerta, "alertas_consumo_agua_tiempo"),
            div_comun(s_div_alerta, "alertas_consumo_agua_distance"),
        ],
    ),
    div_comun(s_div_mapa, "", div_comun(s_mapa, "mapa_alertas_consumo_agua")),
]

alertas("consumo_agua")
