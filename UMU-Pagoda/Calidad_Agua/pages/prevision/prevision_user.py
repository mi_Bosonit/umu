from dash import dcc

from components.templates import div_comun, selector
from pages.prevision.prevision_css import *
from pages.prevision.prevision_user_call import *

id_p = "calidad_agua_prev_user"

layout = div_comun(
    s_panel_general_user,
    "",
    [
        div_comun(s_div_titulo_user, "", "Modelos"),
        div_comun(
            s_div_selector,
            "",
            div_comun(
                s_selector,
                "",
                selector(
                    f"selector_modelos_{id_p}", {},
                    placeholder="Seleccione modelo"
                ),
            ),
        ),
        div_comun(
            s_loading,
            "",
            dcc.Loading(
                id="loading-2",
                type="default",
                fullscreen=False,
                children=div_comun(s_div_prevision, f"user_loading_{id_p}"),
            ),
        ),
        div_comun(s_div_prevision, f"prevision_user_{id_p}"),
        div_comun(s_div_fiabilidad, f"fiabilidad_user_{id_p}"),
    ],
)
