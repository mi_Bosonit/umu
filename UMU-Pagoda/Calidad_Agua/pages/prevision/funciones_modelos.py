import json
from datetime import datetime, timedelta

import pandas as pd
import requests

from components.utils import conection
from configuracion import URL, tabla_bbdd
from pages.prevision.listas import dispositivos


def guardar_modelo(model_id, response_json):
    with open(f"Data/modelos/json/{model_id}.json", "w") as file:
        json.dump(response_json, file)


def comprobar_modelo(url):
    result = False
    try:
        response = requests.get(url, verify='PagodaCA.crt').json()
        if 'detail' not in response:
            result = response
    except Exception:
        pass
    return result


def obtener_info_df():
    engine = conection()
    info_df = pd.read_sql(
        f""" SELECT table_name, column_name
             FROM information_schema.columns
             WHERE table_schema = '{tabla_bbdd}'
             GROUP BY 1, 2
             ORDER BY 1 """,
        engine,
    )
    engine.dispose()
    return info_df


def obtener_supervisado(response):
    varstrings = response.keys()
    info_df = obtener_info_df()
    variables = {}
    for varstring in varstrings:
        column = varstring.rsplit("_", 1)[0]
        variables[varstring] =\
            info_df[
                info_df["column_name"] == column
                ]['table_name'].iloc[0]
    return {"variables": variables, "type": "supervised"}


# usada en prevision_admin_call
def obtener_modelo(model_id):
    respuesta = ""
    sm_url = URL + f"/supervised/training/{model_id}/"
    response = comprobar_modelo(sm_url)

    if response:
        response_json = obtener_supervisado(response)
        respuesta = 'Supervisado'
        guardar_modelo(model_id, response_json)
    else:
        ts_url = URL + f"/time_series/training/{model_id}/"
        response = comprobar_modelo(ts_url)

        if response:
            response_json = {"type": "time_series"}
            respuesta = "Serie Temporal"
            guardar_modelo(model_id, response_json)
        else:
            respuesta = "Modelo no encontrado en IAAAS"

    return respuesta


# usadas en prevision_user_call
def solicitar_prediccion(data, id_modelo):
    url = URL + f"""/supervised/training/{id_modelo}/?"""
    engine = conection()

    for x in data["variables"].keys():
        columna = "_".join(x.split("_")[:-1])
        dispositivo = x.split("_")[-1]
        valor = pd.read_sql(
            f"""SELECT {columna}
                FROM {tabla_bbdd}.{data['variables'][x]}
                WHERE ocb_id = '{dispositivo}'
                ORDER BY time_instant DESC LIMIT 1""",
            engine,
        )[columna].iloc[0]
        url = url + f"{x}={valor}&"

    engine.dispose()
    return requests.get(url, verify='PagodaCA.crt').json()


def sm_get_pred_supervised(data, id_modelo):

    response = solicitar_prediccion(data, id_modelo)
    variable =\
        " ".join("_".join(response["variable"].split("_")[:-1]).split("_"))
    dispositivo = dispositivos[response["variable"].split("_")[-1:][0]]
    prediccion = round(response["predicted_value"], 2)

    url = URL + f"/prediction/?training_id={id_modelo}"
    response = requests.get(url, verify='PagodaCA.crt').json()
    fecha_prediccion = response["results"][-1]["predicted_date"]
    fecha_prediccion = " ".join(fecha_prediccion.split("T")).split('.')[0]

    response_df = pd.DataFrame(
        columns=["Fecha Predicción", "Variable", "Predicción", "Localización"],
        data=[[fecha_prediccion, variable, prediccion, dispositivo]],
    )

    return response_df


def obtener_historico(id_modelo):
    engine = conection()
    url = URL + f"/prediction/?training_id={id_modelo}"

    response = requests.get(url, verify='PagodaCA.crt').json()
    df = pd.DataFrame(columns=["Fecha", "Real", "Predicción"])

    for i, result in enumerate(response["results"]):
        fecha = " ".join(result["predicted_date"].split("T"))
        df.loc[i] =\
            [fecha, result["observed_value"], result["predicted_value"]]

    df_fiab = df.dropna().drop_duplicates(["Fecha"])
    df_fiab = df_fiab.sort_values(by=["Fecha"])
    df_fiab = df_fiab[(df_fiab["Fecha"] < str(datetime.now()))]
    df_fiab['Real'] = df_fiab['Real'].astype('float')

    engine.dispose()
    return df_fiab


# Usadas en interval_modelos
def crear_historico_supervisado(data, id_modelo):
    solicitar_prediccion(data, id_modelo)


def crear_historico_time_series(modelo, freq="60T", hours=12):    
   
    df_time_series = pd.DataFrame(columns=[
        "predicted_date", "predicted_value", "variable", "localizacion"
    ])
    
    start = f"{datetime.now().date()} {datetime.now().hour}:00:00"
    end = datetime.now() + timedelta(hours=hours)
    ts_url = URL + f"/time_series/training/{modelo}/"
    date_range = pd.date_range(start=start, end=end, freq=freq)
    response_df = pd.DataFrame(
        columns=["predicted_date", "predicted_value", "variable"])

    for time_instant in date_range:
        header = {"date": time_instant.strftime("%Y-%m-%dT%H:%M:%S")}
        response_json = requests.get(ts_url, header,
                                     verify='PagodaCA.crt').json()
        response_df = pd.concat(
            [response_df, pd.DataFrame(response_json, index=[0])],
            ignore_index=True)

    response_df["predicted_date"] =\
        pd.to_datetime(response_df["predicted_date"])
    response_df["localizacion"] = [
        dispositivos[variable.split("_")[-1:][0]]
        for variable in response_df["variable"]
    ]
    response_df["variable"] = [
        " ".join(variable.split("_")[:-1]).capitalize()
        for variable in response_df["variable"]
    ]
    response_df = pd.concat([df_time_series[:-1], response_df])
    response_df.to_csv(f"Data/modelos/previsiones_time_series/{modelo}.csv")
