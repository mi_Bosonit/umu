from dash import html

import callbacks.depuradora.depuradora_monitorizacion_call
from assets.estilos.css_monitorizacion_depuradora import *
from components.templates import div_comun

layout = [
    div_comun(
        s_panel_principal,
        "",
        [
            div_comun(s_titulo, "", html.P("Último valor recogido")),
            div_comun(
                s_panel_botones,
                "",
                [
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_kpi, "", "Ph"),
                            div_comun(s_boton, "div_boton_ph"),
                        ],
                    ),
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_kpi, "", "Temperatura"),
                            div_comun(s_boton, "div_boton_temperatura"),
                        ],
                    ),
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_kpi, "", "O2"),
                            div_comun(s_boton, "div_boton_o2"),
                        ],
                    ),
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_kpi, "", "Conductividad"),
                            div_comun(s_boton, "div_boton_cond"),
                        ],
                    ),
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_kpi, "", "Turbidez"),
                            div_comun(s_boton, "div_boton_turbidez"),
                        ],
                    ),
                    div_comun(
                        s_panel_kpi,
                        "",
                        [
                            div_comun(s_titulo_ult_reg, "", "Último Reg."),
                            div_comun(s_boton, "div_ultimo_registro"),
                        ],
                    ),
                ],
            ),
            div_comun(s_div_grafico, "div_grafico"),
        ],
    )
]
