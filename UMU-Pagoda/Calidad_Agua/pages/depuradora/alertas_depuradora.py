from callbacks.alertas_call import alertas
from components.templates import div_comun

s_panel_general = {
    "width": "94%",
    "height": "95%",
    "margin-left": "2%",
    "margin-top": "2%",
}

s_panel_alertas = {
    "float": "left",
    "width": "52%",
    "background": "#F1F5F7"}

s_div_tiempo = {
    "float": "left",
    "margin-top": "2%",
    "width": "28%",
    "height": "55%"}

s_div_alerta = {
    "float": "left",
    "margin-top": "2%",
    "width": "72%"}

s_panel_mapa = {
    "float": "left",
    "margin-left": "2%",
    "width": "45%",
    "height": "50%",
    "background": "#F1F5F7",
    "border-radius": "35px",
}

s_div_mapa = {
    "margin-left": "5%",
    "margin-top": "2%",
    "width": "92%",
    "height": "45vmax%",
}

layout = [
    div_comun(
        s_panel_general,
        "",
        [
            div_comun(
                s_panel_alertas,
                "",
                [
                    div_comun(s_div_tiempo, "alertas_depuradora_tiempo"),
                    div_comun(s_div_alerta, "alertas_depuradora_distance"),
                ],
            ),
            div_comun(
                s_panel_mapa,
                "",
                div_comun(s_div_mapa, "mapa_alertas_depuradora")
            ),
        ],
    )
]

alertas("depuradora")
