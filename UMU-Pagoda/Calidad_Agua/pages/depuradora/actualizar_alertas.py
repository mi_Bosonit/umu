from components.funciones.funciones_actualizar_alertas import \
    layout_actualizar_alertas
from components.listas import (columns_depuradora,
                               style_data_conditional_depuradora)

layout = layout_actualizar_alertas(
    "depuradora", columns_depuradora, style_data_conditional_depuradora
)
