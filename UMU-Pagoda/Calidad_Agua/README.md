# Caso de uso Digitalización de la red de abastecimiento y saneamiento del campus

## Introducción

En este caso de uso, se ha pretendido lograr diferentes objetivos:
* Obtener una gestión eficiente del consumo de agua. Para conseguir este objetivo, se ha hecho uso de la información obtenida de los analizadores de consumo desplegados en diferentes edificios.
* Monitorizar la calidad del agua a la salida de la depuradora.



## Despliegue


## Funcionamiento

#### Interfaz para monitorización
La monitorización de datos para esta vertical se divide en dos partes: monitorización de niveles de la depuradora y monitorización del consumo de agua.

__El panel de monitorización de la calidad del agua a la salida de la depuradora__ (Ilustración 1) se construyó para ofrecer información útil y sencilla sobre los parámetros de la calidad del agua. En él se grafican las variables más importantes para tener una lectura sencilla e intuitiva sobre su calidad. 

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/depuradora_monitorizacion 

Ilustración 1
![image.png](assets/imagenes/image.png)

Haciendo click en los diferentes indicadores el gráfico inferior mostrará los valores máximos para ese indicador en las últimas 12 horas.

__Se incluyen dos paneles auxiliares.__ Uno para poder configurar alertas por el usuario y dar aviso ante valores no deseados en la calidad del agua (Ilustración 2) y otro para poder monitorizar estas alertas (Ilustración 3).

Al panel para configurar las alertas accederemos a través de la siguiente ruta

http://localhost:8050/dash/actualizar_alertas_depuradora

Ilustración 2
![image2.png](assets/imagenes/image2.png)

Al panel para monitorizar las alertas accederemos a través de la siguiente ruta

http://localhost:8050/dash/alertas_depuradora

Ilustración 3
![image3.png](assets/imagenes/image3.png)

__El panel de monitorización de consumo__ (Ilustración 4) se encarga de analizar y mostrar información sobre el consumo de agua por edificio (que podrá ser seleccionado por el usuario). En éste se incluyen gráficas de consumo acumulado por mes (año actual) y por día (durante el mes en curso) del consumo total de agua.

Para acceder a este panel usaremos la ruta

http://localhost:8050/dash/consumo_de_agua_monitorizacion

Ilustración 4
![image4.png](assets/imagenes/image4.png)

__Al igual que con la depuradora, también se incluyen dos paneles auxiliares.__ Uno para poder configurar alertas por el usuario y dar aviso ante valores no deseados en consumo (Ilustración 5) y otro para poder monitorizar estas alertas (Ilustración 6).

Ilustración 5
![image5.png](assets/imagenes/image5.png)

Ilustración 6
![image6.png](assets/imagenes/image6.png)


#### Interfaz para predicción
En este caso tambien disponemos de 2 paneles.

__Al primer panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_admin

Desde este panel podemos importar los modelos creados en la Iaaas (Ilustración 7)

Ilustracion 7
![image7.png](assets/imagenes/image7.png)

__Al segundo panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_user

Desde este panel podemos monitorizar los modelos que hayamos importado (Ilustración 8)

Ilustración 8
![image8.png](assets/imagenes/image8.png)


