from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

import pages
from components.queries import *
from components.utils import conection
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/depuradora_monitorizacion":
        pages.depuradora.depuradora_monitorizacion.layout,
    f"/{raiz}/alertas_depuradora": pages.depuradora.alertas_depuradora.layout,
    f"/{raiz}/actualizar_alertas_depuradora":
        pages.depuradora.actualizar_alertas.layout,
    f"/{raiz}/consumo_de_agua_monitorizacion":
        pages.consumo_agua.consumo_agua_monitorizacion.layout,
    f"/{raiz}/alertas_consumo_agua":
        pages.consumo_agua.alertas_consumo_agua.layout,
    f"/{raiz}/actualizar_alertas_consumo":
        pages.consumo_agua.actualizar_alertas.layout,
    f"/{raiz}/prevision_admin": pages.prevision.prevision_admin.layout,
    f"/{raiz}/prevision_user": pages.prevision.prevision_user.layout,
}


# Update page content
@callback(Output("page_content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname in lista_de_paginas:
        return lista_de_paginas[pathname]


# Guardar query en Store
@callback(
    [Output("store", "data"), Output(f"loading_consumo_agua", "children")],
    Input("url", "pathname"),
)
def guardar_datos(pathname):
    data = {}
    engine = conection()
    if pathname.split("_")[0] in (f"/{raiz}/depuradora",):
        data["edarmonitoring_last_24"] = read_sql(
            edarmonitoring_last_24, engine
        ).to_json(orient="columns")
    if pathname in (f"/{raiz}/consumo_de_agua_monitorizacion", f"/{raiz}/alertas_consumo_agua"):        
        data["latitudes"] = read_sql(latitudes,
                                     engine
                                     ).to_json(orient="columns")    
    if pathname in (f"/{raiz}/alertas_depuradora",):
        data["edarmonitoring"] = read_sql(edarmonitoring_last_24,
                                          engine).to_json(
            orient="columns"
        )
    engine.dispose()
    return [data, ""]
