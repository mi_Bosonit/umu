'''
callbacks de app.py
'''
from datetime import datetime

from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

import pages
from components.queries import co2, people
from components.utils import conection
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/monitorizacion_co2":
    pages.monitorizacion_co2.layout,
    f"/{raiz}/monitorizacion_co2_alertas":
    pages.monitorizacion_co2_alertas.layout,
    f"/{raiz}/monitorizacion_co2_actualizar_alertas":
    pages.monitorizacion_co2_actualizar_alertas.layout,
    f"/{raiz}/monitorizacion_usuarios":
    pages.monitorizacion_usuarios.layout,
    f"/{raiz}/monitorizacion_usuarios_actualizar_alertas":
    pages.monitorizacion_usuarios_actualizar_alertas.layout,
    f"/{raiz}/monitorizacion_usuarios_alertas":
    pages.monitorizacion_usuarios_alertas.layout,    
    f"/{raiz}/prevision_co2":
    pages.prevision_co2.layout,
    f"/{raiz}/monitorizacion_modelos":
    pages.monitorizacion_modelos.layout,
    f"/{raiz}/monitorizacion_modelos_admin":
    pages.monitorizacion_modelos_admin.layout
}


# Update page content
@callback(Output('page_content', 'children'), Input('url', 'pathname'))
def display_page(pathname):
    pagina = ''
    if pathname in lista_de_paginas:
        pagina = lista_de_paginas.get(pathname)
    return pagina


# Guardar query en Store
@callback(
    [Output('store', 'data'),
     Output('loading_intervertical', 'children')], Input('url', 'pathname'))
def guardar_datos(pathname):
    data = {}
    engine = conection()

    if pathname in (f"/{raiz}/monitorizacion_co2",
                    f"/{raiz}/monitorizacion_co2_alertas"):
        data['co2'] = read_sql(co2, engine).to_json(orient='columns')
    if pathname in (f"/{raiz}/monitorizacion_usuarios",
                    f"/{raiz}/monitorizacion_usuarios_alertas"):

        df_people = read_sql(people, engine)

        if df_people.shape[0] > 0:
            exceso = df_people.iloc[0]['people_exited']
            df_people['people_exited'] = df_people['people_exited'] - exceso
            df_people['ocupacion'] = df_people['people_entered'] - df_people[
                'people_exited']
        else:
            df_people.loc[0] = [datetime.now().date(), 0, 0]
            df_people['ocupacion'] = 0

        data['people'] = df_people.to_json(orient='columns')
    engine.dispose()
    return [data, '']
