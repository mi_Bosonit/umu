from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

from components.funciones_monitorizacion_alertas import alertas_valores
from components.utils import conection, conection_2


@callback([Output('interval', 'n_intervals')],
          [Input('interval', 'n_intervals')],
          prevent_initial_call=True)
def display_page(interval):

    engine = conection()
    # Calidad aire
    df = read_sql(
        '''SELECT DISTINCT ON (ocb_id) ocb_id as description,
                            time_instant, controlled_asset,
                            area_served AS "Aula",
                            temperature AS "Temperatura",
                            relative_humidity AS "Humedad Relativa",
                            barometric_pressure AS "Presión Atmosférica",
                            co2 AS "Co2"
           FROM openiotv2.ambientmonitoring_cometsensor
           WHERE time_instant > Now() - '7 hours'::interval
           AND controlled_asset = 'building-biblioteca-general'
           ORDER BY ocb_id, time_instant DESC''', engine)

    lista_alertas_aire = []

    if df.shape[0] > 0:
        _, lista_alertas_aire = alertas_valores('calidad_aire', df[:1], [])
    
    from datetime import datetime
    df = read_sql(
        f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,
             people_entered, people_exited
             from openiotv2.peoplecounting_hikvision_group
             where description = 'Biblioteca General' 
             AND time_instant > '{datetime.now().date()} 05:00:00'
             order by time_instant asc""", engine)

    lista_alertas_ocupacion = []

    if df.shape[0] > 0:
        exceso = df.iloc[0]['people_exited']
        df['people_exited'] = df['people_exited'] - exceso
        df['ocupacion'] = df['people_entered'] - df['people_exited']
        
        _, lista_alertas_ocupacion = alertas_valores('ocupacion', df[df.shape[0] - 1:], [])

    lista_alertas = lista_alertas_aire + lista_alertas_ocupacion
    engine.dispose()
    engine = conection_2()
    # Guardar Alertas en BBDD
    engine.execute('''DELETE FROM openiotv2.intervertical_warnings ''')

    if len(lista_alertas) > 0:
        for alerta in lista_alertas:
            engine.execute(f'''INSERT INTO
                               openiotv2.intervertical_warnings
                               (tipo_alerta, localizacion, alerta)
                               VALUES ('{alerta[0]}',
                                       '{alerta[1]}',
                                       '{alerta[2]}') ''')

    engine.dispose()
    return [0]
