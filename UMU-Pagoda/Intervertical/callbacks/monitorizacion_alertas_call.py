from dash import dcc, callback
from dash.dependencies import Input, Output, State
from pandas import read_json, DataFrame, to_datetime
from datetime import datetime
from assets.estilos.css_monitorizacion_alertas import *
from components.templates import div_comun, mapa
from components.funciones_monitorizacion_alertas import alertas_tiempo
from components.funciones_monitorizacion_alertas import alertas_valores


def alertas(id):

    @callback(Output(f'loading_{id}', 'children'), Input('url', 'pathname'),
              Input('store', 'data'))
    def input_triggers_nested(value, data):
        return ''

    @callback([
        Output(f'alertas_{id}_tiempo', 'children'),
        Output(f'alertas_{id}_distance', 'children')
    ], Input(f'loading_{id}', 'children'), State('store', 'data'))
    def sel_hora(click, data):

        obj_tiempo = [div_comun(s_titulo_alerta, '', 'Alertas de Tiempo')]
        obj_valores = [div_comun(s_titulo_alerta, '', 'Alertas de Valores')]

        if id == 'calidad_aire':
            df = read_json(data['co2'])[0:1]

        if id == 'ocupacion':
            df = read_json(data['people'])[-1:]   

        if df.shape[0] > 0:
            df, obj_tiempo = alertas_tiempo(df, obj_tiempo)
            obj_valores, _ = alertas_valores(id, df, obj_valores)
        else:
            obj_tiempo.append(
                div_comun({
                    'marginLeft': '25%',
                    'marginTop': '2%'
                }, '', 'Sin registros en las últimas 12 horas'))

        return [obj_tiempo, obj_valores]

    # Mapa
    @callback([Output(f'mapa_alertas_{id}', 'children')],
              [Input(f'alertas_{id}_distance', 'children')],
              [State('store', 'data')],
              prevent_initial_call=True)
    def sel_ho(load, data):

        if id == 'calidad_aire':
            df = read_json(data['co2'])[0:1]
        if id == 'ocupacion':
            df = read_json(data['people'])[0:1]

        if df.shape[0] > 0:
            df['lat'] = 37.988103
            df['long'] = -1.125190
            df['Hora'] = [
                datetime.fromtimestamp(i / 1000).time()
                for i in df['time_instant']
            ]
        else:
            df = DataFrame({
                'lat': [37.988103],
                'long': [-1.125190],
                'Hora': ['---']
            })

        return [dcc.Graph(figure=mapa(df, 'markers', 13, 'Hora', 14))]
