from datetime import datetime, timezone

from dash import callback, dcc
from dash.dependencies import Input, Output
from pandas import read_json, read_csv

from assets.estilos.css_monitorizacion_co2 import *
from components.templates import div_comun, line, pie


# Funcion auxiliar
def kpi(s_1, s_2, s_3, titulo, valor):
    return div_comun(s_panel_kpi, '', [
        div_comun(s_titulo_kpi, '', titulo),
        div_comun(s_1, '',
                  [div_comun(s_2, '', f'{valor}'),
                   div_comun(s_3, '', 'ppm')])
    ])


# Callbacks
@callback([
    Output('co2_valor_actual', 'children'),
    Output('co2_valor_max', 'children'),
    Output('co2_valor_min', 'children'),
    Output('co2_valor_recomendado', 'children'),
    Output('co2_valor_limite', 'children'),
    Output('line', 'children')
],
          Input('store', 'data'),
          prevent_initial_call=True)
def input_triggers_nested(data):

    co2_valor_actual = ''
    co2_maximo = div_comun(s_titulo, '',
                           'No hay registros en las últimas 12 horas')
    co2_minimo = ''
    donut = ''
    grafico = ''

    df = read_json(data['co2'])

    alerta = read_csv('Data/valores_alertas/calidad_aire.csv')['Max'].iloc[0]

    co2_valor_recomendado =\
        kpi(s_panel_valor_kpi, s_valor_kpi,
            s_ppm, 'Máximo recomendado', 1000)
    co2_valor_limite =\
        kpi(s_panel_valor_kpi, s_valor_kpi,
            s_ppm, 'Valor Alerta', alerta)

    if df.shape[0] > 0:

        # Kpi's
        valor_actual = df['Co2'].iloc[0]
        valor_maximo = df['Co2'].max()
        valor_min = df['Co2'].min()

        if valor_actual > alerta:
            s_panel_valor_kpi_actual['background'] = 'salmon'
        if valor_maximo > alerta:
            s_valor_kpi_max['color'] = s_ppm_max['color'] = 'salmon'
        if valor_min > alerta:
            s_valor_kpi_min['color'] = s_ppm_min['color'] = 'salmon'

        co2_valor_actual =\
            kpi(s_panel_valor_kpi_actual, s_valor_kpi_actual,
                s_ppm_actual, 'Valor actual', valor_actual)
        co2_maximo =\
            kpi(s_panel_valor_kpi, s_valor_kpi_max,
                s_ppm_max, 'Máximo alcanzado', valor_maximo)
        co2_minimo =\
            kpi(s_panel_valor_kpi, s_valor_kpi_min,
                s_ppm_min, 'Mínimo alcanzado', valor_min)

        df['time_instant'] = df['time_instant'].apply(
            lambda x: datetime.fromtimestamp(x / 1000, timezone.utc).replace(
                tzinfo=None))

        # grafico
        fig = line(df,
                   'time_instant',
                   'Co2',
                   range_y=[350, 1100],
                   title='',                   
                   mode='markers + lines')

        fig.update_layout(paper_bgcolor='#F8FBFB')
        
        fig.add_shape(  # add a horizontal "target" line
            type="line",
            line_color="#B03E24",
            line_width=3,
            opacity=1,
            line_dash="dot",
            x0=0.008,
            x1=0.998,
            xref="paper",
            y0=1000,
            y1=1000,
            yref="y")

        fig.add_shape(  # add a horizontal "target" line
            type="line",
            line_color="salmon",
            line_width=3,
            opacity=1,
            line_dash="dot",
            x0=0.008,
            x1=0.998,
            xref="paper",
            y0=alerta,
            y1=alerta,
            yref="y")

        fig.add_annotation(  # add a text callout with arrow
            text="Nivel de alerta",
            x=0.7,
            y=alerta,
            arrowhead=2,
            showarrow=True,
            textangle=0,
            xanchor='center',
            xref="paper",
        )

        fig.add_annotation(  # add a text callout with arrow
            text="Máximo recomendado",
            x=0.8,
            y=1000,
            arrowhead=2,
            showarrow=True,
            textangle=0,
            xanchor='center',
            xref="paper",
        )

        grafico =\
            dcc.Graph(figure=fig)

    return [
        co2_valor_actual, co2_maximo, co2_minimo, co2_valor_recomendado,
        co2_valor_limite, grafico
    ]
