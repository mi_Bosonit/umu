from datetime import datetime, timezone

from dash import callback, dcc
from dash.dependencies import Input, Output
from pandas import read_json

from components.templates import line


@callback([
    Output('personas_int', 'children'),
    Output('ocupacion', 'children'),
    Output('aforo_disp', 'children')
],
          Input('store', 'data'),
          prevent_initial_call=True)
def input_triggers_nested(data):

    df = read_json(data['people'])
    personas_interior = df['ocupacion'].iloc[df.shape[0] - 1]

    if personas_interior < 0:
        personas_interior = 0

    return [
        personas_interior,
        f''' {round((personas_interior * 100) / 800, 2)} %''',
        800 - personas_interior
    ]


@callback(Output('grafico_ocupacion', 'children'),
          Input('store', 'data'),
          prevent_initial_call=True)
def input_triggers_nested(data):

    df = read_json(data['people'])
    df['time_instant'] = df['time_instant'].apply(
        lambda x: datetime.fromtimestamp(x / 1000, timezone.utc).replace(
            tzinfo=None))
    
     
    return dcc.Graph(
        figure=line(df,
                    'time_instant',
                    'ocupacion',
                    range_y=[0, 800],
                    title=f'''Histórico Ocupación {
                        datetime.now().date().day}-{
                            datetime.now().date().month}-{datetime.now().date().year}''',
                    mode='lines').update_layout(paper_bgcolor='#F8FBFB'))
