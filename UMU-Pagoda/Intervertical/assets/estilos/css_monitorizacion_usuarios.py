s_panel_general = {'margin': '1%', 'width': '95%', 'height': '95%'}
s_titulo = {
    'height': '8%',
    'font-size': '4.5vmin',
    'text-align': 'center',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif'
}
s_aforo = {
    'margin-top': '2%',
    'width': '95%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '4vmin',
    'text-align': 'center'
}
s_panel_kpis = {'margin-top': '1%', 'width': '100%', 'height': '11%'}
s_kpi = {'float': 'left', 'width': '25%', 'height': '93%'}
s_titulo_kpi = {
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '2.3vmin',
    'text-align': 'center'
}
s_aforo_biblioteca = {
    'margin-top': '1%',
    'margin-left': '10%',
    'width': '80%',
    'height': '67%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '6vmin',
    'text-align': 'center',
    'background': '#E95EF7',
    'color': 'white'
}

s_panel_graficos = {
    'margin-left': '2%',
    'margin-top': '0.6%',
    'width': '100%',
    'height': '70%'
}
s_panel_grafico = {'float': 'left', 'width': '67%', 'height': '90%'}

s_panel_inf = {
    'float': 'left',
    'margin-left': '7%',
    'width': '22%',
    'height': '90%' 
}
s_panel_img = {'margin-left': '8%', 'height': '45%'}
s_image = {'width': '92%', 'height': '100%'}
s_div_inf = {'margin-top': '2%', 'height': '16%'}
s_titulo_inf = {
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '2.5vmin',
    'text-align': 'center'
}
s_valor_inf = {
    'margin-top': '2%',
    'height': '67%',
    'font-size': '5.5vmin',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'text-align': 'center',
    'background': 'green',
    'color': 'white'
}
