# Layout
s_panel_general = {'width': '97%', 'height': '85%'}
s_titulo = {
    'width': '100%',
    'height': '10%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
}
s_titulo_sub = {
    'float': 'left',
    'margin-left': '15%',
    'font-size': '4vmin', 
}

s_titulo_sub2 = {
    'float': 'left',
    'margin-left': '0.2%',
    'margin-top': '0.7%',
    'font-size': '1.7vmax', 
}

s_titulo_sub3 = {
    'float': 'left',
    'width': '8%',
    'margin-left': '0.2%',
    'font-size': '4vmin' 
}

div_titulo2 = {
    'width': '100%',
    'text-align': 'center',
    'font-size': '3vmin'
}

s_titulo2 = {
    'width': '90%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif'
}

s_panel_kpis = {
    'margin-top': '1.2%',
    'margin-left': '2%',
    'width': '100%', 
    'height': '19%'
}

s_div_kpi = {
    'float': 'left',
    'width': '19.2%',
    'height': '90%'
}

s_panel_graficos = {
    'margin-top': '0%',
    'margin-left': '1%',
    'width': '100%'
}

s_panel_line = {'margin-left': '2%', 'width': '90%'}

# Callbacks
s_panel_kpi = {'width': '95%', 'height': '100%'}
s_titulo_kpi = {
    'width': '100%',
    'height': '15%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '2.5vmin',
    'text-align': 'center'
}

s_panel_valor_kpi_actual = {
    'margin-top': '4%',
    'margin-left': '20%',
    'width': '75%',
    'height': '50%',
    'background': '#6CC859',
    'border-radius': '40%'
}
s_valor_kpi_actual = {
    'float': 'left',
    'margin-top': '2%',
    'margin-left': '10%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '6vmin',
    'text-align': 'center',
    'color': 'white'
}
s_ppm_actual = {
    'float': 'left',
    'margin-top': '8%',
    'margin-left': '1%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '4.5vmin',
    'color': 'white'
}

s_panel_valor_kpi = {'width': '100%', 'height': '80%'}

s_valor_kpi_max = {
    'float': 'left',
    'margin-top': '-4%',
    'margin-left': '25%',
    'position': 'relative',
    'top': '20%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '6vmin',
    'text-align': 'center',
    'color': '#6CC859'
}
s_ppm_max = {
    'float': 'left',
    'margin-top': '8%',
    'margin-left': '1%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '4.5vmin',
    'color': '#6CC859'
}

s_valor_kpi_min = {
    'float': 'left',
    'margin-top': '-4%',
    'margin-left': '25%',
    'position': 'relative',
    'top': '20%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '6vmin',
    'text-align': 'center',
    'color': '#6CC859'
}
s_ppm_min = {
    'float': 'left',
    'margin-top': '8%',
    'margin-left': '1%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '4.5vmin',
    'color': '#6CC859'
}

s_valor_kpi = {
    'float': 'left',
    'margin-top': '-4%',
    'margin-left': '25%',
    'position': 'relative',
    'top': '20%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '6vmin',
    'text-align': 'center',
    'color': 'grey'
}
s_ppm = {
    'float': 'left',
    'margin-top': '8%',
    'margin-left': '1%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '4vmin',
    'color': 'grey'
}
