s_panel_alertas = {
    'float': 'left',
    'margin-left': '2%',
    'margin-top': '2%',
    'width': '70%'
}
s_div = {'float': 'left', 'margin-top': '2%', 'width': '50%', 'height': '100%'}
s_div2 = {
    'width': '99%',
    'height': '95%',
    'border-radius': '35px 35px 5px 5px',
    'background': '#F1F5F7'
}
s_mapa = {
    'float': 'left',
    'margin-left': '1%',
    'margin-top': '3%',
    'width': '25%',
    'height': '90%',
    'border-radius': '35px 35px 5px 5px',
    'background': '#F1F5F7'
}
s_mapa2 = {
    'margin-left': '5%',
    'margin-top': '2%',
    'width': '90%',
    'height': '90%'
}

# alertas_call
color_boton = '#add8e6'
on = '2px solid #403DA0'
off = '1px solid #8380F7'
s_sin_datos = {
    'float': 'left',
    'margin-left': '2%',
    'color': '#FE0804',
    'margin-right': '3px',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '1rem'
}
s_aviso = {
    'margin-left': '2%',
    'margin-top': '5px',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '1rem',
    'color': 'red'
}
s_aviso_normal = {
    'margin-left': '2%',
    'margin-top': '5px',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '1rem',
    'color': 'black'
}
s_titulo_alerta = {
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '1.5rem',
    'text-align': 'center'
}
s_boton = {
    'margin-left': '2%',
    'width': '95%',
    'height': '95%',
    'background': color_boton,
    'padding': '2px 2px 0px 2px',
    'border-radius': '7px 7px 5px 5px',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'font-size': '1rem',
    'border': f'0.1px solid {color_boton}'
}
