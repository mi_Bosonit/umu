panel_general = {
    'width': '87%',     
    'font-family': 'Roboto, Helvetica, Arial, sans-serif',
    'margin-top': '2%',
    'background': '#F8FBFB'
}

div_panel = {
    'margin-top': '4%',
    'margin-left': '1%',
    'width': '100%',
    'height': '34vmax',
}

div_titulo = {
    'width': '90%',
    'height': '12%',
    'text-align': 'center',
    "font-size": "1.5vmax" 
}

div_grafico = {'width': '100%', 'margin-top': '1%'}

div_tarta = {
    'position': 'absolute',
    'left': '80%',
    'top': '22vmax',
    'width': '13vmax',
    'height': '18vmax' 
}

div_tarta2 = {
    'position': 'absolute',
    'left': '80%',
    'top': '60vmax',
    'width': '13vmax',
    'height': '18vmax'
}

div_tarta3 = {
    'position': 'absolute',
    'left': '80%',
    'top': '97vmax',
    'width': '13vmax',
    'height': '18vmax'
}

div_tarta4 = {
    'position': 'absolute',
    'left': '80%',
    'top': '135vmax',
    'width': '13vmax',
    'height': '18vmax'
}

titulo_pagina = {
    'margin-left': '22%',
    'margin-top': '1%',
    'margin-bottom': '2%',
    'width': '90%',
    'height': '30%',
    'font-size': '2.2vmax'
}

# pag reentrenar
s_boton = {
    "margin-left": "5%",
    "width": "20%",
    "height": "60%",
    "background": "#add8e6",
    "padding": "2px 2px 0px 2px",
    "border-radius": "17px 17px 15px 15px",
    "font-size": "1.5vmax",
    "border": '1px solid grey',
}