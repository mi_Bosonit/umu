s_loading = {'position': 'absolute', 'top': '20%', 'left': '25%'}

panel_general = {
    'margin-left': '2%',
    'width': '98%',
    'height': '96%',
    'font-family': 'Roboto, Helvetica, Arial, sans-serif'
}

div_previsiones = {
    'position': 'relative',
    'left': '2%',
    'top': '2%',
    'width': '98%',
    'height': '65%'
}

div_titulo = {
    'width': '100%',
    'height': '22%',
    'font-size': 'calc(1.3vmax + 1vmin)'
}

s_titulo = {
    'float': 'left',
    'margin-left': '20%'
    }

s_titulo_sub = {
    'float': 'left',
    'margin-left': '0.2%',
    'margin-top': '0.6%',
    'font-size': '1.3vmax',
    }

s_titulo_sub2 = {
    'float': 'left',
    'margin-left': '0.5%'
    }

div_titulo2 = {
    'width': '100%',
    'height': '5%',
    'font-size': 'calc(1vmax + 1vmin)'
}

s_titulo2 = {
    'float': 'left',
    'margin-left': '1%',
}

div_boton = {
    'float': 'left',
    'margin-left': '1%',
    'margin-top': '-0.3%',
    'width': '7.7%',
    'background': 'white',
    'border': '0.1px solid grey',
    'border-radius': '10px 10px 10px 10px',
    'font-size': 'calc(1.3vmax + 1.1vmin)'
}

div_boton_actualizar = {
    'float': 'left',
    'margin-top': '0%',
    'margin-left': '2%',
    'width': '6%',
    'border-radius': '10px 10px 10px 10px',
    'background': '#EBFEFB',
    'border': '0.5px solid grey',
    #'font-size': 'calc(1.3vmax + 1.1vmin)'
}

div_titulos = {
    'position': 'relative',
    'margin-top': '1%',
    'height': '5%',
    'font-size': 'calc(1vmax + 1.2vmin)',
    'color': '#0643AD'
}

titulo = {'float': 'left', 'margin-left': '0.5%', 'width': '30%'}

div_graficos = {
    'position': 'relative',
    'margin-top': '2%',
    'height': '75%',
    'text-align': 'center',
    'font-size': '1.2vmax'
}

div_parking = {
    'float': 'left',
    'margin-left': '0.5%',
    'width': '30%',
    'height': '100%',
    'border-radius': '15px 57px 25px 25px',
    'background': '#EBFEFB'
}

div_plazas = {'float': 'left', 'width': '30%'}

div_tarta = {
    'margin-top': '10%',
    'float': 'left',
    'width': '60%',    
}

div_co2 = {
    'margin-top': '4%',
    'margin-left': '2%',
    'width': '90%',
    'height': '30%',
    'border-radius': '15px 57px 25px 25px',
    'background': '#EBFEFB'
}

div_co2_2 = {
    'margin-top': '0.8%',
    'margin-left': '2%',
    'width': '90%',
    'height': '30%',
    'border-radius': '15px 57px 25px 25px',
    'background': '#EBFEFB'
}

div_imagen = {
    'float': 'left',
    'margin-top': '1%',
    'height': '100%',
    'width': '20%'
}

div_dato_co2 = {
    'float': 'left',
    'margin-left': '2%',
    'height': '95%',
    'width': '60%'
}



div_previsiones30 = {
    'position': 'relative',
    'left': '2%',
    'top': '8%',
    'width': '98%',
    'height': '65%'
}

div_titulo3 = {
    'width': '90%',
    'margin-left': '0%',
    'margin-top': '0%',
    'height': '10%',
    'font-size': 'calc(1vmax + 1vmin)' 
}

s_titulo3 = {
    'float': 'left',
    'width': '10%',
    'text-align': 'left',
    'margin-left': '-22%' 
}

s_titulo4 = {
    'float': 'left',
    'width': '10%',
    'text-align': 'left',
    'margin-left': '-7%' 
}

s_selector = {'float':'left',
              'margin-left': '-12.6%',
              'margin-top': '-0.55%',
              'border': '1px solid white'}

div_boton2 = {
    'float': 'left',
    'margin-left': '-1%',
    'width': '8.5%',
    'background': 'white',
    'border': '0.1px solid grey',
    'border-radius': '10px 10px 10px 10px',
    'font-size': 'calc(1.3vmax + 1.1vmin)'
}

div_graficos2 = {
     
    'margin-top': '1%',
    'height': '75%',
    'text-align': 'center',
    'font-size': '1.2vmax'
}