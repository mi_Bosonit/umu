import configparser

config = configparser.ConfigParser()
config.read('config.ini')

# BBDD
user = config['BBDD']['USER']
password = config['BBDD']['PASSWORD']
host = config['BBDD']['HOST']
port = config['BBDD']['PORT']
database = config['BBDD']['DATABASE']

# BBDD2
user2 = config['BBDD']['USER']
password2 = config['BBDD']['PASSWORD']
host2 = config['BBDD']['HOST']
port2 = config['BBDD']['PORT']
database2 = config['BBDD']['DATABASE']

bbdd = f'''postgresql://{user}:{password}@{host}:{port}/{database}'''
bbdd2 = f'''postgresql://{user2}:{password2}@{host2}:{port2}/{database2}'''

tabla_bbdd = config['BBDD']['TABLE']

# IAAAS
host = config['IAAAS']['HOST']
URL = f'''http://{host}/api/v1'''

# pagina
raiz = config['PAGINAS']['RAIZ']
