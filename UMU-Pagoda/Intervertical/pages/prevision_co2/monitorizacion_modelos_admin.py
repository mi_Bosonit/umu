import requests
from dash import callback, dcc
from dash.dependencies import Input, Output
from pandas import DataFrame

from assets.estilos.css_monitorizacion_modelos import *
from components.templates import boton, div_comun

s_loading = {'position': 'absolute', 'top': '25%', 'left': '15%'}
s_mensaje = {
    'margin-top': '2%',
    'margin-left': '5%',
    'width': '23%',
    "border-radius": "10px 15px 65px 15px",
    "padding": "4px 4px 0px 4px",
    'background': '#EAF2F8'
}

layout = div_comun(panel_general, '', [
    div_comun(
        s_loading, '',
        dcc.Loading(id='loading-2',
                    children=[div_comun({}, 'loading_entreno')],
                    type='default',
                    fullscreen=False)),
    div_comun(
        s_mensaje, '',
        '''Reentrenar los modelos puede llevar más de 10 minutos. 
           Se recomienda realizar el entreno en un horario en el que no 
           haya usuarios que quieran realizar predicciones.'''),
    div_comun({
        'margin-top': '1%',
        'height': '10%'
    }, '', boton('boton_reentrenar', 'Reentrenar Modelos', s_boton)),
])


@callback([
    Output('boton_reentrenar', 'n_clicks'),
    Output('loading_entreno', 'children')
], Input('boton_reentrenar', 'n_clicks'))
def display_page(n_clicks):
    if n_clicks > 0:        
        archivos = ['biblioteca_15', 'parking1_15', 'parking2_15']
        for archivo in archivos:
            DataFrame(columns=['plazas_libres', 'time_instant']).to_csv(
                f'pages/prevision_co2/data/{archivo}.csv', index=False)
        archivos = ['co2_15']
        for archivo in archivos:
            DataFrame(columns=['co2_final', 'time_instant']).to_csv(
                f'pages/prevision_co2/data/{archivo}.csv', index=False)
        url = 'https://intervertical.pagoda.local/ia/api/v1/train_models/'
        print(requests.get(url, verify='PagodaCA.crt'))
    return [0, '']
