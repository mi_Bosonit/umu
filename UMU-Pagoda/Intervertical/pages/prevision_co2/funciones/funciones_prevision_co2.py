from datetime import datetime, timedelta

import plotly.express as px
from dash import dcc
from pandas import (DataFrame, Timedelta, concat, merge_asof, read_csv,
                    to_datetime)
from sklearn.metrics import mean_absolute_percentage_error

from components.templates import div_comun


def crear_tarta(libre, capacidad, title):

    libre = libre * 100 / capacidad
    ocupado = 100 - libre

    df = DataFrame({'Estado': ['Libre', 'ocupado'], 'Total': [libre, ocupado]})

    return dcc.Graph(figure=px.pie(
        df,
        values="Total",
        title=title,
        color="Estado",
        hole=0.05,
        color_discrete_sequence=["#6EC15F", "#F5B041"],
    ).update_layout(
        paper_bgcolor='#EBFEFB', height=200, margin=dict(t=25, l=0, b=0, r=0)))


def plazas_libres_tarta(plazas_libres, capacidad, title):
    try:
        plazas_libres = int(plazas_libres)
        if plazas_libres < 0:
            plazas_libres = 0
        tarta = crear_tarta(plazas_libres, capacidad, title)
    except Exception:
        plazas_libres = 'No hay registros actuales para realizar previsión'
        tarta = ''
    return tarta, plazas_libres


def modelo_no_disponible():
    return ['', div_comun({'font-size': '1vmax'}, '', 'Modelo no disponible')]


def guardar_dato(columna, dato, hora, archivo):
    try:
        if dato < 0:
            dato = 0
        df = read_csv(f'pages/prevision_co2/data/{archivo}.csv')

        time_instant = datetime.now() + timedelta(minutes=15)
        if hora == 'hora2':
            time_instant = datetime.now() + timedelta(minutes=30)

        concat([
            df,
            DataFrame({
                columna: [int(dato)],
                'time_instant': [time_instant]
            })
        ]).to_csv(f'pages/prevision_co2/data/{archivo}.csv', index=False)
    except:
        pass


def line(df, x, y, range_y=[]):
    fig = px.line(df, x, y, range_y=range_y,)
    fig.update_layout(yaxis_title=y,
                      xaxis_title='',
                      xaxis={'showgrid': False},
                      yaxis={'showgrid': False},
                      plot_bgcolor='#F8FBFB',
                      paper_bgcolor='#F8FBFB',
                      autosize=True,
                      height=400,
                      margin=dict(t=0, l=0, b=0, r=0))
    fig.update_traces(showlegend=True,
                      name="Real",
                      mode='markers + lines',
                      line_color='#4B4238',
                      marker=dict(color="#4B4238", size=4))
    return fig


def add_predicciones(fig, df_15, y):
    fig.add_traces(
        line(df_15, x="time_instant",
             y=y).update_traces(showlegend=True,
                                name="Predicción",
                                mode="markers + lines",
                                line_color="#5C9C7A",
                                marker=dict(color="#48B75F", size=4)).data)
    return fig


def primer_registro(df):
    try:
        ult_reg = df['time_instant'][0].split('.')[0]
        return datetime.strptime(ult_reg, '%Y-%m-%d %H:%M:%S')
    except Exception:
        return ''


def ultimo_registro(df):
    try:
        ult_reg = df['time_instant'][df.shape[0] - 1].split('.')[0]
        return datetime.strptime(ult_reg, '%Y-%m-%d %H:%M:%S')
    except Exception:
        return ''


def get_mape(df_obs: DataFrame, df_pred: DataFrame,
             tolerance_minutes: int) -> float:
    df_obs['time_instant'] = to_datetime(df_obs['time_instant'])
    df_pred['time_instant'] = to_datetime(df_pred['time_instant'])
    df_obs = df_obs.sort_values(by='time_instant')
    df_pred = df_pred.sort_values(by='time_instant')
    df_pred.dropna(inplace=True)
    df_obs.dropna(inplace=True)
    ytrue_name = df_obs.columns.to_list()
    ytrue_name.remove('time_instant')
    ytrue_name = ''.join(ytrue_name)
    ypred_name = df_pred.columns.to_list()
    ypred_name.remove('time_instant')
    ypred_name = ''.join(ypred_name)
    data = merge_asof(df_obs, df_pred, on='time_instant',
                         tolerance=Timedelta(minutes=tolerance_minutes))
    data.dropna(inplace=True)
    if data.empty:
        mape = 0
    else:
        mape = mean_absolute_percentage_error(
            data[ytrue_name],
            data[ypred_name]
        )
    return mape


def crear_tarta_monitorizacion(libre):      

    values=[libre, 100-libre]
    
    return dcc.Graph(
        figure=px.pie(
            values=values,
            title = 'Error Medio',
            hole=0.7,                 
            color_discrete_sequence=["#CEFC4B", '#DD3D05'],
            ).update_layout(
                height=200,
                margin=dict(t=30, l=0, b=0, r=0),
                paper_bgcolor='#F8FBFB',
                annotations=[dict(text=f'{libre}%',
                                  x=0.2,
                                  y=0.5,
                                  font_size=25,
                                  showarrow=False)]                 
                ).update_traces(textinfo='none'))