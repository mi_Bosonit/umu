import requests

from pages.prevision_co2.funciones.funciones_prevision_co2 import guardar_dato


def peticiones_modelos():    
    
    try:
        url = 'https://intervertical.pagoda.local/ia/api/v1/single_preds/?times_fwd=1'
        single_preds = requests.get(url, verify='PagodaCA.crt').json()
        url = 'https://intervertical.pagoda.local/ia/api/v1/general_pred/?times_fwd=1'
        global_pred = requests.get(url, verify='PagodaCA.crt').json()
    except Exception:
        return 0

    try:
        # parking 1
        plazas_libres = single_preds['1'][0]['available_spot_number_03']
        time_instant = 'hora'
        archivo = 'parking1_15'
        guardar_dato('plazas_libres', plazas_libres, time_instant, archivo)
    except Exception:
        pass

    try:
        # parking 2
        plazas_libres = single_preds['1'][0]['available_spot_number_02']
        time_instant = 'hora'
        archivo = 'parking2_15'
        guardar_dato('plazas_libres', plazas_libres, time_instant, archivo)
    except Exception:
        pass

    try:
        # biblioteca
        plazas_ocupadas = single_preds['1'][0][
            'difference_deviceGroup-biblioteca-general']
        time_instant = 'hora'
        archivo = 'biblioteca_15'
        plazas_libres = 800 - plazas_ocupadas        
        if plazas_libres > 800:
            plazas_libres = 800
        if plazas_libres < 0:
            plazas_libres = 0
        guardar_dato('plazas_libres', plazas_libres, time_instant, archivo)
    except Exception:
        pass

    try:
        # co2
        co2_final = global_pred['1'][0]['co2_device-20280196']
        time_instant = 'hora'
        archivo = 'co2_15'
        guardar_dato('co2_final', co2_final, time_instant, archivo)
    except Exception:
        pass
    return 0