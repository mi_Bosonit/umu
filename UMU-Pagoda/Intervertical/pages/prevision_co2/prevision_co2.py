from dash import dcc, html

from assets.estilos.css_prevision_co2 import *
from components.templates import boton, div_comun, selector
from pages.prevision_co2.callbacks.prevision_co2_call import *
from configuracion import raiz

layout = div_comun(panel_general, '', [
    div_comun(div_previsiones, '', [
        div_comun(
            s_loading, '',
            dcc.Loading(id='loading-2',
                        children=[div_comun({}, 'loading_prevision_co2')],
                        type='default',
                        fullscreen=False)),
        div_comun(div_titulo, '', [
            div_comun(s_titulo, '', 'Previsión de ocupación y nivel de CO'),
            div_comun(s_titulo_sub, '', '2'),
            div_comun(s_titulo_sub2, '' ,'en la Biblioteca General'''),                        
        ]),
        div_comun(div_titulo2, '', [
            div_comun(s_titulo2, '', 'Previsión a 15 minutos'),              
            div_comun(div_boton, 'hora_15'),
            boton('boton_actualizar',
                  'Actualizar',
                  div_boton_actualizar,
                  n_clicks=0)                 
        ]),
        div_comun(div_graficos, 'div_graficos', [
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_p1')
                ]),
                div_comun(div_tarta, 'parking1'),
            ]),
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_p2')
                ]),
                div_comun(div_tarta, 'parking2'),
            ]),
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_biblio')
                ]),
                div_comun(div_tarta, 'usuarios'),
            ]),
        ])
    ]),
    div_comun(div_co2, '', [
        html.Img(style=div_imagen,
                 src=f'/{raiz}/assets/co2-svgrepo-com.svg'),
        div_comun(div_dato_co2, 'dato_co2')
    ]),
    div_comun(div_previsiones30, '', [                
        div_comun(div_titulo3, '', [
            div_comun(s_titulo3, '', 'Previsión a'), 
            selector('selector', s_selector, options=[30,45,60], value=30, placeholder=''), 
            div_comun(s_titulo4, '', 'minutos'),
            div_comun(div_boton2, 'hora_30')
        ]),
        div_comun(div_graficos2, 'div_graficos_30', [
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_p1_30')
                ]),
                div_comun(div_tarta, 'parking1_30'),
            ]),
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_p2_30')
                ]),
                div_comun(div_tarta, 'parking2_30'),
            ]),
            div_comun(div_parking, '', [
                div_comun(div_plazas, '', [
                    div_comun({'margin-top': '10%'}, '', 'Plazas libres'),
                    div_comun({}, 'plazas_biblio_30')
                ]),
                div_comun(div_tarta, 'usuarios_30'),
            ]),
        ])
    ]),
    div_comun(div_co2_2, '', [
        html.Img(style=div_imagen,
                 src='/dash/assets/co2-svgrepo-com.svg'),
        div_comun(div_dato_co2, 'dato_co2_30')
    ])    
])
