from assets.estilos.css_monitorizacion_modelos import *
from components.templates import div_comun
from pages.prevision_co2.callbacks.monitorizacion_modelos_call import *


layout = div_comun(panel_general, '', [
    div_comun(titulo_pagina, '', 'Rendimiento de los modelos'),
    div_comun({'position': 'absolute', 'left': '51%', 'top': '6%'}, '', 
              'Error del modelo (porcentaje de error medio absoluto)'),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Ocupación Parking Superior'),            
        div_comun(div_grafico, 'grafico1'),
        div_comun(div_tarta, 'tarta1')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Ocupación Parking Inferior'),
        div_comun(div_grafico, 'grafico2'),
        div_comun(div_tarta2, 'tarta2')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Ocupación Biblioteca General'),
        div_comun(div_grafico, 'grafico3'),
        div_comun(div_tarta3, 'tarta3')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Nivel Co2 en Biblioteca General'),
        div_comun(div_grafico, 'grafico4'),
        div_comun(div_tarta4, 'tarta4')
    ]),
])



layout2 = div_comun(panel_general, '', [
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', [
            div_comun({'float': 'left', 'width': '70%'}, '', 'Modelo Ocupación Parking Superior'),
            div_comun({'float': 'left', 'width': '25%'}, '', '80%'),
        ]),
        div_comun(div_grafico, 'grafico1')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Ocupación Parking Inferior'),
        div_comun(div_grafico, 'grafico2')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Ocupación Biblioteca General'),
        div_comun(div_grafico, 'grafico3')
    ]),
    div_comun(div_panel, '', [
        div_comun(div_titulo, '', 'Modelo Nivel Co2 en Biblioteca General'),
        div_comun(div_grafico, 'grafico4')
    ]),
])
