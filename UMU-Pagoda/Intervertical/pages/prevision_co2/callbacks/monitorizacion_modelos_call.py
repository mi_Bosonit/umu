import plotly.express as px
from dash import callback, dcc
from dash.dependencies import Input, Output
from pandas import read_csv, read_sql

from components.utils import conection
from configuracion import tabla_bbdd
from pages.prevision_co2.funciones.funciones_prevision_co2 import (
    add_predicciones, crear_tarta_monitorizacion, get_mape, line,
    primer_registro, ultimo_registro)


@callback([Output('grafico1', 'children'),
           Output('tarta1', 'children')],
            Input('url', 'pathname'))
def display_page(pathname):

    df_15 = read_csv('pages/prevision_co2/data/parking1_15.csv')
    prim_registro = primer_registro(df_15)
    ult_reg = ultimo_registro(df_15)
    if ult_reg != '':
        query = f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,                 
                        available_spot_number AS "Plazas Libres"
                    FROM {tabla_bbdd}.parkingmonitoring
                    WHERE time_instant >= '{prim_registro}'
                    AND time_instant <= '{ult_reg}'
                    AND ocb_id = '3'
                    ORDER by time_instant asc"""

        engine = conection()
        df = read_sql(query, engine)
        engine.dispose()
        rendimiento = get_mape(df_15, df, 20) 

        df['Plazas Ocupadas'] = df['Plazas Libres'].apply(lambda x: 38 - x)
        df_15['Plazas Ocupadas'] = df_15['plazas_libres'].apply(lambda x: 38 - x)
        fig = line(df, 'time_instant', 'Plazas Ocupadas', [0, 40])
        fig = add_predicciones(fig, df_15, 'Plazas Ocupadas')               
        tarta = 'No hay datos suficientes para evaluar el modelo'

        if rendimiento != 'No hay datos':
            tarta = crear_tarta_monitorizacion(round(rendimiento * 100, 2))
            
        return [dcc.Graph(figure=fig), tarta]
    else:
        return ['No se han registrado predicciones aún', '']


@callback([Output('grafico2', 'children'),
           Output('tarta2', 'children')],
          Input('grafico1', 'children'))
def display_page(pathname):

    df_15 = read_csv('pages/prevision_co2/data/parking2_15.csv')
    prim_registro = primer_registro(df_15)
    ult_reg = ultimo_registro(df_15)
    if ult_reg != '':
        query = f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,                 
                        available_spot_number AS "Plazas Libres"
                    FROM {tabla_bbdd}.parkingmonitoring
                    WHERE time_instant >= '{prim_registro}'
                    AND time_instant <= '{ult_reg}'
                    AND ocb_id = '2'
                    ORDER by time_instant asc"""
        engine = conection()
        df = read_sql(query, engine)
        engine.dispose()        
        rendimiento = get_mape(df_15, df, 4)

        df['Plazas Ocupadas'] = df['Plazas Libres'].apply(lambda x: 227 - x)
        df_15['Plazas Ocupadas'] = df_15['plazas_libres'].apply(lambda x: 227 - x)
        fig = line(df, 'time_instant', 'Plazas Ocupadas', [0, 230])
        fig = add_predicciones(fig, df_15, 'Plazas Ocupadas')  
        tarta = 'No hay datos suficientes para evaluar el modelo'

        if rendimiento != 'No hay datos':
            tarta = crear_tarta_monitorizacion(round(rendimiento * 100, 2))
            
        return [dcc.Graph(figure=fig), tarta]
    else:
        return ['No se han registrado predicciones aún', '']


@callback([Output('grafico3', 'children'),
           Output('tarta3', 'children')],
          Input('grafico2', 'children'))
def display_page(pathname):

    df_15 = read_csv('pages/prevision_co2/data/biblioteca_15.csv')   
    prim_registro = primer_registro(df_15)
    ult_reg = ultimo_registro(df_15)
    if ult_reg != '':
        query = f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,                 
                        800 - difference AS "Plazas Libres"
                    FROM {tabla_bbdd}.peoplecounting_hikvision_group
                    WHERE time_instant >= '{prim_registro}'
                    AND time_instant <= '{ult_reg}'
                    AND controlled_asset = 'building-biblioteca-general'
                    ORDER by time_instant asc"""

        engine = conection()
        df_real = read_sql(query, engine)
        engine.dispose()
        rendimiento = get_mape(df_15, df_real, 1)
        df_real['Plazas Ocupadas'] = df_real['Plazas Libres'].apply(lambda x: 800 - x)
        df_15['Plazas Ocupadas'] = df_15['plazas_libres'].apply(lambda x: 800 - x)
        fig = line(df_real, 'time_instant', 'Plazas Ocupadas', [0, 800])
        fig = add_predicciones(fig, df_15, 'Plazas Ocupadas')  
        tarta = 'No hay datos suficientes para evaluar el modelo'

        if rendimiento != 'No hay datos':
            tarta = crear_tarta_monitorizacion(round(rendimiento * 100, 2))
            
        return [dcc.Graph(figure=fig), tarta]
    else:
        return ['No se han registrado predicciones aún', '']


@callback([Output('grafico4', 'children'),
           Output('tarta4', 'children')],
          Input('grafico3', 'children'))
def display_page(pathname):

    df_15 = read_csv('pages/prevision_co2/data/co2_15.csv')
    prim_registro = primer_registro(df_15)
    ult_reg = ultimo_registro(df_15)
    if ult_reg != '':
        co2 = f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,                 
                        co2 AS "Co2"
                FROM {tabla_bbdd}.ambientmonitoring_cometsensor
                WHERE time_instant >= '{prim_registro}'
                AND time_instant <= '{ult_reg}'
                AND controlled_asset = 'building-biblioteca-general'
                ORDER by time_instant asc"""

        engine = conection()
        df = read_sql(co2, engine)
        engine.dispose()
        fig = line(df, 'time_instant', 'Co2', [300, 800])
        fig = add_predicciones(fig, df_15, 'co2_final') 
        rendimiento = get_mape(df_15, df, 5)
        tarta = 'No hay datos suficientes para evaluar el modelo'
        
        if rendimiento != 'No hay datos':
            tarta = crear_tarta_monitorizacion(round(rendimiento * 100, 2))
            
        return [dcc.Graph(figure=fig), tarta]
    else:
        return ['No se han registrado predicciones aún', '']
