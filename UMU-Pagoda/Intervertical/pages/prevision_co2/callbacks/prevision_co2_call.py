from datetime import datetime, timedelta

import requests
from dash import callback
from dash.dependencies import Input, Output
from pandas import read_csv

from components.templates import div_comun
from pages.prevision_co2.funciones.funciones_prevision_co2 import (
    modelo_no_disponible, plazas_libres_tarta)

global single_preds
global global_pred
capacidad_parking1 = 38
capacidad_parking2 = 227
capacidad_biblio = 800


@callback([
    Output('hora_15', 'children'),
    Output('loading_prevision_co2', 'children'),
    Output('boton_actualizar', 'n_clicks')
], [Input('url', 'pathname'),
    Input('boton_actualizar', 'n_clicks')])
def display_page(pathname, bot):
    global single_preds
    global global_pred

    try:        
        url = 'https://intervertical.pagoda.local/ia/api/v1/single_preds/?times_fwd=4'
        single_preds = requests.get(url, verify='PagodaCA.crt').json()
        url = 'https://intervertical.pagoda.local/ia/api/v1/general_pred/?times_fwd=4'
        global_pred = requests.get(url, verify='PagodaCA.crt').json()

    except Exception:
        single_preds = ''
        global_pred = ''
        pass

    hora = str((datetime.now() + timedelta(minutes=15)).time()).split(".")[0]
    return [hora, '', 0]


@callback([Output('parking1', 'children'),
           Output('plazas_p1', 'children')],
          Input('hora_15', 'children'))
def display_page(bot1):

    global single_preds
    try:
        plazas_libres = single_preds['1'][0]['available_spot_number_03']
        return plazas_libres_tarta(plazas_libres, capacidad_parking1, 'Parking Superior')
    except Exception:
        return modelo_no_disponible()


@callback([Output('parking2', 'children'),
           Output('plazas_p2', 'children')],
          Input('parking1', 'children'))
def display_page(bot1):

    global single_preds
    try:
        plazas_libres = single_preds['1'][0]['available_spot_number_02']
        return plazas_libres_tarta(plazas_libres, capacidad_parking2, 'Parking Inferior')
    except Exception:
        return modelo_no_disponible()


@callback(
    [Output('usuarios', 'children'),
     Output('plazas_biblio', 'children')],
    Input('parking2', 'children'))
def display_page(bot1):

    global single_preds
    try:
        plazas_ocupadas = single_preds['1'][0][
            'difference_deviceGroup-biblioteca-general']
        plazas_libres = capacidad_biblio - plazas_ocupadas

        if plazas_libres > 800:
            plazas_libres = 800
        if plazas_libres < 0:
            plazas_libres = 0
        return plazas_libres_tarta(plazas_libres, capacidad_biblio, 'Biblioteca General')
    except Exception:
        return modelo_no_disponible()


@callback(Output('dato_co2', 'children'),
          Input('usuarios', 'children'))
def display_page(bot1):
    try:
        global global_pred
        try:
            alerta_co2 = read_csv(
                'Data/valores_alertas/calidad_aire.csv')['Max'].iloc[0]
            co2_final = global_pred['1'][0]['co2_device-20280196']
        except Exception:
            return modelo_no_disponible()

        co2_final = int(co2_final)
        color = "#6EC15F"
        aviso = f'(Alerta en {alerta_co2})'

        if co2_final > alerta_co2:
            color = '#943126'

        dato_co2 = div_comun(
            {
                'padding-top': '5%',
                'font-size': '4.5vmax',
                'color': f'{color}',
            }, '', f'{co2_final} ppm {aviso}')
    except Exception:
        dato_co2 = 'No hay registros actuales para realizar previsión'
    return dato_co2


@callback(Output('hora_30', 'children'),
          [Input('selector', 'value'),
           Input('boton_actualizar', 'n_clicks')])
def display_page(dat_hora, bot):
    try:
        hora = str((datetime.now() +
                    timedelta(minutes=dat_hora)).time()).split(".")[0]
    except Exception:
        hora = ''
    return [hora]


@callback(
    [Output('parking1_30', 'children'),
     Output('plazas_p1_30', 'children')],
    [Input('hora_15', 'children'),
     Input('selector', 'value')],
    prevent_initial_call=True)
def display_page(bot1, selector):
    global single_preds
    hora = '2'
    if selector == 45:
        hora = '3'
    if selector == 60:
        hora = '4'

    try:
        plazas_libres = single_preds[hora][0]['available_spot_number_03']
        return plazas_libres_tarta(plazas_libres, capacidad_parking1, 'Parking Superior')
    except Exception:
        return modelo_no_disponible()


@callback(
    [Output('parking2_30', 'children'),
     Output('plazas_p2_30', 'children')],
    [Input('parking1_30', 'children'),
     Input('selector', 'value')],
    prevent_initial_call=True)
def display_page(bot1, selector):
    global single_preds
    hora = '2'
    if selector == 45:
        hora = '3'
    if selector == 60:
        hora = '4'
    try:
        plazas_libres = single_preds[hora][0]['available_spot_number_02']
        return plazas_libres_tarta(plazas_libres, capacidad_parking2, 'Parking Inferior')
    except Exception:
        return modelo_no_disponible()


@callback([
    Output('usuarios_30', 'children'),
    Output('plazas_biblio_30', 'children')
], [Input('parking2_30', 'children'),
    Input('selector', 'value')],
          prevent_initial_call=True)
def display_page(bot1, selector):
    global single_preds
    hora = '2'
    if selector == 45:
        hora = '3'
    if selector == 60:
        hora = '4'
    try:
        plazas_ocupadas = single_preds[hora][0][
            'difference_deviceGroup-biblioteca-general']        
        plazas_libres = capacidad_biblio - plazas_ocupadas
        if plazas_libres > 800:
            plazas_libres = 800
        if plazas_libres < 0:
            plazas_libres = 0
        return plazas_libres_tarta(plazas_libres, capacidad_biblio, 'Biblioteca General')
    except Exception:
        return modelo_no_disponible()


@callback(
    Output('dato_co2_30', 'children'),
    [Input('usuarios_30', 'children'),
     Input('selector', 'value')],
    prevent_initial_call=True)
def display_page(bot1, selector):

    try:
        global global_pred
        hora = '2'
        if selector == 45:
            hora = '3'
        if selector == 60:
            hora = '4'

        try:
            alerta_co2 = read_csv(
                'Data/valores_alertas/calidad_aire.csv')['Max'].iloc[0]
            co2_final = global_pred[hora][0]['co2_device-20280196']
        except Exception:
            return modelo_no_disponible()
        
        co2_final = int(co2_final)
        color = "#6EC15F"
        aviso = f'(Alerta en {alerta_co2})'

        if co2_final > alerta_co2:
            color = "#943126"

        dato_co2 = div_comun(
            {
                'padding-top': '5%',
                'font-size': '4.5vmax',
                'color': f'{color}',
            }, '', f'{co2_final} ppm {aviso}')
    except Exception:
        dato_co2 = 'No hay registros actuales para realizar previsión'
    return dato_co2
