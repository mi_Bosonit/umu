import requests
from dash import callback
from dash.dependencies import Input, Output
from pandas import DataFrame


@callback(Output('boton_reentrenar', 'n_clicks'),
          Input('boton_reentrenar', 'n_clicks'))
def display_page(n_clicks):
    if n_clicks > 0:
        url = 'http://intervertical.pagoda.local:3000/api/v1/train_models/'
        print(requests.get(url))
        archivos = [
            'biblioteca_15', 'parking1_15',
            'parking2_15'
        ]
        for archivo in archivos:
            DataFrame(columns=['plazas_libres', 'time_instant']).to_csv(
                f'pages/prevision_co2/data/{archivo}.csv', index=False)
        archivos = ['co2_15']
        for archivo in archivos:
            DataFrame(columns=['co2_final', 'time_instant']).to_csv(
                f'pages/prevision_co2/data/{archivo}.csv', index=False)
    return 0
