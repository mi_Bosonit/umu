from dash import html

from assets.estilos.css_monitorizacion_usuarios import *
from callbacks import monitorizacion_ocupacion_call
from components.templates import div_comun

layout = div_comun(s_panel_general, '', [
    div_comun(s_titulo, '', 'Monitorización Tráfico de usuarios en la Biblioteca General'),
    #div_comun(s_aforo, '', 'Aforo Máximo'),
    div_comun(s_panel_kpis, '', [
        div_comun(s_kpi, '', [
            div_comun(s_titulo_kpi, '', 'Aforo Máximo Biblioteca'),
            div_comun(s_aforo_biblioteca, '', '800')
        ]),
        
    ]),
    div_comun(s_panel_graficos, '', [
        div_comun(s_panel_grafico, 'grafico_ocupacion'),
        div_comun(s_panel_inf, '', [
            div_comun(s_panel_img, '',
                      html.Img(style=s_image, src='/dash/assets/people.png')),
            div_comun(s_div_inf, '', [
                div_comun(s_titulo_inf, '', 'Total personas en el interior'),
                div_comun(s_valor_inf, 'personas_int')
            ]),
            div_comun(s_div_inf, '', [
                div_comun(s_titulo_inf, '', 'Ocupación (personas) %'),
                div_comun(s_valor_inf, 'ocupacion')
            ]),
            div_comun(s_div_inf, '', [
                div_comun(s_titulo_inf, '', 'Aforo disponible (personas)'),
                div_comun(s_valor_inf, 'aforo_disp')
            ])
        ])
    ])
])
