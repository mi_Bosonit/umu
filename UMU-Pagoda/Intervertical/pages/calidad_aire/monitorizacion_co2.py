from dash import html

from assets.estilos.css_monitorizacion_co2 import *
from callbacks import monitorizacion_co2_call
from components.templates import div_comun

layout = div_comun(s_panel_general, '', [
    div_comun(s_titulo, '', [
        div_comun(s_titulo_sub, '', 'Monitorización calidad del aire en la Biblioteca General (Nivel de CO'),
        div_comun(s_titulo_sub2, '', '2'),
        div_comun(s_titulo_sub3, '', ')'),
    ]),
    div_comun(s_panel_kpis, '', [
        div_comun(s_div_kpi, 'co2_valor_actual'),
        div_comun(s_div_kpi, 'co2_valor_max'),
        div_comun(s_div_kpi, 'co2_valor_min'),
        div_comun(s_div_kpi, 'co2_valor_recomendado'), 
        div_comun(s_div_kpi, 'co2_valor_limite'), 
                 
    ]),
    div_comun(div_titulo2, '',
        div_comun(s_titulo2, '', 'Valores de recogidos en las últimas 12 horas')
    ),
    div_comun(
        s_panel_graficos, '', div_comun(s_panel_line, 'line'))
])
