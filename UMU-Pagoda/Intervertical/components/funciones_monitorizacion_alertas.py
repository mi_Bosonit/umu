from datetime import datetime

from pandas import read_csv

from assets.estilos.css_monitorizacion_alertas import *
from components.templates import boton, div_comun


def alertas_tiempo(df, obj_tiempo):

    dispositivo = 'Biblioteca General'
    df['time_instant'] = [
        datetime.fromtimestamp(i / 1000) for i in df['time_instant']
    ]
    horas =\
        int((
            datetime.now() - df.iloc[0]['time_instant']
             ).total_seconds() / 3600)

    if horas > 2:
        obj_tiempo.append(
            div_comun({'margin-top': '10px'}, '', [
                boton('', dispositivo, s_boton),
                div_comun({
                    'margin-left': '6px',
                    'margin-top': '0px'
                }, '', [
                    div_comun(s_sin_datos, '', 'Sin Datos '),
                    div_comun(s_aviso, '', f'por {horas} horas')
                ])
            ]))
        df = df.drop([0], axis=0)

    return df, obj_tiempo


def alertas_valores(id, df, obj_valores):

    lista_de_alertas = []
    for i in range(len(df)):
        dispositivo = 'Biblioteca General'
        aviso = []

        if id == 'calidad_aire':
            valores = read_csv('Data/valores_alertas/calidad_aire.csv')

            if df.iloc[i]['Co2'] > valores.iloc[0]['Max']:
                aviso.append(
                    div_comun(
                        s_aviso, '',
                        f'''Co2 alcanza {df.iloc[i]['Co2']} ppm. Límite en {
                            valores.iloc[0]['Max']} ppm'''
                    ))
                lista_de_alertas.append([
                    'Co2', dispositivo,
                    f'''Co2 alcanza {df.iloc[i]['Co2']} ppm. Límite en {
                        valores.iloc[0]['Max']} ppm'''
                ])

        if id == 'ocupacion':
            valores = read_csv('Data/valores_alertas/ocupacion.csv')

            if df.iloc[i]['ocupacion'] > valores.iloc[0]['Max']:
                aviso.append(
                    div_comun(
                        s_aviso, '',
                        f'''Ocupación alcanza {
                            df.iloc[i]['ocupacion']} personas. Límite en {
                                valores.iloc[0]['Max']}'''
                    ))
                lista_de_alertas.append([
                    'Ocupación', dispositivo,
                    f'''Ocupación alcanza {
                        df.iloc[i]['ocupacion']} personas. Límite en {
                            valores.iloc[0]['Max']}'''
                ])

        if len(aviso) == 0:
            aviso.append(div_comun(s_aviso_normal, '', f'''Sin Alertas'''))

        obj_valores.append(
            div_comun({'margin-top': '10px'}, '', [
                boton('', dispositivo, s_boton),
                div_comun({
                    'margin-left': '6px',
                    'margin-top': '5px'
                }, '', aviso)
            ]))

    return obj_valores, lista_de_alertas
