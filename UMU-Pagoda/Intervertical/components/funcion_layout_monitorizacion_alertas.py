from dash import dcc

from assets.estilos.css_monitorizacion_alertas import *
from components.templates import div_comun


def layout_monitorizacion_alertas(id):
    return [
        dcc.Loading(
            id='loading-2',
            children=[div_comun({}, '', [div_comun({}, f'loading_{id}')])],
            type='circle'),
        div_comun(s_panel_alertas, '', [
            div_comun(s_div, '', div_comun(s_div2, f'alertas_{id}_tiempo')),
            div_comun(s_div, '', div_comun(s_div2, f'alertas_{id}_distance'))
        ]),
        div_comun(s_mapa, '', div_comun(s_mapa2, f'mapa_alertas_{id}'))
    ]
