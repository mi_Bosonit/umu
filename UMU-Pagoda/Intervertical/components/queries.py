from datetime import datetime, timedelta

from configuracion import tabla_bbdd

# Monitorizacion Co2
co2 = f"""SELECT ocb_id, time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,
                 temperature AS "Temperatura", relative_humidity AS "Humedad Relativa",
                 barometric_pressure AS "Presión Atmosférica",
                 co2 AS "Co2", controlled_asset
          FROM {tabla_bbdd}.ambientmonitoring_cometsensor
          WHERE time_instant > '{datetime.now() - timedelta(hours = 12)}'
          and controlled_asset = 'building-biblioteca-general'
          ORDER by time_instant desc""" #building-biblioteca-antonio-nebrija

# Ocupacion
people = f"""SELECT time_instant AT TIME ZONE 'Europe/Madrid' AS time_instant,
             people_entered, people_exited
             from {tabla_bbdd}.peoplecounting_hikvision_group
             where description = 'Biblioteca General' 
             AND time_instant > '{datetime.now().date()} 05:00:00'
             order by time_instant asc""" #Biblioteca Antonio de Nebrija
