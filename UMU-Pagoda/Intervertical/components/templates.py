import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
from dash import dash_table, dcc, html


def div_comun(style, id='', children='', hidden=False):
    return html.Div(style=style, children=children, id=id, hidden=hidden)


def boton(id, nombre, style, hidden=False, disabled=False, n_clicks=0):
    return html.Button(style=style,
                       id=id,
                       n_clicks=n_clicks,
                       hidden=hidden,
                       children=nombre,
                       disabled=disabled)


def selector(id, style, options=[], value='', placeholder=''):
    return dcc.Dropdown(style=style,
                        id=id,
                        options=options,
                        value=value,
                        placeholder=placeholder)


# Tablas
def tabla_alertas(id, df, columns, style_data_conditional):
    return dbc.Container([
        dash_table.DataTable(df.to_dict('records'),
                             columns=columns,
                             id=f'tabla_{id}',
                             page_size=8,
                             style_table={'box-sizing': 'inherit'},
                             style_data={
                                 'width': '33%',
                                 'padding': '6px',
                                 'align-items': 'center',
                                 'border-right': '0.1px solid white',
                                 'border-left': '0.1px solid white',
                                 'height': '50px',
                                 'color': '#44535A',
                                 'font-family': '''Roboto,
                                                  Helvetica,
                                                  Arial,
                                                  sans-serif''',
                                 'font-size': '1rem'
                             },
                             style_header={
                                 'padding': '6px',
                                 'color': 'white',
                                 'background': '#4189AD',
                                 'font-family': '''Roboto,
                                                    Helvetica,
                                                    Arial,
                                                    sans-serif''',
                                 'font-size': '1rem',
                                 'height': '55px',
                                 'border-botton': '0.1px solid #4189AD',
                                 'border-right': '0.1px solid #4189AD'
                             },
                             style_cell={'textAlign': 'center'},
                             style_data_conditional=style_data_conditional,
                             editable=True)
    ])


# line
def line(df,
         x,
         y,
         range_y=[],
         title='',
         yaxis_title='',
         xaxis_title='',
         mode='lines',
         color_discrete_map={}):

     
    fig = px.line(df,
                    x=x,
                    y=y,                    
                    range_y=range_y,                     
                    title=title,
                    color_discrete_map=color_discrete_map)
     

    fig.update_layout(title={
                         'font': {
                         'family': 'Roboto, Helvetica, Arial, sans-serif',
                         'color': 'black',
                         'size': 17
                        }
                      },
                      yaxis_title=yaxis_title,
                      xaxis_title=xaxis_title,
                      xaxis={'showgrid': False},
                      yaxis={'showgrid': False},                     
                      plot_bgcolor='#FFFDFD',
                      margin=dict(t=35, l=0, b=0, r=0))
    fig.update_traces(mode=mode,line_color="#463A37",)

    return fig


# pie
def pie(df,
        values,
        height,
        names='',
        title='',
        hole=0,
        color='',
        color_discrete_map={}):

    fig = px.pie(df,
                 values=values,
                 names=names,
                 title=title,
                 hole=hole,
                 color=color,
                 color_discrete_map=color_discrete_map)
    fig.update_layout(title={
        'font': {
            'family': 'Roboto, Helvetica, Arial, sans-serif',
            'color': 'black',
            'size': 20
        }
    },
                      plot_bgcolor='white',
                      autosize=True,
                      height=height,
                      margin=dict(t=0, l=0, b=0, r=50))
    return fig


# Mapas
def mapa(df, mode, size, text, zoom=13, lat=0, lon=0):

    if lat == 0:
        lat = df['lat'].iloc[0]
    if lon == 0:
        lon = df['long'].iloc[0]
    fig = go.Figure(
        go.Scattermapbox(lat=df["lat"],
                         lon=df["long"],
                         mode=mode,
                         marker=go.scattermapbox.Marker(size=size,
                                                        color='red'),
                         text=df[text]))
    fig.update_layout(hovermode='closest',
                      mapbox_style="open-street-map",
                      mapbox={
                          'zoom': zoom,
                          'center': {
                              'lon': lon,
                              'lat': lat
                          }
                      })
    fig.update_layout(autosize=True, margin={"r": 0, "t": 0, "l": 0, "b": 0})

    return fig
