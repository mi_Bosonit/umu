from datetime import datetime

import callbacks.monitorizacion_contenedores_call
from assets.estilos.css_monitorizacion import *
from components.listas import meses
from components.templates import boton, div_comun, selector

layout = div_comun(
    s_panel,
    "",
    [
        div_comun(
            s_panel_botones,
            "",
            [
                div_comun(
                    s_botones,
                    "",
                    [
                        div_comun(
                            s_sub_panel,
                            "",
                            boton(
                                "boton_ult_reg", "Último Registro",
                                s_boton, n_clicks=1
                            ),
                        ),
                        div_comun(
                            s_sub_panel,
                            "",
                            boton(
                                "boton_sel_reg",
                                f"Registros {meses[datetime.now().month - 1]}",
                                s_boton,
                            ),
                        ),
                    ],
                ),
                div_comun(
                    s_selectores,
                    "panel_selectores",
                    [
                        div_comun(
                            s_selector_iz,
                            "",
                            selector(
                                "sel_dia",
                                {"height": "2.8vmax"},
                                placeholder="Seleccione Día",
                            ),
                        ),
                        div_comun(
                            s_selector,
                            "",
                            selector(
                                "sel_hora",
                                {"height": "2.8vmax"},
                                placeholder="Seleccione Hora",
                            ),
                        ),
                    ],
                    True,
                ),
                div_comun(
                    s_botones,
                    "panel_boton_todos",
                    div_comun(
                        {}, "", boton("boton_ver_todos", "Ver Todos",
                                      s_boton_todos)
                    ),
                    True,
                ),
            ],
        ),
        div_comun(
            s_graficos,
            "",
            [
                div_comun(
                    s_grafico,
                    "",
                    [
                        div_comun(
                            s_div_tabla,
                            "panel_ultimo_registro",
                            [
                                div_comun({}, "tabla_ultimo_registro"),
                                div_comun({}, "tabla_ultimo"),
                            ],
                            True,
                        ),
                        div_comun(
                            s_div_tabla,
                            "panel_seleccionar_registro",
                            [
                                div_comun({}, "tabla_seleccionar_registro"),
                                div_comun({}, "tabla_seleccionar"),
                            ],
                            True,
                        ),
                    ],
                ),
                div_comun(
                    s_grafico_der,
                    "",
                    [
                        div_comun(
                            s_div_mapa,
                            "",
                            [
                                div_comun(s_mapa, "mapa_ultimo_registro",
                                          True),
                                div_comun(s_mapa, "mapa_seleccionar_registro",
                                          True),
                            ],
                            False,
                        )
                    ],
                ),
            ],
        ),
    ],
)
