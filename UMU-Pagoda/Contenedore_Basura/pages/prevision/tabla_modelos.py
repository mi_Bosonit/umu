# Dash
import dash_bootstrap_components as dbc
from dash import dash_table

columns = [
    dict(id="Nombre Modelo", name="Nombre Modelo", editable=True),
    dict(id="Id Modelo", name="Id Modelo", editable=False),
    dict(id="Tipo", name="Tipo", editable=False),
]

style_data_conditional = [
    {
        "if": {"column_id": ["Nombre Modelo"],
               "column_id": ["Nombre Modelo"]},
        "width": "40%",
        "text-align": "left",
    },
    {"if": {"column_id": ["Id Modelo"],
            "column_id": ["Id Modelo"]},
     "width": "40%"},
    {"if": {"column_id": ["Tipo"],
            "column_id": ["Tipo"]},
     "text-align": "left"},
    {
        "if": {"column_id": ["Localización"],
               "column_id": ["Localización"]},
        "text-align": "left",
    },
]


def tabla(
     id, df, columns=columns,
     style_data_conditional=style_data_conditional
):

    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columns,
                id=f"tabla_{id}",
                page_size=8,
                style_table={"box-sizing": "inherit"},
                style_data={
                    "width": "33%",
                    "padding": "6px",
                    "align-items": "center",
                    "border-right": "0.1px solid white",
                    "border-left": "0.1px solid white",
                    "height": "50px",
                    "color": "#44535A",
                    "font-family": "Roboto, Helvetica, Arial, sans-serif",
                    "font-size": "1rem",
                },
                style_header={
                    "padding": "6px",
                    "color": "white",
                    "background": "#4189AD",
                    "font-family": "Roboto, Helvetica, Arial, sans-serif",
                    "font-size": "1rem",
                    "height": "55px",
                    "border-botton": "0.1px solid #4189AD",
                    "border-right": "0.1px solid #4189AD",
                },
                style_cell={"textAlign": "center"},
                style_data_conditional=style_data_conditional,
                editable=True,
            )
        ]
    )
