import callbacks.recorrido_camion_call
from assets.estilos.css_recorrido import *
from components.listas import nivel_de_llenado
from components.templates import boton, div_comun, selector

layout = div_comun(
    s_panel,
    "",
    # Estado Actual
    [
        div_comun(
            s_panel_botones1,
            "panel_prevision_actual_sup",
            [
                div_comun(
                    s_sub_panel1,
                    "",
                    boton("bot_tabla_actual", "Tabla", s_boton1, n_clicks=1),
                ),
                div_comun(s_sub_panel1, "", boton("bot_mapa_actual", "Mapa",
                                                  s_boton1)),
            ],
        ),
        # Sub panel Izquierdo
        div_comun(
            s_sub_panel_lateral_iz,
            "panel_prevision_actual_iz",
            [
                div_comun(s_div_grafico, "tabla_prevision_actual", "", True),
                div_comun(s_div_grafico, "mapa_prevision_actual", "", True),
            ],
        ),
        # Sub panel derecho
        div_comun(
            s_sub_panel_lateral_der,
            "panel_prevision_actual_der",
            [
                div_comun(s_div_titulo, "", "Nivel de llenado"),
                div_comun(
                    s_selector_distancia,
                    "d_selector_dist",
                    selector(
                        "dist_llenado",
                        {},
                        nivel_de_llenado,
                        value="60%",
                        placeholder="Seleccione",
                    ),
                    False,
                ),
                div_comun(s_div_titulo, "", "Entrada Camión"),
                div_comun(
                    s_panel_botones_recorrido,
                    "",
                    [
                        div_comun(
                            s_sub_panel_boton,
                            "",
                            boton("bot_madrid_en", "Carretera a Madrid",
                                  s_boton_med),
                        ),
                        div_comun(
                            s_sub_panel_boton,
                            "",
                            boton(
                                "bot_murcia_en", "Autovía de Murcia Norte",
                                s_boton_med
                            ),
                        ),
                    ],
                ),
                div_comun(
                    s_panel_botones_recorrido,
                    "",
                    div_comun(
                        s_sub_panel_boton,
                        "",
                        boton("bot_garcia_en", "Autovía de Murcia Sur",
                              s_boton_med),
                    ),
                ),
                div_comun(s_div_titulo, "", "Salida Camión"),
                div_comun(
                    s_panel_botones_recorrido,
                    "",
                    [
                        div_comun(
                            s_sub_panel_boton,
                            "",
                            boton("bot_madrid_sa", "Carretera a Madrid",
                                  s_boton_med),
                        ),
                        div_comun(
                            s_sub_panel_boton,
                            "",
                            boton(
                                "bot_murcia_sa", "Autovía de Murcia Norte",
                                s_boton_med
                            ),
                        ),
                    ],
                ),
                div_comun(
                    s_panel_botones_recorrido,
                    "",
                    div_comun(
                        s_sub_panel_boton,
                        "",
                        boton("bot_garcia_sa", "Autovía de Murcia Sur",
                              s_boton_med),
                    ),
                ),
                div_comun(
                    s_sub_panel_todos,
                    "",
                    boton(
                        "boton_ver_recorrido",
                        "Ver recorrido",
                        s_boton_med,
                        disabled=True,
                    ),
                ),
            ],
        ),
        # Panel recorrido
        div_comun(
            s_div_recorrido,
            "panel_mapa_recorrido_actual",
            [
                div_comun(s_mapa_recorrido_actual, "mapa_recorrido_actual"),
                div_comun(
                    s_sub_panel_ocultar,
                    "",
                    boton(
                        "boton_ocultar_recorrido", "Ocultar recorrido",
                        s_boton_ocultar
                    ),
                ),
            ],
            True,
        ),
    ],
)
