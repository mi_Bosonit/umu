from dash import dcc

from callbacks.alertas_call import alertas
from components.templates import div_comun

s_div = {'float': 'left', 'margin-top': '2%', 'width': '33%',
         'height': '100%'}
s_div2 = {'width': '99%', 'height': '95%',
          'border-radius': '35px 35px 5px 5px', 'background': '#F1F5F7'}
s_mapa = {'float': 'left', 'margin-left': '1%', 'margin-top': '3%',
          'width': '25%', 'height': '90%',
          'border-radius': '35px 35px 5px 5px', 'background': '#F1F5F7'}
s_mapa2 = {'margin-left': '5%', 'margin-top': '2%', 'width': '90%',
           'height': '90%'}

layout = [
    dcc.Loading(id='loading-2',
                children=[div_comun({}, 'loading_contenedores_bateria')],
                type='default', fullscreen=True),
    div_comun({'float': 'left', 'margin-left': '2%', 'width': '70%'}, '',
              [div_comun(s_div, '',
                         div_comun(s_div2,
                                   'alertas_contenedores_bateria_tiempo')),
               div_comun(s_div, '',
                         div_comun(s_div2,
                                   'alertas_contenedores_bateria_distance')),
               div_comun(s_div, '',
                         div_comun(s_div2,
                                   'no_alertas_contenedores_bateria'))]),
    div_comun(s_mapa, '',
              div_comun(s_mapa2, 'mapa_alertas_contenedores_bateria'))]

alertas('contenedores_bateria')
