import callbacks.monitorizacion_contenedores_por_localizacion_call
from assets.estilos.css_monitorizacion_por_localizacion import *
from components.listas import lista_contenedores
from components.templates import div_comun, selector

layout = [
    div_comun(
        s_panel,
        "",
        [
            div_comun(
                s_div_selec,
                "div_selectores",
                div_comun(
                    s_selector,
                    "",
                    selector(
                        "selector_localizacion",
                        {'height': '3vmax'},
                        sorted(lista_contenedores),
                        value="Aulario Giner Rios",
                        placeholder="Seleccione Localización",
                    ),
                ),
            ),
            div_comun(
                {"width": "100%"},
                "",
                [
                    div_comun(s_div_tabla, "tabla_moni_cont_loc"),
                    div_comun(s_titulo_grafico, "div_titulo_grafico", ""),
                    div_comun(
                        s_div_grafico,
                        "div_grafico",
                        div_comun({}, "grafico_llenado_cont_loc"),
                    ),
                ],
            ),
        ],
    )
]
