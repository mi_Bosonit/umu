from .import monitorizacion
from .import monitorizacion_por_localizacion
from .import prevision
from .import alertas_llenado
from .import actualizacion_alertas_llenado
from .import alertas_bateria
from .import actualizacion_alertas_bateria
from .import recorrido_camion
