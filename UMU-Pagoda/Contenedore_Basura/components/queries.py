from datetime import datetime, timedelta

from configuracion import tabla_bbdd

fecha = datetime.now() - timedelta(days = 30)
# Gestion Contenedores Basura
wastecontainermonitoring_ultimo_reg =\
       f"""SELECT DISTINCT ON (description) description, lat,
                  long, 100 - (distance * 100)/175 AS distance,
                  battery_level, temperature, rssi, time_instant,
                  controlled_asset, distance as distace_para_llenado,
                  ocb_id
            FROM {tabla_bbdd}.wastecontainermonitoring
            ORDER BY description, time_instant DESC """

wastecontainermonitoring = f"""SELECT description, lat, long,
                  100 - (distance * 100)/175 AS distance,
                  battery_level, temperature, rssi,
                  time_instant, controlled_asset, ocb_id,
                  distance as distace_para_llenado
           FROM {tabla_bbdd}.wastecontainermonitoring
           WHERE time_instant > '{fecha}'
           ORDER BY description, time_instant DESC"""
