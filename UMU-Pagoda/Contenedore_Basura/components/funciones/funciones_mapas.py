import plotly.graph_objects as go
from pandas import DataFrame

from components.listas import lista_colores


def mapa(df, mode, size, text, zoom=13, lat=0, lon=0):
    if lat == 0:
        lat = df["lat"].iloc[0]
    if lon == 0:
        lon = df["long"].iloc[0]

    fig = go.Figure(
        go.Scattermapbox(
            lat=df["lat"],
            lon=df["long"],
            mode=mode,
            marker=go.scattermapbox.Marker(size=size, color="green"),
            text=df[text],
        )
    )
    fig.update_layout(
        hovermode="closest",
        mapbox_style="open-street-map",
        mapbox={"zoom": zoom, "center": {"lon": lon, "lat": lat}},
        autosize=True,
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
    )
    return fig


def mapa_trayectos(fig, df, trayectos):

    for x, trayecto in enumerate(trayectos):
        df_recorrido = DataFrame(columns=["description", "lat", "long"])
        df_recorrido.loc[0] = df.loc[x]
        i = 1

        for localizacion in trayecto:
            df_recorrido.loc[i] = ["", localizacion[0], localizacion[1]]
            i += 1

        df_recorrido.loc[i] = df.loc[x + 1]

        fig.update_layout(
            legend={
                "x": 1,
                "y": 1,
                "font": {
                    "size": 13,
                    "family": """Roboto,
                                 Helvetica,
                                 Arial,
                                 sans-serif""",
                },
            }
        )

        fig.update_layout(legend=dict(yanchor="top", y=1,
                                      xanchor="left", x=0.0))

        fig.add_traces(
            go.Scattermapbox(
                lat=df_recorrido["lat"],
                lon=df_recorrido["long"],
                mode="lines",
                marker=go.scattermapbox.Marker(size=100,
                                               color=lista_colores[x]),
                name=f"""{x+1}.{
                    df['description'].iloc[x]
                    }-{df['description'].iloc[x+1]}""",
            )
        ).data
    return fig
