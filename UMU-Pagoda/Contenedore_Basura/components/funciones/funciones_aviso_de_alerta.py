from datetime import datetime

from pandas import read_csv
from pytz import timezone

from components.listas import contenedores_basura_por_descripcion
from components.queries import *


def alertas_por_tiempo(df):
    dispositivos_a_eliminar = []

    for i in range(len(df)):
        horas = int(
            (
                (
                    datetime.now().astimezone(timezone("Europe/Madrid"))
                    - df.iloc[i]["time_instant"]
                ).total_seconds()
            )
            / 3600
        )
        if horas > 6:
            dispositivos_a_eliminar.append(i)

    df = df.drop(dispositivos_a_eliminar, axis=0)
    return df


def df_valores_alertas_contenedores(indicador, df):
    if indicador == "distance":
        df_valores = read_csv("Data/valores_alertas/llenado.csv")[
            ["Dispositivo", "Min"]
        ]
    if indicador == "bateria":
        df_valores = read_csv("Data/valores_alertas/bateria.csv")[
            ["Dispositivo", "Min"]
        ]

    description = [
        contenedores_basura_por_descripcion[dispositivo]
        for dispositivo in df_valores["Dispositivo"]
    ]
    df_valores["description"] = description

    if indicador == "distance":
        return df_valores.merge(df, on="description")[
            ["Dispositivo", "Min", "description", "distace_para_llenado"]
        ]
    if indicador == "bateria":
        return df_valores.merge(df, on="description")[
            ["Dispositivo", "Min", "description", "battery_level"]
        ]


def alertas_por_valores_contenedores(df, df_alertas, indicador):

    df = df_valores_alertas_contenedores(indicador, df)

    if indicador == "distance":
        loc_df_alertas = df_alertas.shape[0]

        for loc in range(len(df)):
            if df.iloc[loc]["distace_para_llenado"] <= df.iloc[loc]["Min"]:
                df_alertas.loc[loc_df_alertas] = (
                    ["Contenedores Basura", df.iloc[loc]["Dispositivo"]]
                    + list(df.iloc[loc][["description"]])
                    + [
                        f"""El contenedor está a {
                            df.iloc[loc]["distace_para_llenado"]
                            } cm. de llenarse"""
                    ]
                )
                loc_df_alertas += 1

    if indicador == "bateria":
        loc_df_alertas = df_alertas.shape[0]

        for loc in range(len(df)):
            if df.iloc[loc]["battery_level"] <= df.iloc[loc]["Min"]:
                df_alertas.loc[loc_df_alertas] = (
                    ["Contenedores Basura", df.iloc[loc]["Dispositivo"]]
                    + list(df.iloc[loc][["description"]])
                    + [f'Nivel de bateria a {df.iloc[loc]["battery_level"]}']
                )
                loc_df_alertas += 1

    return df_alertas
