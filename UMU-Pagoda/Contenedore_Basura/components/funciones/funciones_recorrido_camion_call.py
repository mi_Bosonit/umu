from json import loads

from pandas import DataFrame
from requests import request

from components.funciones.funciones_monitorizacion import tratar_df

on = "3px solid #5BB6CE"
off = "1px solid #8380F7"


def cambiar_estado_boton(boton1, boton2, boton3):
    boton1["border"] = on
    boton2["border"] = off
    boton3["border"] = off
    return [boton1, boton2, boton3]


def df_actual(df, distance=70):
    df = tratar_df(df)
    return df[df["distance"] > distance].sort_values(by="Llenado",
                                                     ascending=False)


def entrada_salida(entradas, nom_entradas, coordenadas):
    for i, en in enumerate(entradas):
        if en["border"] == on:
            return coordenadas[nom_entradas[i]]


def obtener_recorrido(origin, destination, df_act):
    waypoints = "".join(
        [
            f"{lat},{long}|"
            for lat, long in zip(list(df_act["lat"]), list(df_act["long"]))
        ]
    )[:-1]
    url = f"""https://maps.googleapis.com/maps/api/directions/json?
              &origin={origin}&destination={destination}
              &waypoints=optimize:true|{waypoints}
              &mode=driving&key=AIzaSyBM7Dv3-VcnnnbtQoVsiYhncZj9q-Yr83A"""

    response = request("GET", url, headers={}, data={})
    recorrido = loads(response.text)

    lat_en, long_en = origin.split(",")
    lat_sa, long_sa = destination.split(",")
    lat_en, long_en, lat_sa, long_sa = [
        float(x) for x in (lat_en, long_en, lat_sa, long_sa)
    ]

    df = DataFrame(
        data={"description": ["Entrada"], "lat": [lat_en], "long": [long_en]}
    )

    for i, cont in enumerate(recorrido["routes"][0]["waypoint_order"]):
        df.loc[i + 1] = [
            " ".join(
                df_act.iloc[cont]["controlled_asset"].split("-")[1:]
                ).capitalize(),
            df_act.iloc[cont]["lat"],
            df_act.iloc[cont]["long"],
        ]

    df.loc[df.shape[0]] = ["Salida", lat_sa, long_sa]

    trayectos = []
    for leg in recorrido["routes"][0]["legs"]:
        etapa = []
        for step in leg["steps"]:
            etapa.append([step["end_location"]["lat"],
                          step["end_location"]["lng"]])
        trayectos.append(etapa)

    return df, trayectos
