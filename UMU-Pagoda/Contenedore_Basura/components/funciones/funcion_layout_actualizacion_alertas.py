import dash_bootstrap_components as dbc
from dash import callback, dash_table
from dash.dependencies import Input, Output
from pandas import DataFrame, read_csv, concat

from components.templates import boton, div_comun

s_table = {
    "box-sizing": "inherit",
    "letter-spacing": "0.0001em",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}
s_data = {
    "width": "33%",
    "padding": "6px",
    "align-items": "center",
    "border-right": "0.1px solid white",
    "border-left": "0.1px solid white",
    "height": "50px",
    "color": "#44535A",
}
s_header = {
    "padding": "6px",
    "color": "white",
    "background": "#4189AD",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "height": "55px",
    "border-botton": "0.1px solid #4189AD",
    "border-right": "0.1px solid #4189AD",
}
s_data_conditional = [
    {
        "if": {"column_id": ["Dispositivo"], "column_id": ["Dispositivo"]},
        "font-size": "1rem",
        "width": "50%",
    },
    {"if": {"column_id": ["Min"], "column_id": ["Min"]}, "width": "25%"},
]


def tabla_depuradora(df, id):
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=[
                    dict(id="Dispositivo", name="Dispositivo", editable=False),
                    dict(id="Min", name="Min", type="numeric"),
                ],
                id=f"tabla_{id}",
                page_size=9,
                style_table=s_table,
                style_data=s_data,
                style_header=s_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=s_data_conditional,
                editable=True,
            )
        ]
    )


color_boton = "#add8e6"
off = "1px solid #8380F7"
s_general = {
    "margin-top": "3%",
    "margin-left": "2%",
    "width": "90%",
    "height": "90%"
}
s_boton = {
    "margin-left": "40%",
    "margin-top": "5%",
    "width": "25%",
    "height": "20%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": off,
}
s_panel_pop_up = {
    "position": "absolute",
    "left": "40%",
    "top": "20%",
    "width": "20%",
    "height": "20%",
    "background": "#F1F5F7",
    "border-radius": "50px 50px 50px 50px",
}
s_div_pop_up = {
    "margin-top": "10%",
    "font-size": "1.2rem",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "text-align": "center",
}


def layout_actualizacion_alertas(id):

    layout = div_comun(
        s_general,
        "",
        [
            tabla_depuradora(read_csv(f"Data/valores_alertas/{id}.csv"), id),
            div_comun({}, f"table-editing-simple-output_{id}"),
            div_comun(
                s_panel_pop_up,
                f"pop_up_{id}",
                [
                    div_comun(s_div_pop_up, "", "Alerta actualizada"),
                    boton(f"boton_aceptar_{id}", "Aceptar", s_boton),
                ],
                True,
            ),
        ],
    )

    @callback(Output(f'tabla_{id}', 'data'), Input('url', 'pathname'))
    def df(pathname):
        path = f'Data/valores_alertas/{id}.csv'
        return read_csv(path).to_dict('records')

    @callback(
        [
            Output(f"pop_up_{id}", "hidden"),
            Output(f"boton_aceptar_{id}", "n_clicks")
        ],
        [
            Input(f"tabla_{id}", "data"),
            Input(f"tabla_{id}", "columns"),
            Input(f"boton_aceptar_{id}", "n_clicks"),
        ],
        prevent_initial_call=True,
    )
    def display_output(rows, columns, b_aceptar):
        if b_aceptar:
            return [True, 0]

        path = f'Data/valores_alertas/{id}.csv'
        df = read_csv(path).drop(["Unnamed: 0"], axis=1)

        df2 = DataFrame(rows, columns=[c['name'] for c in columns])

        if concat([df, df2]).drop_duplicates(keep=False).shape[0] > 0:
            df2.to_csv(f'Data/valores_alertas/{id}.csv')
            return [False, 0]

        return [True, 0]

    return layout
