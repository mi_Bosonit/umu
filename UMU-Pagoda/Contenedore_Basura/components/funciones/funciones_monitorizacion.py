from datetime import datetime

from pandas import read_json

from components.listas import contenedores_basura_por_dispositivo


def boton_on_off(state_on, state_off):
    state_on["border"] = "3px solid #5BB6CE"
    for state in state_off:
        state["border"] = "2px solid #add8e6"


def tratar_df(df, mapa=False):

    df["Localizacion"] = [
        contenedores_basura_por_dispositivo[x] for x in df["description"]
    ]

    if not mapa:
        df["time_instant"] = [
            datetime.fromtimestamp(i / 1000) for i in df["time_instant"]
        ]
        df["Hora"] = [f"{i.time()}" for i in df["time_instant"]]
        df["Fecha"] = [i.date() for i in df["time_instant"]]
        df["Dispositivo"] = df["description"]
        df["Llenado"] = df["distance"]
        df["Bateria"] = df["battery_level"]
        df["Nivel de Llenado"] = df["distance"] / 100
        df["Nivel de Batería"] = df["battery_level"] / 100
    return df


def df_seleccionar_registro(s_sel_hora, s_sel_dia, data):
    s_sel_dia2 = s_sel_dia

    if s_sel_hora == "23:00:00":
        fecha = s_sel_dia.split("-")
        s_sel_dia2 = f"""{fecha[0]}-{fecha[1]}-{int(fecha[2]) + 1}"""
        s_sel_hora2 = "00:00:00"
    else:
        s_sel_hora2 = f"""{int(s_sel_hora.split(':')[0]) + 1}:00:00"""

    df = read_json(data["wastecontainermonitoring"])[
        ["description", "distance", "lat", "long",
         "battery_level", "time_instant"]
    ]

    df = tratar_df(df.sort_values("distance", ascending=False))
    df = df[
        (df["time_instant"] > f"{s_sel_dia} {s_sel_hora}")
        & (df["time_instant"] < f"{s_sel_dia2} {s_sel_hora2}")
    ]
    return df
