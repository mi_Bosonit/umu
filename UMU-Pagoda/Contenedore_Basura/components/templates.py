import dash_bootstrap_components as dbc
import plotly.express as px
from dash import dash_table, dcc, html

boton_blanco = "white"
azul = "#BFEBF9"
barra_bot = "#2791B4"
style_barra_boton = {
    "margin-left": "0.5%",
    "margin-right": "0.5%",
    "width": "99%",
    "display": "block",
    "border-radius": "10px",
    "border": "2px solid #1c2b36",
    "background": azul,
    "font": "bold 180% monospace",
}


def div_comun(style, id="", children="", hidden=False):
    return html.Div(style=style, children=children, id=id, hidden=hidden)


def boton(id, nombre, style, hidden=False, disabled=False, n_clicks=0):
    return html.Button(
        style=style,
        id=id,
        n_clicks=n_clicks,
        hidden=hidden,
        children=nombre,
        disabled=disabled,
    )


def div_barra_boton(style, id, nombre):
    return div_comun(style, "", boton(id, nombre, style_barra_boton))


def selector(id, style, options=[], value="", placeholder=""):
    return dcc.Dropdown(
        style=style, id=id, options=options, value=value,
        placeholder=placeholder
    )


# Tablas
def tabla(
    id, df, columnas_tabla, s_data_conditional, page_size=18,
    row_selectable="multi"
):
    s_table = {
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "border": "0.1px solid white",
        "overflow-x": "auto",
    }
    s_data = {
        "padding": "6px",
        "align-items": "center",
        "width": "8vmax",
        "min-width": "8vmax",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.15vmax",
    }
    s_header = {
        "padding": "6px",
        "color": "#143CB8",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.25vmax",
        "border": "0.1px solid white",
    }
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columnas_tabla,
                id=id,
                row_selectable=row_selectable,
                page_size=page_size,
                selected_rows=[],
                style_table=s_table,
                style_data=s_data,
                style_header=s_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=s_data_conditional,
            )
        ]
    )


# line
def line(df, x, y, title="", yaxis_title="", xaxis_title="", mode="lines"):
    fig = px.line(
        df,
        x=x,
        y=y,
        title=title,
        color_discrete_sequence=["#4189AD", "#88dd44", "#CE7B54"],
    )

    fig.update_layout(
        title={
            "font": {
                "family": "Roboto, Helvetica, Arial, sans-serif",
                "color": "black",
                "size": 25,
            }
        },
        autosize=True,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        xaxis={"showgrid": False},
        yaxis={"showgrid": False},
        plot_bgcolor="white",
        margin=dict(t=60, l=0, b=0, r=0),
    )
    fig.update_traces(mode=mode)
    return fig
