color_boton = "#add8e6"
color_fondo = "#2A465E"

s_panel = {"margin-top": "0.4%", "width": "99.9%", "height": "99%"}
s_div_selec = {"width": "100%", "height": "10%"}
s_selector = {
    "float": "left",
    "margin-left": "2%",
    "margin-top": "0.7%",
    "width": "27vmax",
    "height": "3.2vmax",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.4vmax",
    "background": color_boton,
}
s_div_tabla = {"margin-left": "3%", "margin-top": "2vmax", "width": "80%"}
s_titulo_grafico = {
    "margin-top": "1%",
    "margin-left": "15%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "2vmax",
}
s_div_grafico = {"margin-left": "3%", "margin-top": "1vmax", "width": "95%"}
