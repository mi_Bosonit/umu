from datetime import datetime, timedelta

from dash import dash_table

naranja = "#FDA203"
rojo = "#FD0703"

percentage = dash_table.FormatTemplate.percentage(0)

# Nivel de llenado
llenado = [
    {
        "if": {"column_id": ["Nivel de Llenado"],
               "column_id": ["Nivel de Llenado"]},
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "width": "13vmax",
        "min-width": "13vmax",
        "textAlign": "right",
    }
]

for x in range(81, 101):
    llenado.append(
        {
            "if": {
                "filter_query": "{{Llenado}}={}".format(x),
                "column_id": ["Nivel de Llenado"],
            },
            "font-weight": "600",
            "background": """linear-gradient(80deg, #3ed21d 0%,
                              #3ed21d 59%, #e8a809 61%, #e8a809 80%,
                              red {max_bound_percentage}%, white {white}%)
                           """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )

for x in range(61, 81):
    llenado.append(
        {
            "if": {
                "filter_query": "{{Llenado}}={}".format(x),
                "column_id": ["Nivel de Llenado"],
            },
            "background": """linear-gradient(80deg, #3ed21d 0%, #3ed21d 59%,
                          #e8a809 61%, #e8a809 {max_bound_percentage}%,
                          white {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )

for x in range(61):
    llenado.append(
        {
            "if": {
                "filter_query": "{{Llenado}}={}".format(x),
                "column_id": ["Nivel de Llenado"],
            },
            "background": """linear-gradient(80deg, #3ed21d 0%,
                          #3ed21d {max_bound_percentage}%, white {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )

# Estilo Bateria
bateria = [
    {
        "if": {"column_id": ["Nivel de Batería"],
               "column_id": ["Nivel de Batería"]},
        "textAlign": "right",
        "width": "13vmax",
        "min-width": "13vmax",
    },
    {
        "if": {"filter_query": "{Bateria}=100",
               "column_id": ["Nivel de Batería"]},
        "background": """linear-gradient(80deg, red 0%, #e8a809 25%,
                      #e8a809 50%, #3ed21d 51%, #3ed21d 100%) """,
    },
]

for x in range(51, 100):
    bateria.append(
        {
            "if": {
                "filter_query": "{{Bateria}}={}".format(x),
                "column_id": ["Nivel de Batería"],
            },
            "background": """linear-gradient(80deg, red 0%, #e8a809 25%,
                          #e8a809 50%, #3ed21d 51%,
                          #3ed21d {max_bound_percentage}%,
                          white {white}%)""".format(
                max_bound_percentage=x - 1, white=x
            ),
        }
    )

for x in range(26, 51):
    bateria.append(
        {
            "if": {
                "filter_query": "{{Bateria}}={}".format(x),
                "column_id": ["Nivel de Batería"],
            },
            "background": """linear-gradient(80deg, red 0%, #e8a809 25%,
                          #e8a809 {max_bound_percentage}%, white {white}%)
                       """.format(
                max_bound_percentage=x - 1, white=x
            ),
        }
    )

for x in range(0, 26):
    bateria.append(
        {
            "if": {
                "filter_query": "{{Bateria}}={}".format(x),
                "column_id": ["Nivel de Batería"],
            },
            "background": """linear-gradient(80deg, red 0%,
                          #e8a809 {max_bound_percentage}%, white {white}%)
                       """.format(
                max_bound_percentage=x - 1, white=x
            ),
        }
    )

column_monitorizacion_loc = [
    dict(id="Dispositivo", name="Dispositivo"),
    dict(
        id="Nivel de Llenado",
        name="Nivel de Llenado",
        type="numeric",
        format=percentage,
    ),
    dict(
        id="Nivel de Batería",
        name="Nivel de Batería",
        type="numeric",
        format=percentage,
    ),
    dict(id="Temperatura", name="Temperatura"),
    dict(id="Fecha", name="Fecha"),
    dict(id="Hora", name="Hora"),
]


def style_data_conditional_monitorizacion_por_loc():
    style_data_conditional_monitorizacion = []
    # Estilo Localizacion
    style_data_conditional_monitorizacion.append(
        {
            "if": {"column_id": ["Dispositivo"], "column_id": ["Dispositivo"]},
            "width": "13vmax",
            "min-width": "13vmax",
            "border-right": "1px solid rgba(36, 41, 46, 0.12)",
            "color": "white",
            "background": "linear-gradient(100deg, #394F93, #052073) padding-box",
        }
    )

    return style_data_conditional_monitorizacion + llenado + bateria


column_monitorizacion = [
    dict(id="Localizacion", name="Dispositivo", selectable=True),
    dict(
        id="Nivel de Llenado",
        name="Nivel de Llenado",
        type="numeric",
        format=percentage,
    ),
    dict(
        id="Nivel de Batería",
        name="Nivel de Batería",
        type="numeric",
        format=percentage,
    ),
    dict(id="Fecha", name="Fecha"),
    dict(id="Hora", name="Hora"),
]


def style_data_conditional_monitorizacion():

    style_data_conditional_monitorizacion = []

    # Estilo Horas
    colores = [naranja, rojo]
    for i, x in enumerate([3, 5]):
        style_data_conditional_monitorizacion.append(
            {
                "if": {
                    "filter_query": "{{Hora}} < {}".format(
                        (datetime.now() - timedelta(hours=x)).time()
                    ),
                    "column_id": ["Hora"],
                },
                "color": colores[i],
                "width": "2vmax",
                "min-width": "2vmax",
            }
        )
    # Estilo Fecha
    style_data_conditional_monitorizacion.append(
        {
            "if": {
                "filter_query":
                    "{{Fecha}} != {}".format(datetime.now().date()),
                "column_id": ["Fecha", "Hora"],
            },
            "color": rojo,
        }
    )

    # Estilo Localizacion
    style_data_conditional_monitorizacion.append(
        {
            "if": {"column_id": ["Localizacion"],
                   "column_id": ["Localizacion"]},
            "width": "13vmax",
            "min-width": "13vmax",
            "border-right": "1px solid rgba(36, 41, 46, 0.12)",
            "color": "white",
            "background": "linear-gradient(100deg, #394F93, #052073) padding-box",
        }
    )

    return style_data_conditional_monitorizacion + llenado + bateria
