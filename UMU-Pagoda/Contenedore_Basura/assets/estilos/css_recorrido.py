color_boton = "#add8e6"
off = "1px solid #add8e6"
color_boton = "#add8e6"
color_fondo = "#2A465E"

s_panel = {
    "margin-top": "0.4%",
    "width": "100%",
    "height": "97%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_panel_botones1 = {"width": "90%", "height": "12%"}
s_sub_panel1 = {
    "float": "left",
    "margin-left": "0.8%",
    "margin-top": "1%",
    "width": "36%",
    "height": "80%",
}
s_boton1 = {
    "width": "100%",
    "height": "70%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.25vmax",
    "border": off,
}

s_sub_panel_lateral_iz = {
    "float": "left",
    "margin-top": "0.1%",
    "width": "62%",
    "height": "85%",
}
s_div_grafico = {"margin-left": "4%", "width": "98%", "height": "90%"}

s_sub_panel_lateral_der = {
    "float": "left",
    "margin-left": "4%",
    "width": "34%",
    "height": "86%",
}
s_div_titulo = {
    "margin-top": "2%",
    "margin-left": "5%",
    "height": "5%",
    "width": "90%",
    "font-size": "1.3vmax",
}
s_selector_distancia = {
    "margin-top": "2%",
    "margin-left": "5%",
    "width": "30%",
    "height": "3vmax",
    "font-size": "1.2vmax",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "background": color_boton,
}
s_panel_botones_recorrido = {
    "margin-left": "5%",
    "margin-top": "1%",
    "width": "90%",
    "height": "8%",
}
s_sub_panel_boton = {
    "float": "left",
    "margin-top": "0.3%",
    "margin-left": "0.5%",
    "width": "49%",
    "height": "83%",
}
s_boton_med = {
    "width": "98%",
    "height": "85%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.25vmax",
    "border": off,
}

s_sub_panel_todos = {
    "margin-left": "5%",
    "margin-top": "1.2%",
    "width": "85%",
    "height": "8%",
}
s_boton_todos = {
    "width": "100%",
    "height": "85%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.3vmax",
    "border": off,
}

s_div_recorrido = {
    "position": "absolute",
    "left": "0%",
    "top": "1%",
    "width": "98%",
    "height": "95%",
    "background": "white",
}
s_mapa_recorrido_actual = {
    "margin-left": "1%",
    "margin-top": "3%",
    "width": "97%",
    "height": "85%",
}
s_sub_panel_ocultar = {
    "margin-left": "5%",
    "margin-top": "1%",
    "width": "85%",
    "height": "7%",
}
s_boton_ocultar = {
    "width": "100%",
    "height": "90%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.3vmax",
    "border": off,
}
