color_boton = "#add8e6"
color_fondo = "#2A465E"

s_panel = {
    "margin-top": "0.4%",
    "width": "99.9%",
    "height": "99%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}
s_panel_botones = {"width": "100%", "height": "10%"}
s_botones = {
    "float": "left",
    "margin-top": "1.4%",
    "margin-left": "3%",
    "width": "47%",
    "height": "99%",
}
s_boton = {
    "width": "95%",
    "height": "60%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.4vmax",
}
s_boton_todos = {
    "margin-left": "40%",
    "width": "40%",
    "height": "100%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.4vmax",
    "border": "1px solid #8380F7",
}

s_selectores = {
    "float": "left",
    "margin-top": "1%",
    "margin-left": "3%",
    "width": "47%",
    "height": "99%",
}
s_selector_iz = {
    "float": "left",
    "width": "17.5vmax",
    "height": "3.2vmax",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1.3vmax",
    "background": color_boton,
}
s_selector = {
    "float": "left",
    "margin-left": "3%",
    "width": "17.5vmax",
    "height": "3.2vmax",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1.3vmax",
    "background": color_boton,
}

s_graficos = {"margin-left": "1%", "width": "98.8%", "height": "80%"}
s_grafico = {"float": "left", "width": "65%", "height": "99%"}
s_grafico_der = {
    "float": "right",
    "margin-right": "3%",
    "width": "30%",
    "height": "99%",
}
s_sub_panel = {"float": "left", "width": "50%", "height": "100%"}

s_div_tabla = {"width": "100%", "height": "90%"}
s_div_mapa = {"margin-left": "6%", "margin-top": "11%", "width": "70%",
              "height": "45%"}
s_mapa = {"margin-top": "-10%", "width": "25vmax", "height": "20vmax"}
