# Caso de uso Gestión de contenedores de basura y papeleras

## Introducción

El objetivo buscado en este caso de uso es llevar un control sobre el estado de los contenedores de basura y calcular una ruta optimizada para el vaciado de los mismos. 

## Despliegue


## Funcionamiento

#### Interfaz para monitorización

La monitorización sobre el estado de los contenedores de basura se divide en dos paneles:

__Monitorización de Contenedores__. Mediante una tabla y un mapa (Ilustración 1), se indica el estado -por dispositivo- de los distintos sensores colocados en contenedores. En la tabla también se indica la fecha y la hora del último registro recogido.
En este panel se nos ofrece la posibilidad de seleccionar los contenedores que queramos que aparezcan en el mapa.

Para acceder a este panel utilizaremos la siguiente ruta

http://localhost:8050/dash/contenedores_basura_monitorizacion

Ilustración 1
![image.png](assets/imagenes/image.png)

Las variables monitorizadas en la tabla son:
* Dispositivo: Identificador del dispositivo.
* Nivel de llenado: Indicador en porcentaje del nivel de llenado del contenedor.
* Nivel de batería: Indicador en porcentaje del nivel de batería del sensor.
* Fecha: Fecha del último registro enviado por el sensor.
* Hora: Hora del último registro enviado por el sensor.

En este panel también tenemos la posibilidad de ver los registros recogidos en una fecha y hora en concreto dentro del mes en curso  (Ilustración 2)

Ilustración 2
![image2.png](assets/imagenes/image2.png)

__Monitorización Contenedores por localización.__

En este panel podemos ver los registros recogidos en el mes en curso para un determinado dispositivo, que seleccionara el usuario (Ilustración 3).

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/contenedores_basura_monitorizacion_por_localizacion

Ilustración 3
![image3.png](assets/imagenes/image3.png)

Las variables monitorizadas en la tabla son:
* Dispositivo: Identificador del dispositivo.
* Nivel de llenado: Indicador en porcentaje del nivel de llenado del contenedor.
* Nivel de batería: Indicador en porcentaje del nivel de batería del sensor.
* Temperatura: Temperatura en el interior del contenedor.
* Fecha: Fecha del registro enviado por el sensor.
* Hora: Hora del registro enviado por el sensor.

También se muestra una gráfica con los datos de nivel de llenado y temperatura en el contenedor para el dispositivo seleccionado y mes en curso.


__Se incluyen cuatro paneles auxiliares.__ para poder monitorizar y configurar alertas para el nivel de llenado y nivel de batería de los contenedores.

Al panel para configurar las alertas de llenado (Ilustración 4) accederemos a través de la siguiente ruta

http://localhost:8050/dash/contenedores_basura_actualizacion_alertas_llenado

Ilustración 4 
![image4.png](assets/imagenes/image4.png)

Al panel para monitorizar las alertas de llenado (Ilustración 5) accederemos a través de la siguiente ruta

http://localhost:8050/dash/contenedores_basura_alertas_llenado

Ilustración 5
![image5.png](assets/imagenes/image5.png)

Al panel para configurar las alertas de nivel de batería (Ilustración 6) accederemos a través de la siguiente ruta

http://localhost:8050/dash/contenedores_basura_actualizacion_alertas_bateria

Ilustración 6
![image6.png](assets/imagenes/image6.png)

Al panel para monitorizar las alertas de de nivel de batería (Ilustración 7) accederemos a través de la siguiente ruta

http://localhost:8050/dash/contenedores_basura_alertas_bateria

Ilustración 7
![image7.png](assets/imagenes/image7.png)


__Para el calculo de la ruta de recogida de los contenedores__ tenemos un único panel (Ilustración 8).

Para acceder a este panel utilizaremos la siguiente ruta:

http://localhost:8050/dash/contenedores_basura_recorrido_camion

Ilustración 8
![image8.png](assets/imagenes/image8.png)

En este panel tenemos la opción de visualizar una tabla (Ilustración 8) o el mapa con la localización (Ilustración 9) de los contenedores que superen un umbral (seleccionado por el usuario) de nivel de llenado.

Ilustración 9
![image9.png](assets/imagenes/image9.png)

Para poder ver la ruta optima de recogida de esos contenedores, habrá que seleccionar el lugar de llegada y salida del camión. En ese momento se activará el botón 'Ver recorrido'. Pulsando este botón podremos ver la ruta óptima para la recogida (Ilustracion 10).

Ilustración 10
![image10.png](assets/imagenes/image10.png)

#### Interfaz para predicción
En este caso tambien disponemos de 2 paneles.

__Al primer panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_admin

Desde este panel podemos importar los modelos creados en la Iaaas (Ilustración 11)

Ilustracion 11
![image11.png](assets/imagenes/image11.png)

__Al segundo panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_user

Desde este panel podemos monitorizar los modelos que hayamos importado (Ilustración 12)

Ilustración 12
![image12.png](assets/imagenes/image12.png)


