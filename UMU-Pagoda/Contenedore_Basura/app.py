import dash
from dash import dcc
from flask import Flask

import callbacks.aviso_de_alertas
import pages.prevision.interval_modelos
from app_call import *
from components.templates import div_comun
from configuracion import raiz

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
server = Flask(__name__)
app = dash.Dash(server=server, url_base_pathname=f'/{raiz}/')
app.config.suppress_callback_exceptions = True

style_div = {
    "position": "absolute",
    "left": "0px",
    "top": "0px",
    "width": "100%",
    "height": "100%",
}
s_loading = {"position": "absolute", "top": "20%", "left": "40%"}

app.layout = div_comun(
    {},
    "",
    [
        dcc.Interval(id="interval", interval=1800000, n_intervals=0),
        dcc.Interval(
            id="interval_modelos_contenedores", interval=1800000, n_intervals=0
        ),
        dcc.Location(id="url"),
        dcc.Store(id="store"),
        # Panel donde irán las diferentes paginas
        div_comun(
            s_loading,
            "",
            dcc.Loading(
                id="loading-2",
                children=[div_comun({}, "loading_contenedores")],
                type="default",
                fullscreen=True,
            ),
        ),
        div_comun(style_div, "page_content"),
    ],
)

if __name__ == "__main__":
    app.run_server(debug=True)
