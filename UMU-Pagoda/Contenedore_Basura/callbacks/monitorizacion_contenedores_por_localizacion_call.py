from contextlib import suppress
from datetime import datetime, timedelta

import plotly.express as px
from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from assets.estilos.tabla_monitorizacion import (
    column_monitorizacion_loc, style_data_conditional_monitorizacion_por_loc)
from components.funciones.funciones_monitorizacion import tratar_df
from components.listas import contenedores_basura_por_descripcion
from components.templates import div_comun, tabla

global df_dispositivo


# Tabla
@callback(
    Output("tabla_moni_cont_loc", "children"),
    [Input("selector_localizacion", "value"), Input("store", "data")],
    State("store", "data"),
    prevent_initial_call=True,
)
def desplegar_tabla(b_loc, b_data, data):

    tabla_cont = ""

    if b_loc:
        global df_dispositivo
        df_dispositivo = read_json(data["wastecontainermonitoring"])[
            ["time_instant", "description", "distance",
             "battery_level", "temperature"]]
        df_dispositivo =\
            df_dispositivo[df_dispositivo["description"] ==
                           contenedores_basura_por_descripcion[b_loc]]

        df_dispositivo = df_dispositivo[
            df_dispositivo["time_instant"]
            > (datetime.now() - timedelta(days=15)).timestamp() * 1000
        ]

        df_dispositivo = tratar_df(df_dispositivo).sort_values(
            "time_instant", ascending=False
        )

        df_dispositivo["Temperatura"] = [
            f"{temperatura}ºC" for temperatura in df_dispositivo["temperature"]
        ]

        df_dispositivo = df_dispositivo[
            [
                "Dispositivo",
                "time_instant",
                "description",
                "temperature",
                "Temperatura",
                "Llenado",
                "Bateria",
                "Nivel de Llenado",
                "Nivel de Batería",
                "Hora",
                "Fecha",
            ]
        ]

        tabla_cont = div_comun(
            {
                "margin-left": "5%",
                "font-size": "1rem",
                "font-family": "Roboto, Helvetica, Arial, sans-serif",
            },
            "",
            "No hay registros para la localización seleccionada",
        )

        if df_dispositivo.shape[0] > 0:
            tabla_cont = tabla(
                "tabla_monitorizacion_contenedores_loc",
                df_dispositivo,
                column_monitorizacion_loc,
                style_data_conditional_monitorizacion_por_loc(),
                12,
                False,
            )
    return tabla_cont


# Grafico
@callback(
    [
        Output("grafico_llenado_cont_loc", "children"),
        Output("div_titulo_grafico", "children"),
    ],
    Input("tabla_moni_cont_loc", "children"),
    prevent_initial_call=True,
)
def desplegar_tabla(b_children):

    grafico = ""
    titulo = ""

    if b_children:
        global df_dispositivo
        while True:
            with suppress(Exception):
                df = df_dispositivo[["time_instant", "temperature", "Llenado"]]
                break

        if df.shape[0] > 0:

            df["time_instant"] =\
                df["time_instant"].apply(
                    lambda x: f"{x.date()} {x.time().hour}:00:00")

            fig = px.bar(
                df,
                x="time_instant",
                y="Llenado",
                title="",
                color_discrete_sequence=["#2A465E"] * len(df),
            )
            fig.update_layout(
                title={
                    "font": {
                        "family": """Roboto, Helvetica,
                                              Arial, sans-serif""",
                        "color": "black",
                        "size": 25,
                    }
                },
                yaxis_title="",
                xaxis_title="",
                xaxis={"showgrid": False},
                yaxis={"showgrid": False},
                plot_bgcolor="white",
                margin=dict(t=60, l=0, b=0, r=0),
            )
            fig.update_traces(
                base=dict(line=dict(color="green", width=1)),
                showlegend=True,
                name="Nivel de Llenado",
            )

            fig.add_traces(
                px.scatter(df, x="time_instant", y="temperature")
                .update_traces(
                    showlegend=True,
                    name="Temperatura",
                    mode="markers+lines",
                    marker=dict(color="#B9F208", size=5),
                    line=dict(color="#CAEE05", width=2),
                )
                .data
            )

            grafico = dcc.Graph(figure=fig)
            titulo = """Nivel de llenado (%) y temperatura
                        interior(ºC) del contenedor"""

    return [grafico, titulo]
