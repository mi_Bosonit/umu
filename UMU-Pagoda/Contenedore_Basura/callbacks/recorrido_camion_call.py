from dash import callback, dcc, html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from pandas import DataFrame, read_json

from assets.estilos.tabla_monitorizacion import (
    column_monitorizacion, style_data_conditional_monitorizacion)
from components.funciones.funciones_mapas import mapa, mapa_trayectos
from components.funciones.funciones_recorrido_camion_call import (
    cambiar_estado_boton, df_actual, entrada_salida, obtener_recorrido)
from components.listas import (contenedores_basura_por_dispositivo,
                               entradas_camion, salidas_camion)
from components.templates import tabla

on = "3px solid #5BB6CE"
off = "2px solid #add8e6"
s_tabla_cont = {
    "margin-left": "50px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}

global df_tabla_actual
df_tabla_actual = DataFrame()


# Seleccionar Boton Tabla/Mapa
@callback(
    [
        Output("bot_tabla_actual", "n_clicks"),
        Output("bot_mapa_actual", "n_clicks"),
        Output("bot_tabla_actual", "style"),
        Output("bot_mapa_actual", "style"),
    ],
    [
        Input("bot_tabla_actual", "n_clicks"),
        Input("bot_mapa_actual", "n_clicks")
    ],
    [
        State("bot_tabla_actual", "style"),
        State("bot_mapa_actual", "style")
    ],
)
def seleccionar(b_tabla, b_mapa, s_tabla, s_mapa):

    s_tabla["border"] = s_mapa["border"] = off
    if b_tabla:
        s_tabla["border"] = on
    if b_mapa:
        s_mapa["border"] = on
    return [0, 0, s_tabla, s_mapa]


# ocultar/ver paneles tabla/mapa
@callback(
    [
        Output("tabla_prevision_actual", "hidden"),
        Output("mapa_prevision_actual", "hidden"),
    ],
    [
        Input("bot_tabla_actual", "n_clicks"),
        Input("bot_mapa_actual", "n_clicks"),
        Input("mapa_recorrido_actual", "children"),
    ],
    [State("bot_tabla_actual", "style"), State("bot_mapa_actual", "style")],
)
def seleccionar(b_tabla, b_mapa, b_recorrido, s_tabla, s_mapa):

    tabla = mapa = True

    if s_tabla["border"] == on and len(b_recorrido) == 0:
        tabla = False
    if s_mapa["border"] == on and len(b_recorrido) == 0:
        mapa = False
    return [tabla, mapa]


# Tabla
@callback(
    Output("tabla_prevision_actual", "children"),
    [
        Input("bot_tabla_actual", "style"),
        Input("dist_llenado", "value"),
        Input("store", "data"),
    ],
    [State("dist_llenado", "value"), State("store", "data")],
)
def desplegar_ocultar_panel_monitorizacion_contenedores(
    b_tabla, b_dist, b_data, s_dist, data
):

    if data:
        tabla_cont = html.H2(
            style=s_tabla_cont,
            children="Todos los contenedores por debajo del límite"
        )

        if b_dist:
            df = df_actual(
                read_json(data["wastecontainermonitoring_ultimo_reg"]),
                int(s_dist.split("%")[0]),
            )
            df["description"] =\
                df["description"].apply(
                    lambda x: contenedores_basura_por_dispositivo[x])

            if df.shape[0] > 0:
                global df_tabla_actual
                df_tabla_actual = df
                tabla_cont = tabla(
                    "tabla_ultimo_prevision",
                    df,
                    column_monitorizacion,
                    style_data_conditional_monitorizacion(),
                    12,
                    False,
                )

        return tabla_cont
    else:
        raise PreventUpdate


# Mapa
@callback(
    Output("mapa_prevision_actual", "children"),
    [
        Input("bot_mapa_actual", "n_clicks"),
        Input("tabla_prevision_actual", "children")
    ],
    prevent_initial_call=True,
)
def desplegar_ocultar_panel_monitorizacion_contenedores(b_mapa, b_tabla):

    mapa_cont = ""
    global df_tabla_actual
    df = df_tabla_actual.copy()

    if df.shape[0] > 0:
        mapa_cont = dcc.Graph(figure=mapa(df, "markers", 13, "description"))

    return mapa_cont


# Seleccionar boton entrada camion
@callback(
    [
        Output("bot_madrid_en", "n_clicks"),
        Output("bot_murcia_en", "n_clicks"),
        Output("bot_garcia_en", "n_clicks"),
        Output("bot_madrid_en", "style"),
        Output("bot_murcia_en", "style"),
        Output("bot_garcia_en", "style"),
    ],
    [
        Input("bot_madrid_en", "n_clicks"),
        Input("bot_murcia_en", "n_clicks"),
        Input("bot_garcia_en", "n_clicks"),
    ],
    [
        State("bot_madrid_en", "style"),
        State("bot_murcia_en", "style"),
        State("bot_garcia_en", "style"),
    ],
)
def seleccionar(
    b_madrid_en, b_murcia_en, b_garcia_en, s_madrid_en,
    s_murcia_en, s_garcia_en
):

    if b_madrid_en:
        s_madrid_en, s_murcia_en, s_garcia_en = cambiar_estado_boton(
            s_madrid_en, s_murcia_en, s_garcia_en
        )
    if b_murcia_en:
        s_murcia_en, s_madrid_en, s_garcia_en = cambiar_estado_boton(
            s_murcia_en, s_madrid_en, s_garcia_en
        )
    if b_garcia_en:
        s_garcia_en, s_madrid_en, s_murcia_en = cambiar_estado_boton(
            s_garcia_en, s_madrid_en, s_murcia_en
        )
    return [0, 0, 0, s_madrid_en, s_murcia_en, s_garcia_en]


# Seleccionar boton salida camion
@callback(
    [
        Output("bot_madrid_sa", "n_clicks"),
        Output("bot_murcia_sa", "n_clicks"),
        Output("bot_garcia_sa", "n_clicks"),
        Output("bot_madrid_sa", "style"),
        Output("bot_murcia_sa", "style"),
        Output("bot_garcia_sa", "style"),
    ],
    [
        Input("bot_madrid_sa", "n_clicks"),
        Input("bot_murcia_sa", "n_clicks"),
        Input("bot_garcia_sa", "n_clicks"),
    ],
    [
        State("bot_madrid_sa", "style"),
        State("bot_murcia_sa", "style"),
        State("bot_garcia_sa", "style"),
    ],
)
def seleccionar(
    b_madrid_sa, b_murcia_sa, b_garcia_sa, s_madrid_sa,
    s_murcia_sa, s_garcia_sa
):

    if b_madrid_sa:
        s_madrid_sa, s_murcia_sa, s_garcia_sa = cambiar_estado_boton(
            s_madrid_sa, s_murcia_sa, s_garcia_sa
        )
    if b_murcia_sa:
        s_murcia_sa, s_madrid_sa, s_garcia_sa = cambiar_estado_boton(
            s_murcia_sa, s_madrid_sa, s_garcia_sa
        )
    if b_garcia_sa:
        s_garcia_sa, s_madrid_sa, s_murcia_sa = cambiar_estado_boton(
            s_garcia_sa, s_madrid_sa, s_murcia_sa
        )

    return [0, 0, 0, s_madrid_sa, s_murcia_sa, s_garcia_sa]


# activar boton ver recorrido
@callback(
    Output("boton_ver_recorrido", "disabled"),
    [
        Input("bot_madrid_en", "n_clicks"),
        Input("bot_murcia_en", "n_clicks"),
        Input("bot_garcia_en", "n_clicks"),
        Input("bot_madrid_sa", "n_clicks"),
        Input("bot_murcia_sa", "n_clicks"),
        Input("bot_garcia_sa", "n_clicks"),
    ],
    [
        State("bot_madrid_en", "style"),
        State("bot_murcia_en", "style"),
        State("bot_garcia_en", "style"),
        State("bot_madrid_sa", "style"),
        State("bot_murcia_sa", "style"),
        State("bot_garcia_sa", "style"),
    ],
    prevent_initial_call=True,
)
def activar(b1, b2, b3, b4, b5, b6, s1, s2, s3, s4, s5, s6):
    disabled = True
    global df_tabla_actual
    df = df_tabla_actual

    if df.shape[0] > 0:
        if s1["border"] == on or s2["border"] == on or s3["border"] == on:
            if s4["border"] == on or s5["border"] == on or s6["border"] == on:
                disabled = False
    return disabled


# Mapa recorrido
@callback(
    [
        Output("mapa_recorrido_actual", "children"),
        Output("panel_mapa_recorrido_actual", "hidden"),
        Output("boton_ver_recorrido", "n_clicks"),
        Output("boton_ocultar_recorrido", "n_clicks"),
        Output("d_selector_dist", "hidden"),
    ],
    [
        Input("boton_ver_recorrido", "n_clicks"),
        Input("boton_ocultar_recorrido", "n_clicks"),
    ],
    [
        State("bot_madrid_en", "style"),
        State("bot_murcia_en", "style"),
        State("bot_garcia_en", "style"),
        State("bot_madrid_sa", "style"),
        State("bot_murcia_sa", "style"),
        State("bot_garcia_sa", "style"),
    ],
    prevent_initial_call=True,
)
def mapa_recorrido(
    bot_ver,
    bot_ocultar,
    s_madrid_en,
    s_murcia_en,
    s_garcia_en,
    s_madrid_sa,
    s_murcia_sa,
    s_garcia_sa,
):

    if bot_ver:
        global df_tabla_actual
        df_tabla = df_tabla_actual

        lat_en, long_en = entrada_salida(
            [s_madrid_en, s_murcia_en, s_garcia_en],
            ["madrid", "murcia_nor", "murcia_sur"],
            entradas_camion,
        )
        lat_sa, long_sa = entrada_salida(
            [s_madrid_sa, s_murcia_sa, s_garcia_sa],
            ["madrid", "murcia_nor", "murcia_sur"],
            salidas_camion,
        )
        df, trayectos = obtener_recorrido(
            f"{lat_en},{long_en}", f"{lat_sa},{long_sa}", df_tabla
        )

        fig = mapa(df, "markers", 18, "description")
        fig.update_traces(name="Localizaciones").data
        fig = mapa_trayectos(fig, df, trayectos)

        return [dcc.Graph(figure=fig), False, 0, 0, True]
    return ["", True, 0, 0, False]
