from datetime import datetime

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from components.funciones.funciones_aviso_de_alerta import \
    df_valores_alertas_contenedores
from components.funciones.funciones_mapas import mapa
from components.listas import contenedores_basura_por_dispositivo
from components.templates import boton, div_comun

color_boton = "#add8e6"
on = "2px solid #403DA0"
off = "1px solid #8380F7"

s_sin_datos = {
    "float": "left",
    "margin-left": "2%",
    "color": "#FE0804",
    "margin-right": "3px",
    "font-size": "1rem",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}
s_aviso = {
    "margin-left": "2%",
    "margin-top": "5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "color": "red",
}
s_titulo_alerta = {
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.5rem",
    "text-align": "center",
}
s_boton = {
    "margin-left": "2%",
    "width": "95%",
    "height": "95%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": f"0.1px solid {color_boton}",
}


# Funcion para callbacks de paginas de Alertas
def alertas(id):

    lista_dispositivos = contenedores_basura_por_dispositivo
    input_n_clicks = [
        Input(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}", "n_clicks")
        for i, x in enumerate(lista_dispositivos.keys())
    ]
    output_n_clicks = [
        Output(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}", "n_clicks")
        for i, x in enumerate(lista_dispositivos.keys())
    ]
    output_style = [
        Output(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}", "style")
        for i, x in enumerate(lista_dispositivos.keys())
    ]
    state_style = [
        State(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}", "style")
        for i, x in enumerate(lista_dispositivos.keys())
    ]

    @callback(
        Output(f"loading_{id}", "children"),
        Input("url", "pathname"),
        Input("store", "data"),
    )
    def input_triggers_nested(value, data):
        return ""

    @callback(
        [
            Output(f"alertas_{id}_tiempo", "children"),
            Output(f"alertas_{id}_distance", "children"),
            Output(f"no_alertas_{id}", "children"),
        ],
        Input(f"loading_{id}", "children"),
        State("store", "data"),
    )
    def sel_hora(click, data):

        obj_tiempo = [div_comun(s_titulo_alerta, "", "Alertas de Tiempo")]
        obj_valores = [div_comun(s_titulo_alerta, "", "Alertas de Valores")]
        obj_no_alerta = [div_comun(s_titulo_alerta, "",
                         "Dispositivos sin Alertas")]

        # contenedores basura
        if id == "contenedores_basura":
            df = read_json(data["wastecontainermonitoring_ultimo_reg"])
            df = df.sort_values(by=["description"])
            df_distance = df_valores_alertas_contenedores("distance", df)
            df_distance = df_distance.sort_values(by=["description"])
        if id == "contenedores_bateria":
            df = read_json(data["wastecontainermonitoring_ultimo_reg"])
            df = df.sort_values(by=["description"])
            df_bateria = df_valores_alertas_contenedores("bateria", df)
            df_bateria = df_bateria.sort_values(by=["description"])

        df["time_instant"] =\
            df["time_instant"].apply(
                lambda x: datetime.fromtimestamp(x / 1000))

        df = (
            df
            .assign(
                Mes=df["time_instant"].apply(lambda x: x.date().month))
            .assign(
                Dia=df["time_instant"].apply(lambda x: x.date().day))
            .assign(
                Hora=df["time_instant"].apply(lambda x: x.time()))
        )

        for i in range(len(df)):
            dispositivo = lista_dispositivos[df.iloc[i]["description"]]
            periodo = datetime.now() - df.iloc[i]["time_instant"]
            dias = periodo.days
            horas = int(periodo.total_seconds() / 3600)

            if dias > 30:
                meses = int(dias / 30)
                periodo = f" por {meses} mes"

                if meses > 1:
                    periodo = f" por {meses} meses"
            elif dias > 0:
                periodo = f" por {dias} día"

                if dias > 1:
                    periodo = f"por {dias} días"
            elif dias == 0 and horas > 6:
                horas = int(periodo.total_seconds() / 3600)
                periodo = f"por {horas} horas"
            else:
                if id == "contenedores_basura":
                    if (
                        df_distance.iloc[i]["distace_para_llenado"]
                        <= df_distance.iloc[i]["Min"]
                    ):
                        aviso = div_comun(
                            s_aviso,
                            "",
                            f"""El contenedor está a {
                                df_distance.iloc[i]["distace_para_llenado"]
                                } cm. de llenarse""",
                        )
                        obj_valores.append(
                            div_comun(
                                {"margin-top": "10px"},
                                "",
                                [
                                    boton(
                                        f"""alerta_dispositivo_{
                                            dispositivo}_{id}""",
                                        dispositivo,
                                        s_boton,
                                    ),
                                    aviso,
                                ],
                            )
                        )
                    else:
                        obj_no_alerta.append(
                            div_comun(
                                {"margin-top": "10px"},
                                "",
                                boton(
                                    f"alerta_dispositivo_{dispositivo}_{id}",
                                    dispositivo,
                                    s_boton,
                                ),
                            )
                        )
                    continue
                if id == "contenedores_bateria":
                    if df_bateria.iloc[i]["battery_level"] <=\
                            df_bateria.iloc[i]["Min"]:
                        aviso = div_comun(
                            {"margin-left": "6px", "margin-top": "5px",
                             "color": "red"},
                            "",
                            f"""Nivel de bateria a {
                                df_bateria.iloc[i]["battery_level"]}""",
                        )
                        obj_valores.append(
                            div_comun(
                                {"margin-top": "10px"},
                                "",
                                [
                                    boton(
                                        f"""alerta_dispositivo_{
                                            dispositivo}_{id}""",
                                        dispositivo,
                                        s_boton,
                                    ),
                                    aviso,
                                ],
                            )
                        )
                    else:
                        obj_no_alerta.append(
                            div_comun(
                                {"margin-top": "10px"},
                                "",
                                boton(
                                    f"alerta_dispositivo_{dispositivo}_{id}",
                                    dispositivo,
                                    s_boton,
                                ),
                            )
                        )
                    continue

            if periodo != "":
                hidden_bot = False
                if id == "depuradora_alertas":
                    hidden_bot = True

                obj_tiempo.append(
                    div_comun(
                        {"margin-top": "10px"},
                        "",
                        [
                            boton(
                                f"alerta_dispositivo_{dispositivo}_{id}",
                                dispositivo,
                                s_boton,
                                hidden=hidden_bot,
                            ),
                            div_comun(
                                {"margin-left": "6px", "margin-top": "0px"},
                                "",
                                [
                                    div_comun(s_sin_datos, "", "Sin Datos "),
                                    div_comun(s_aviso, "", periodo),
                                ],
                            ),
                        ],
                    )
                )

        return [obj_tiempo, obj_valores, obj_no_alerta]

    # Mapa
    @callback(
        [
            Output(f"mapa_alertas_{id}", "children"),
            output_n_clicks,
            output_style
        ],
        [
            Input(f"no_alertas_{id}", "children"),
            input_n_clicks
        ],
        [
            state_style,
            State("store", "data")
        ],
        prevent_initial_call=True,
    )
    def sel_ho(load, var_n_clicks, var_state_style, data):

        dispositivo = ""
        lista = list(lista_dispositivos.keys())
        for i, x in enumerate(var_n_clicks):
            if x:
                dispositivo = lista[i]
                var_state_style[i]["border"] = on
                continue
            var_state_style[i]["border"] = off

        df = read_json(data["wastecontainermonitoring_ultimo_reg"])[
            ["time_instant", "description", "lat", "long"]
        ]

        df["time_instant"] =\
            df["time_instant"].apply(
                lambda x: datetime.fromtimestamp(x / 1000))

        df = (
            df
            .assign(
                Hora=df["time_instant"].apply(lambda x: x.time()))
        )
        if dispositivo:
            df = df[df["description"] == dispositivo]

        return [
            dcc.Graph(figure=mapa(df, "markers", 13, "Hora")),
            [0 for x in range(len(var_n_clicks))],
            var_state_style,
        ]
