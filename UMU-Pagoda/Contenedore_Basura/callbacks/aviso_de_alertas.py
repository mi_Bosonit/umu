from dash import callback
from dash.dependencies import Input, Output
from pandas import DataFrame, read_sql

from components.funciones.funciones_aviso_de_alerta import (
    alertas_por_tiempo, alertas_por_valores_contenedores)
from components.queries import *
from components.utils import conection, conection_2


@callback(
    [Output("interval", "n_intervals")],
    [Input("interval", "n_intervals")],
    prevent_initial_call=True,
)
def display_page(interval):
    engine = conection()
    df_alertas = DataFrame(
        columns=["Tipo Alerta", "Localizacion", "Dispositivo", "Alerta"]
    )
    df = read_sql(wastecontainermonitoring_ultimo_reg, engine)    
    df = alertas_por_tiempo(df)
    df_alertas = alertas_por_valores_contenedores(df, df_alertas, "distance")
    df_alertas = alertas_por_valores_contenedores(df, df_alertas, "bateria")    
    engine.dispose()

    # Guardar Alertas en BBDD de Iot
    engine = conection_2()
    engine.execute(
        """ DELETE
                       FROM openiotv2.wastecontainermonitoring_warnings """
    )
    
    if df_alertas.shape[0] > 0:
        
        for i in range(len(df_alertas)):            
            engine.execute(
                f"""INSERT INTO
                               openiotv2.wastecontainermonitoring_warnigs
                               (tipo_Alerta, localizacion, dispositivo, alerta)
                               VALUES ('{df_alertas.iloc[i]['Tipo Alerta']}',
                                       '{df_alertas.iloc[i]['Localizacion']}',
                                       '{df_alertas.iloc[i]['Dispositivo']}',
                                       '{df_alertas.iloc[i]['Alerta']}') """
            )
    engine.dispose()
    return [0]
