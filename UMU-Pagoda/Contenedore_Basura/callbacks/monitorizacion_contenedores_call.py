from contextlib import suppress
from datetime import datetime, timedelta

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from pandas import read_json

from assets.estilos.tabla_monitorizacion import (
    column_monitorizacion, style_data_conditional_monitorizacion)
from components.funciones.funciones_mapas import mapa
from components.funciones.funciones_monitorizacion import (
    boton_on_off, df_seleccionar_registro, tratar_df)
from components.listas import horas
from components.templates import div_comun, tabla

global df_ult_reg
global df_sel_reg


# seleccionar boton ultimo registro/seleccionar registro
@callback(
    [
        Output("boton_ult_reg", "style"),
        Output("boton_sel_reg", "style"),
        Output("boton_ult_reg", "n_clicks"),
    ],
    [
        Input("boton_ult_reg", "n_clicks"),
        Input("boton_sel_reg", "n_clicks")
    ],
    [
        State("boton_ult_reg", "style"),
        State("boton_sel_reg", "style")
    ],
)
def seleccionar(b_ult_reg, b_sel_reg, s_ult_reg, s_sel_reg):
    boton_on_off(s_sel_reg, [s_ult_reg])
    if b_ult_reg:
        boton_on_off(s_ult_reg, [s_sel_reg])
    return [s_ult_reg, s_sel_reg, 0]


# ocultar paneles
@callback(
    [
        Output("panel_ultimo_registro", "hidden"),
        Output("panel_seleccionar_registro", "hidden"),
        Output("mapa_ultimo_registro", "hidden"),
        Output("mapa_seleccionar_registro", "hidden"),
        Output("panel_selectores", "hidden"),
    ],
    [Input("boton_ult_reg", "style"), Input("boton_sel_reg", "style")],
)
def seleccionar(s_ult_reg, s_sel_reg):
    p_ult_reg = True
    p_sel_reg = False
    m_ult_reg = True
    m_sel_reg = False
    p_selectores = False

    if s_ult_reg["border"] == "3px solid #5BB6CE":
        p_ult_reg = False
        p_sel_reg = True
        m_ult_reg = False
        m_sel_reg = True
        p_selectores = True
    return [p_ult_reg, p_sel_reg, m_ult_reg, m_sel_reg, p_selectores]


# opciones selectores
@callback(
    [Output("sel_dia", "options"), Output("sel_hora", "options")],
    [Input("boton_sel_reg", "style")],
)
def seleccionar(s_sel_reg):
    lista_de_fechas, lista_de_horas = [], []
    if s_sel_reg["border"] == "3px solid #5BB6CE":
        rango = range(1, (datetime.now() + timedelta(days=1)).date().day)
        if (datetime.now() + timedelta(days=1)).date().day == 1:
            rango = range(1, (datetime.now().date().day + 1))
        lista_de_fechas = [
            f"{datetime.now().date().year}-{datetime.now().date().month}-{i}"
            for i in rango
        ]
        lista_de_horas = horas
    return [lista_de_fechas, lista_de_horas]


# Tabla ultimo registro
@callback(
    Output("tabla_ultimo_registro", "children"),
    [
        Input("boton_ult_reg", "n_clicks"),
        Input("store", "data")
    ],
    State("store", "data"),
)
def tabla_ult_registro(b_ult_reg, store, data):
    if data:
        global df_ult_reg
        df_ult_reg = read_json(data["wastecontainermonitoring_ultimo_reg"])[
            ["description", "distance", "battery_level",
             "lat", "long", "time_instant"]].sort_values("distance",
                                                         ascending=False)
        df_ult_reg = tratar_df(df_ult_reg)
        return tabla(
            "tabla_ultimo",
            df_ult_reg,
            column_monitorizacion,
            style_data_conditional_monitorizacion(),
            13,
        )
    else:
        raise PreventUpdate


# Mapa Último Registro
@callback(
    [
        Output("mapa_ultimo_registro", "children"),
        Output("panel_boton_todos", "hidden"),
        Output("boton_ver_todos", "n_clicks"),
        Output("tabla_ultimo", "selected_rows"),
    ],
    [
        Input("boton_ver_todos", "n_clicks"),
        Input("tabla_ultimo", "selected_rows"),
        Input("tabla_ultimo_registro", "children"),
    ],
    State("tabla_ultimo_registro", "children"),
)
def mapa_ultimo_registro(b_todos, selected_rows, b_children, s_children):

    if len(s_children) > 0:
        global df_ult_reg
        while True:
            with suppress(Exception):
                df = df_ult_reg.copy()
                break

        panel_boton_todos = True

        if not b_todos and len(selected_rows) > 0:
            df = df.iloc[selected_rows]
            panel_boton_todos = False
        if b_todos:
            selected_rows = []
        return [
            dcc.Graph(figure=mapa(df, "markers", 13, "Localizacion")),
            panel_boton_todos,
            0,
            selected_rows,
        ]
    else:
        raise PreventUpdate


# Tabla seleccionar registro
@callback(
    Output("tabla_seleccionar_registro", "children"),
    [
        Input("sel_dia", "value"),
        Input("sel_hora", "value")
    ],
    [
        State("sel_dia", "value"),
        State("sel_hora", "value"),
        State("store", "data")
    ],
)
def tabla_seleccionar_registro(
    b_sel_dia, b_sel_hora, s_sel_dia, s_sel_hora, data
):

    obj = div_comun({}, "tabla_seleccionar")

    if s_sel_dia and s_sel_hora:
        global df_sel_reg
        df_sel_reg = df_seleccionar_registro(s_sel_hora, s_sel_dia, data)

        if df_sel_reg.shape[0] > 0:
            obj = tabla(
                "tabla_seleccionar",
                df_sel_reg,
                column_monitorizacion,
                style_data_conditional_monitorizacion(),
            )

    return obj


# Mapa seleccionar registro
@callback(
    Output("mapa_seleccionar_registro", "children"),
    [
        Input("tabla_seleccionar_registro", "children"),
        Input("tabla_seleccionar", "derived_virtual_selected_rows"),
    ],
    [State("sel_dia", "value"), State("sel_hora", "value")],
)
def mapa_seleccionar_registro(b_sel_dia, selected_rows, s_sel_dia, s_sel_hora):
    obj = ""

    if s_sel_dia and s_sel_hora:
        global df_sel_reg
        while True:
            with suppress(Exception):
                df = df_sel_reg
                break

        obj = div_comun(
            {
                "margin-left": "-60%",
                "margin-top": "-5%",
                "font-size": "1.5vmax",
                "font-family": "Roboto, Helvetica, Arial, sans-serif",
            },
            "",
            "No hay registros con el día y la hora seleccionados",
        )

        if df.shape[0] > 0:
            if selected_rows is not None and len(selected_rows) > 0:
                df = df.iloc[selected_rows]
            obj = dcc.Graph(figure=mapa(df, "markers", 13, "Localizacion"))

    return obj
