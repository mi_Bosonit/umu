from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

import pages
from components.queries import *
from components.utils import conection
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/contenedores_basura_monitorizacion":
    pages.monitorizacion.layout,
    f"/{raiz}/contenedores_basura_monitorizacion_por_localizacion":
    pages.monitorizacion_por_localizacion.layout,
    f"/{raiz}/contenedores_basura_alertas_llenado":
    pages.alertas_llenado.layout,
    f"/{raiz}/contenedores_basura_alertas_bateria":
    pages.alertas_bateria.layout,
    f"/{raiz}/contenedores_basura_recorrido_camion":
    pages.recorrido_camion.layout,
    f"/{raiz}/contenedores_basura_actualizacion_alertas_llenado":
    pages.actualizacion_alertas_llenado.layout,
    f"/{raiz}/contenedores_basura_actualizacion_alertas_bateria":
    pages.actualizacion_alertas_bateria.layout,
    f"/{raiz}/prevision_admin":
    pages.prevision.prevision_admin.layout,
    f"/{raiz}/prevision_user":
    pages.prevision.prevision_user.layout,
}


# Update page content
@callback(Output("page_content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname in lista_de_paginas:
        return lista_de_paginas[pathname]


# Guardar query en Store
@callback(
    [Output("store", "data"),
     Output(f"loading_contenedores", "children")],
    Input("url", "pathname"),
)
def guardar_datos(pathname):
    data = {}
    engine = conection()
    data["wastecontainermonitoring_ultimo_reg"] = read_sql(
        wastecontainermonitoring_ultimo_reg, engine).to_json(orient="columns")
    data["wastecontainermonitoring"] = read_sql(
        wastecontainermonitoring, engine).to_json(orient="columns")
    engine.dispose()
    return [data, ""]
