from dash import callback
from dash.dependencies import Input, Output

from scripts.bbdd.actualizar_bbdd import (actualizar_energia_acumulado,
                                          actualizar_energia_creada)


@callback(
    Output("interval_bbdd", "n_intervals"),
    Input("interval_bbdd", "n_intervals"),
    prevent_initial_call=True,
)
def display_page(interval):
    actualizar_energia_creada()
    actualizar_energia_acumulado()
    return 0
