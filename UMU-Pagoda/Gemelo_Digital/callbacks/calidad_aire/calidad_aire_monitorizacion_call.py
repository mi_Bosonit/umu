from contextlib import suppress

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from components.funciones_mapa import mapa
from components.listas import (columnas_moni_aire,
                               style_data_conditional_monitorizacion_aire)
from components.templates import tabla
from components.utils import mes_dia_hora, time_instant_to_date_sin_utc

global df_ultimo_reg


# Tabla
@callback(
    Output("tabla_aire", "children"),
    Input("store", "data"),
    State("store", "data")
)
def sel_hora(click, data):
    global df_ultimo_reg
    df = read_json(data["ambientmonitoring_cometsensor_ultimo_reg"])
    df_latitudes = read_json(data["latitudes"])

    df_ultimo_reg = df.merge(
        df_latitudes, left_on="controlled_asset", right_on="controlled_asset"
    ).sort_values("time_instant", ascending=False)

    df = df_ultimo_reg.copy()
    df = time_instant_to_date_sin_utc(df, "time_instant", "time_instant")
    df = mes_dia_hora(df, "time_instant")
    df["Fecha"] = list(map(lambda x: x.date(), df["time_instant"]))

    df["Humedad"] = df["Humedad Relativa"]
    df["Humedad Relativa"] = df["Humedad Relativa"] / 100
    df["Presión Atmosférica"] = round(df["Presión Atmosférica"] / 10, 2)

    return tabla(
        "tabla_aire",
        df,
        columnas_moni_aire,
        style_data_conditional_monitorizacion_aire,
        12,
    )


# Mapa
@callback(
    Output("mapa_aire", "children"),
    [Input("tabla_aire", "children"), Input("tabla_aire", "selected_rows")],
    prevent_initial_call=True,
)
def mapa_ultimo_registro(b_tabla, selected_rows):

    global df_ultimo_reg

    while True:
        with suppress(Exception):
            df = df_ultimo_reg.copy()
            break

    size_mapa = 8

    if len(selected_rows) > 0:
        if len(selected_rows) == 1:
            size_mapa = 14
        df = df.iloc[selected_rows]

    return [dcc.Graph(figure=mapa(df, "markers", 14,
                                  "description", size_mapa))]
