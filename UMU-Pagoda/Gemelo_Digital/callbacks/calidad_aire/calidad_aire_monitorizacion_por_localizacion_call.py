from contextlib import suppress
from datetime import datetime

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from assets.css_calidad_aire_monitorizacion_por_loc import *
from components.listas import unidades_calidad_aire
from components.templates import div_comun, line
from components.utils import time_instant_to_date_sin_utc

global df_latitudes
global df_ultimo_reg
global df_24_horas


# Selector Aula
@callback(
    [Output("sel_aula_aire", "options"), Output("sel_aula_aire", "value")],
    [Input("sel_loc_aire", "value"), Input("store", "data")],
    [State("sel_loc_aire", "value"), State("store", "data")],
    prevent_initial_call=True,
)
def sel_aula(b_aire, b_data, s_loc_aire, data):

    global df_ultimo_reg
    global df_latitudes
    global df_24_horas
    lista = [""]

    df = read_json(data["ambientmonitoring_cometsensor_ultimo_reg"])
    df_latitudes =\
        read_json(data["latitudes"])[["controlled_asset", "description"]]
    df_ultimo_reg = df.merge(
        df_latitudes, left_on="controlled_asset", right_on="controlled_asset"
    )

    df_24_horas = read_json(data["ambientmonitoring_cometsensor"])
    if s_loc_aire:
        mask = df_ultimo_reg["description"] == s_loc_aire
        lista = sorted(
            list(df_ultimo_reg[mask]["Aula"])
        )

    return [lista, lista[0]]


# Ultimo registro
@callback(
    [
        Output("ultimo_reg", "children"),
        Output("loading_calidad_aire_loc", "children"),
        Output("ultimo_reg", "style"),
    ],
    [Input("sel_valor_aire", "value"), Input("sel_aula_aire", "value")],
    [
        State("sel_loc_aire", "value"),
        State("sel_aula_aire", "value"),
        State("ultimo_reg", "style"),
    ],
    prevent_initial_call=True,
)
def ultimo_registro(valor, click, localizacion, aula, s_ultimo):

    obj = ""
    s_ultimo["background"] = "white"

    if valor and aula:
        s_ultimo["background"] = "#add8e6"
        global df_ultimo_reg

        while True:
            with suppress(Exception):
                df = df_ultimo_reg.copy()
                break

        df = df[(df["description"] == localizacion) & (df["Aula"] == aula)]
        df = time_instant_to_date_sin_utc(df, "time_instant", "time_instant")

        if (datetime.now() - df["time_instant"]).iloc[0].days <= 0:

            df["Presión Atmosférica"] =\
                round(df["Presión Atmosférica"] / 10, 2)
            valor_actual = df[valor].iloc[0]
            unidad = unidades_calidad_aire[valor][1]

            obj = [
                div_comun(s_titulo, "", "Último Registro"),
                div_comun(s_hora, "", df["time_instant"].iloc[0].time()),
                div_comun(s_valor, "", valor_actual),
                div_comun(s_unidad, "", unidad),
            ]

    return [obj, "", s_ultimo]


# ultimas 24 horas
@callback(
    Output("graficos_aire_loc", "children"),
    [Input("ultimo_reg", "children")],
    [
        State("sel_loc_aire", "value"),
        State("sel_aula_aire", "value"),
        State("sel_valor_aire", "value"),
        State("ultimo_reg", "style"),
    ],
    prevent_initial_call=True,
)
def ultimo_registro(b_ultimo_reg, localizacion, aula, valor, s_ultimo):

    obj = ""
    global df_latitudes
    global df_24_horas

    if s_ultimo["background"] == "white":
        obj = ""
    elif b_ultimo_reg == "":
        obj = div_comun(
            s_no_hay_registro, "", "No hay registro en las últimas 24 horas"
        )
    else:
        while True:
            with suppress(Exception):
                df_24_horas = df_24_horas.copy()
                df_latitudes = df_latitudes.copy()
                break

        df = df_24_horas.merge(
            df_latitudes, left_on="controlled_asset",
            right_on="controlled_asset"
        ).sort_values("time_instant", ascending=False)

        df = df[(df["description"] == localizacion) & (df["Aula"] == aula)]
        df = time_instant_to_date_sin_utc(df, "Fecha", "time_instant")
        df["Presión Atmosférica"] = round(df["Presión Atmosférica"] / 10, 2)

        obj = dcc.Graph(figure=line(df, x="Fecha", y=valor,
                        title="Últimas 24 horas"))

    return obj
