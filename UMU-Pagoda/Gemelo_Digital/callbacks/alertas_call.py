from contextlib import suppress

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from components.funciones_alertas_call import alertas_tiempo, alertas_valores
from components.funciones_mapa import mapa
from components.listas import (calidad_aire_por_dispositivo,
                               edificios_electrica, edificios_electrica_mapa)
from components.templates import div_comun
from components.utils import mes_dia_hora, time_instant_to_date

s_titulo_alerta = {
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.5rem",
    "text-align": "center",
}

global df_comun


# Funcion para callbacks de paginas de Alertas
def alertas(id):

    if id == "calidad_aire":
        lista_dispositivos = calidad_aire_por_dispositivo
        input_n_clicks = [
            Input(f"alerta_dispositivo_{lista_dispositivos[x][1]}_{id}",
                  "n_clicks")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        output_n_clicks = [
            Output(f"alerta_dispositivo_{lista_dispositivos[x][1]}_{id}",
                   "n_clicks")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        output_style = [
            Output(f"alerta_dispositivo_{lista_dispositivos[x][1]}_{id}",
                   "style")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        state_style = [
            State(f"alerta_dispositivo_{lista_dispositivos[x][1]}_{id}",
                  "style")
            for i, x in enumerate(lista_dispositivos.keys())
        ]

    if id == "electrica":
        lista_dispositivos = edificios_electrica
        input_n_clicks = [
            Input(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}",
                  "n_clicks")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        output_n_clicks = [
            Output(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}",
                   "n_clicks")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        output_style = [
            Output(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}",
                   "style")
            for i, x in enumerate(lista_dispositivos.keys())
        ]
        state_style = [
            State(f"alerta_dispositivo_{lista_dispositivos[x]}_{id}",
                  "style")
            for i, x in enumerate(lista_dispositivos.keys())
        ]

    @callback(
        [
            Output(f"alertas_{id}_tiempo", "children"),
            Output(f"alertas_{id}_distance", "children"),
            Output(f"no_alertas_{id}", "children"),
        ],
        Input("store", "data"),
        State("store", "data"),
    )
    def sel_hora(click, data):

        global df_comun
        obj_tiempo = [div_comun(s_titulo_alerta, "", "Alertas de Tiempo")]
        obj_valores = [div_comun(s_titulo_alerta, "", "Alertas de Valores")]
        obj_no_alerta = [div_comun(s_titulo_alerta, "",
                                   "Dispositivos sin Alertas")]

        if id == "calidad_aire":
            df = read_json(data["ambientmonitoring_cometsensor_ultimo_reg"])
            df["description"] = df["ocb_id"]

        if id == "electrica":
            df = read_json(
                     data["j_energia_consumida_2022_ultimo_reg"]
                 ).reset_index(drop=True)

        df = time_instant_to_date(df, "time_instant", "time_instant")
        df = mes_dia_hora(df, "time_instant")
        df_comun = df.copy()

        df, obj_tiempo = alertas_tiempo(id, df, lista_dispositivos, obj_tiempo)
        obj_valores, obj_no_alerta, _ = alertas_valores(
            id, df, lista_dispositivos, obj_valores, obj_no_alerta
        )

        return [obj_tiempo, obj_valores, obj_no_alerta]

    # Mapa
    @callback(
        [
            Output(f"mapa_alertas_{id}", "children"),
            output_n_clicks, output_style
        ],
        [
            Input(f"no_alertas_{id}", "children"),
            input_n_clicks
        ],
        [
            state_style,
            State("store", "data")
        ],
        prevent_initial_call=True,
    )
    def sel_ho(load, var_n_clicks, var_state_style, data):

        global df_comun
        obj = ""
        dispositivo = ""
        lista = list(lista_dispositivos.keys())
        zoom = 10
        for i, x in enumerate(var_n_clicks):
            if x:
                dispositivo = lista[i]
                var_state_style[i]["border"] = "2px solid #403DA0"
                continue
            var_state_style[i]["border"] = "1px solid #8380F7"

        while True:
            with suppress(Exception):
                df = df_comun.copy()
                break

        if id == "calidad_aire":

            df["description"] = df["ocb_id"]
            df_latitudes = read_json(data["latitudes"])[
                ["controlled_asset", "lat", "long"]
            ]
            df = df.merge(df_latitudes, on="controlled_asset")

        if id == "electrica":

            df["des"] = df["description"]
            df["description"] = list(
                map(lambda x: edificios_electrica_mapa[x], df["description"])
            )

            df_loc = read_json(data["building"])
            df = df.merge(df_loc, on="description")

            df["description"] = df["des"]

        if dispositivo:
            df = df[df["description"] == dispositivo]
            zoom = 14

        obj = dcc.Graph(figure=mapa(df, "markers", 13, "Hora", zoom))

        return [obj, [0 for x in range(len(var_n_clicks))], var_state_style]
