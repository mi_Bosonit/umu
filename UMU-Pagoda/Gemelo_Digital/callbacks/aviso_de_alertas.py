from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

from components.funciones_alertas_call import alertas_valores
from components.listas import calidad_aire_por_dispositivo, edificios_electrica
from components.queries import df_alertas_electrica
from components.utils import conection, conection_2
from configuracion import tabla_bbdd


@callback(
    Output("interval", "n_intervals"),
    Input("interval", "n_intervals"),
    prevent_initial_call=True,
)
def display_page(interval):

    engine = conection()
    # Calidad aire
    df = read_sql(
        f"""SELECT DISTINCT ON (ocb_id) ocb_id as description,
                             time_instant, controlled_asset,
                             area_served AS "Aula",
                             temperature AS "Temperatura",
                             relative_humidity AS "Humedad Relativa",
                             barometric_pressure AS "Presión Atmosférica",
                             co2 AS "Co2"
                      FROM {tabla_bbdd}.ambientmonitoring_cometsensor
                      WHERE time_instant > Now() - '7 hours'::interval
                      ORDER BY ocb_id, time_instant DESC """,
        engine,
    )

    _, _, lista_alertas_aire = alertas_valores(
        "calidad_aire", df, calidad_aire_por_dispositivo, [], []
    )

    # consumo electrico
    df = df_alertas_electrica()

    _, _, lista_alertas_electrica = alertas_valores(
        "electrica", df, edificios_electrica, [], []
    )
    lista_alertas = lista_alertas_aire + lista_alertas_electrica
    engine.dispose()
    
    # Guardar Alertas en BBDD Iot    
    engine = conection_2()
    engine.execute(
        f"""DELETE
                       FROM
                       {tabla_bbdd}.ambientmonitoring_cometsensor_warnigs"""
    )
    
    if len(lista_alertas) > 0:
        
        for alerta in lista_alertas:
            alerta[2] = alerta[2].replace("%", "")
            engine.execute(
                f"""INSERT INTO
                               {tabla_bbdd}.ambientmonitoring_cometsensor_warnings
                               (tipo_Alerta, localizacion, alerta)
                               VALUES
                               ('{alerta[0]}',
                                '{alerta[1]}',
                                '{alerta[2]}')"""
            )

    engine.dispose()
    return 0
