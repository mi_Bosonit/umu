from contextlib import suppress

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json

from assets.css_clima_monitorizacion import *
from components.funciones_mapa import mapa
from components.listas import edificios_clima, unidades_clima
from components.templates import div_comun, line, selector
from components.utils import time_instant_to_date_sin_utc

global df_latitudes


# Selector de localizaciones
@callback(
    [
        Output("div_selector_monitorizacion_clima", "children"),
        Output("div_selector_valor_clima", "children"),
    ],
    Input("url", "pathname"),
)
def tabla_sel_registro(click):

    return [
        selector(
            "sel_localizacion_clima",
            {},
            edificios_clima,
            value="Edificio LAIB/Departamental",
            placeholder="Seleccione Localización",
        ),
        selector(
            "selector_valores_atmosfericos",
            {},
            [key for key in unidades_clima.keys()],
            placeholder="Seleccione Valor",
        ),
    ]


# Mapa
@callback(
    Output("div_mapa", "children"),
    [Input("sel_localizacion_clima", "value"), Input("store", "data")],
    [State("sel_localizacion_clima", "value"), State("store", "data")],
)
def tabla_sel_registro(b_sel, b_data, s_sel, data):

    obj = ""
    global df_latitudes

    if data:
        df = read_json(data["latitudes"])
        df = df[df["description"] == s_sel]
        df_latitudes = df
        obj = [dcc.Graph(figure=mapa(df, "markers", 13,
                                     "description", zoom=15))]
    return obj


# Gráfico
@callback(
    [
        Output("div_valores", "children"),
        Output("div_mapa", "hidden"),
        Output("selector_valores_atmosfericos", "value"),
    ],
    [
        Input("selector_valores_atmosfericos", "value"),
        Input("sel_localizacion_clima", "value"),
    ],
    [
        State("sel_localizacion_clima", "value"),
        State("selector_valores_atmosfericos", "value"),
        State("store", "data"),
    ],
    prevent_initial_call=True,
)
def tabla_sel_registro(valor, click, localizacion, state_valor, data):
    obj = []
    hidden = False
    valor_selector = ""
    global df_latitudes

    if state_valor and localizacion:

        columna, unidad = unidades_clima[valor]
        hidden = True
        valor_selector = state_valor
        obj = 'No hay registros en las últimas 12 horas'

        df = read_json(data["weathermonitoring"])[
            ["time_instant", columna, "controlled_asset"]
        ]

        if df.shape[0] > 0:

            while True:
                with suppress(Exception):
                    df_latitudes = df_latitudes.copy()
                    break

            df = df.merge(
                df_latitudes, left_on="controlled_asset",
                right_on="controlled_asset"
            )[["time_instant", columna]]

            df = time_instant_to_date_sin_utc(df, "time_instant",
                                              "time_instant")
            df.columns = ["Fecha", state_valor]
            df = df.sort_values(by=["Fecha"])

            obj = [
                div_comun(
                    s_div_actual,
                    "",
                    [
                        div_comun(s_titulo, "", "Valor actual"),
                        div_comun(s_valor, "",
                                  df[state_valor].iloc[df.shape[0] - 1]),
                        div_comun(s_unidad, "", unidad),
                    ],
                ),
                div_comun(
                    s_div_grafico,
                    "",
                    dcc.Graph(
                        figure=line(df, x="Fecha", y=state_valor,
                                    title=f"Últimas 12 horas")
                    ),
                ),
            ]

    return [obj, hidden, valor_selector]
