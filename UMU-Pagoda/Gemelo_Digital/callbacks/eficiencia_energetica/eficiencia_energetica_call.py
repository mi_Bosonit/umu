from contextlib import suppress

from dash import callback
from dash.dependencies import Input, Output, State
from pandas import read_json

from components.funciones_energia_callbacks import (cuerpo_periodo,
                                                    cuerpo_semana)

global df_solar


def callbacks_eficiencia(id, path, titulo, lista_edificios, loc=False):

    if loc:

        @callback(
            [
                Output(f"panel_esta_semana_{id}", "children"),
                Output(f"boton_esta_semana_{id}", "n_clicks"),
                Output(f"sel_mes_{id}", "value"),
                Output(f"div_sel_dia_tramo_{id}", "hidden"),
                Output(f"boton_esta_semana_{id}", "style"),
                Output(f"user_loading_{id}", "children"),
            ],
            [
                Input(f"boton_esta_semana_{id}", "n_clicks"),
                Input(f"sel_mes_{id}", "value"),
                Input(f"sel_localizacion_{id}", "value"),
                Input("store", "data"),
            ],
            [
                State(f"boton_esta_semana_{id}", "style"),
                State(f"sel_localizacion_{id}", "value"),
                State("store", "data"),
            ],
            prevent_initial_call=True,
        )
        def semana(semana, mes, loc, bot_data, style_boton,
                   localizacion, data):
            if localizacion or semana:
                global df_solar
                df_solar = read_json(data[path]).drop_duplicates()
                return cuerpo_semana(
                    semana,
                    mes,
                    style_boton,
                    df_solar.copy(),
                    lista_edificios,
                    titulo,
                    localizacion,
                ) + [""]
            return ["", 0, "", True, style_boton, ""]

        @callback(
            [
                Output(f"mes_por_dias_{id}", "children"),
                Output(f"mes_por_tramo_{id}", "children"),
            ],
            [
                Input(f"sel_dia_tramo_{id}", "value"),
                Input(f"sel_mes_{id}", "value")
            ],
            [
                State(f"sel_mes_{id}", "value"),
                State(f"sel_localizacion_{id}", "value")
            ],
            prevent_initial_call=True,
        )
        def periodo(sel_dia_tramo, click, mes, localizacion):
            global df_solar
            while True:
                with suppress(Exception):
                    df = df_solar.copy()
                    break

            return cuerpo_periodo(
                sel_dia_tramo, click, df, lista_edificios, titulo, localizacion
            )

    else:

        @callback(
            [
                Output(f"panel_esta_semana_{id}", "children"),
                Output(f"boton_esta_semana_{id}", "n_clicks"),
                Output(f"sel_mes_{id}", "value"),
                Output(f"div_sel_dia_tramo_{id}", "hidden"),
                Output(f"boton_esta_semana_{id}", "style"),
                Output(f"user_loading_{id}", "children"),
            ],
            [
                Input(f"boton_esta_semana_{id}", "n_clicks"),
                Input(f"sel_mes_{id}", "value"),
                Input("store", "data"),
            ],
            [
                State(f"boton_esta_semana_{id}", "style"),
                State("store", "data")
            ],
            prevent_initial_call=True,
        )
        def semana(semana, mes, bot_data, style_boton, data):

            if semana or data:
                global df_solar
                df_solar = read_json(data[path]).drop_duplicates()
                return cuerpo_semana(
                    semana, mes, style_boton, df_solar.copy(),
                    lista_edificios, titulo
                ) + [""]
            return ["", 0, "", True, style_boton, ""]

        @callback(
            [
                Output(f"mes_por_dias_{id}", "children"),
                Output(f"mes_por_tramo_{id}", "children"),
            ],
            [
                Input(f"sel_dia_tramo_{id}", "value"),
                Input(f"sel_mes_{id}", "value")
            ],
            State(f"sel_mes_{id}", "value"),
            prevent_initial_call=True,
        )
        def periodo(sel_dia_tramo, click, mes):
            global df_solar
            while True:
                with suppress(Exception):
                    df = df_solar.copy()
                    break

            return cuerpo_periodo(sel_dia_tramo, mes, df,
                                  lista_edificios, titulo)
