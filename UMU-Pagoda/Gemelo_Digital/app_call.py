from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

import pages
from components.queries import *
from components.utils import conection
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/eficiencia_solar_monitorizacion":
    pages.eficiencia_energetica.eficiencia_solar_monitorizacion.layout,
    f"/{raiz}/eficiencia_solar_monitorizacion_por_localizacion":
    pages.eficiencia_energetica.
    eficiencia_solar_monitorizacion_por_localizacion.layout,
    f"/{raiz}/eficiencia_electrica_monitorizacion":
    pages.eficiencia_energetica.eficiencia_electrica_monitorizacion.layout,
    f"/{raiz}/eficiencia_electrica_monitorizacion_por_localizacion":
    pages.eficiencia_energetica.
    eficiencia_electrica_monitorizacion_por_localizacion.layout,
    f"/{raiz}/eficiencia_electrica_alertas":
    pages.eficiencia_energetica.eficiencia_electrica_alertas.layout,
    f"/{raiz}/eficiencia_electrica_actualizar_alertas":
    pages.eficiencia_energetica.eficiencia_electrica_actualizar_alertas.layout,
    f"/{raiz}/calidad_aire_monitorizacion":
    pages.calidad_aire.monitorizacion.layout,
    f"/{raiz}/calidad_aire_monitorizacion_por_localizacion":
    pages.calidad_aire.monitorizacion_por_localizacion.layout,
    f"/{raiz}/calidad_aire_alertas":
    pages.calidad_aire.alertas.layout,
    f"/{raiz}/calidad_aire_actualizar_alertas":
    pages.calidad_aire.actualizar_alertas.layout,
    f"/{raiz}/clima_monitorizacion":
    pages.clima.monitorizacion.layout,
    f"/{raiz}/prevision_admin":
    pages.prevision.prevision_admin.layout,
    f"/{raiz}/prevision_user":
    pages.prevision.prevision_user.layout,
}


# Update page content
@callback(Output("page_content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname in lista_de_paginas:
        return lista_de_paginas[pathname]


# Guardar query en Store
@callback(
    [Output("store", "data"),
     Output(f"loading_gemelo", "children")],
    Input("url", "pathname"),
)
def guardar_datos(pathname):
    data = {}
    engine = conection()
    # Eficiencia Energetica
    # Solar

    if pathname in (
            f"/{raiz}/eficiencia_solar_monitorizacion",
            f"/{raiz}/eficiencia_solar_monitorizacion_por_localizacion"):
        df = df_solar()
        data["j_energia_solar_creada_2022"] = df.to_json(orient="columns")
    # Electrica
    if pathname in (
            f"/{raiz}/eficiencia_electrica_monitorizacion",
            f"/{raiz}/eficiencia_electrica_monitorizacion_por_localizacion"):
        data["j_energia_consumida_2022"] =\
            df_electrica().to_json(orient="columns")
    if pathname in (f"/{raiz}/eficiencia_electrica_alertas", ):
        data["j_energia_consumida_2022_ultimo_reg"] =\
            df_alertas_electrica().to_json(orient="columns")
        data["building"] =\
            read_sql(buildind, engine).to_json(orient="columns")

    # Calidad Aire
    if pathname.split("_")[0] in (f"/{raiz}/calidad", ):
        data["latitudes"] =\
            read_sql(latitudes, engine).to_json(orient="columns")
        data["ambientmonitoring_cometsensor_ultimo_reg"] = read_sql(
            ambientmonitoring_cometsensor_ultimo_reg,
            engine).to_json(orient="columns")
    if pathname in (f"/{raiz}/calidad_aire_monitorizacion_por_localizacion", ):
        data["ambientmonitoring_cometsensor"] = read_sql(
            ambientmonitoring_cometsensor, engine).to_json(orient="columns")
    # Clima
    if pathname in (f"/{raiz}/clima_monitorizacion", ):
        data["latitudes"] =\
            read_sql(latitudes, engine).to_json(orient="columns")
        data["weathermonitoring"] =\
            read_sql(weathermonitoring, engine).to_json(orient="columns")
    engine.dispose()
    return [data, ""]
