from dash import dcc
from pandas import read_csv

from components.templates import boton, div_comun, selector
from pages.prevision.prevision_admin_call import *
from pages.prevision.prevision_css import *

id_page = "gemelo_prev_admin"
df_modelos = read_csv(f"Data/modelos/tabla_modelos.csv")
s_panel = {
      "margin-top": "2%",
      "margin-left": "10%",
      "width": "80%",
      "height": "7%"
}

layout = div_comun(
    s_panel_general_admin,
    "",
    [
        div_comun({}, f"div_tabla_{id_page}"),
        div_comun(
            s_panel,
            "",
            [
                div_comun(
                    s_div_boton,
                    "",
                    boton(f"boton_add_{id_page}", "Añadir Modelo", s_boton),
                ),
                div_comun(
                    s_div_boton,
                    "",
                    boton(f"boton_eliminar_{id_page}", "Eliminar Modelo",
                          s_boton),
                ),
            ],
        ),
        div_comun(
            s_panel_add,
            f"panel_add_{id_page}",
            [
                div_comun(
                    s_div_titulo_admin,
                    "",
                    [
                        div_comun(s_div_modelo, "", "Nombre Modelo"),
                        div_comun(s_div_modelo, "", "Id Modelo"),
                    ],
                ),
                div_comun(
                    s_div_inputs,
                    "",
                    [
                        dcc.Input(style=s_input,
                                  id=f"nombre_modelo_{id_page}"),
                        dcc.Input(style=s_input,
                                  id=f"id_modelo_{id_page}"),
                    ],
                ),
                div_comun(
                    s_div_guardar,
                    "",
                    boton(f"bot_guardar_{id_page}", "Guardar",
                          s_boton, disabled=True),
                ),
            ],
            True,
        ),
        div_comun(
            {},
            "",
            [
                dcc.Loading(
                    id="loading-2",
                    children=div_comun({}, f"prevision_admin_{id_page}"),
                    type="default",
                    fullscreen=False,
                )
            ],
        ),
        div_comun(
            s_panel_eliminar,
            f"panel_sup_{id_page}",
            [
                selector(
                    f"eliminar_{id_page}",
                    {"margin-left": "24.5%", "width": "60%"},
                    df_modelos["Nombre Modelo"],
                ),
                div_comun(
                    s_div_boton_aceptar_eliminar,
                    "",
                    boton(
                        f"boton_aceptar_eliminar_{id_page}",
                        "Aceptar",
                        s_boton,
                        disabled=True,
                    ),
                ),
            ],
            True,
        ),
        div_comun(
            s_panel_pop_up,
            f"pop_up_{id_page}",
            [
                div_comun(
                    s_div_pop_up, f"mensage_pop_up_{id_page}",
                    "Tabla actualizada"
                ),
                div_comun(
                    s_div_bot_pop_up,
                    "",
                    boton(f"boton_aceptar_{id_page}", "Aceptar", s_boton),
                ),
            ],
            False,
        ),
    ],
)
