# prevision_admin
s_panel_general_admin = {
    "margin-top": "1%",
    "margin-left": "2%",
    "width": "80%",
    "height": "70%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_panel_pop_up = {
    "position": "absolute",
    "left": "30%",
    "top": "15%",
    "width": "20%",
    "height": "20%",
    "background": "#F1F5F7",
    "border-radius": "50px 50px 50px 50px",
    "border": "1px solid #8380F7",
}

s_div_pop_up = {
    "margin-top": "10%",
    "height": "20%",
    "font-size": "2vmax",
    "text-align": "center",
}

s_div_bot_pop_up = {
    "margin-top": "10%",
    "margin-left": "19%",
    "width": "60%",
    "height": "24%",
}

s_div_boton = {"float": "left", "margin-left": "1%", "width": "48%"}

s_boton = {
    "width": "100%",
    "height": "100%",
    "background": "#add8e6",
    "padding": "2px 2px 0px 2px",
    "border": "1px solid #8380F7",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.5vmax",
}

s_panel_add = {
    "margin-top": "2%",
    "margin-left": "11%",
    "width": "97%",
    "height": "35%",
    "text-align": "center",
}

s_panel_eliminar = {
    "margin-top": "2%",
    "margin-left": "12%",
    "width": "100%",
    "height": "35%",
    "text-align": "center",
}

s_div_titulo_admin = {
    "width": "79.8%",
    "height": "22%",
    "background": "#4189AD"
}

s_div_modelo = {
    "float": "left",
    "margin-top": "1%",
    "width": "50%",
    "height": "80%",
    "color": "white",
    "font-size": "1.2vmax",
}

s_div_inputs = {"width": "80%", "height": "25%"}

s_input = {
    "float": "left",
    "margin-top": "-0.1%",
    "width": "49%",
    "height": "75%",
    "border": "1px solid #4189AD",
    "font-size": "1.2vmax",
    "text-align": "center",
}

s_div_guardar = {
    "margin-top": "1%",
    "margin-left": "2%",
    "width": "12%",
    "height": "20%",
}

s_div_boton_aceptar_eliminar = {
    "margin-left": "44.5%",
    "margin-top": "2%",
    "width": "20%",
}

# prevision_user
s_panel_general_user = {
    "margin-top": "2%",
    "margin-left": "2%",
    "width": "95%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_div_titulo_user = {
    "font-size": "2.2vmax",
    "margin-left": "2%",
    "height": "10%"
}

s_div_selector = {
    "margin-top": "1%",
    "margin-left": "1%",
    "width": "87%",
    "height": "3.1vmax",
}

s_selector = {
    "margin-left": "1%",
    "width": "48%",
    "height": "95%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "background": "#add8e6",
}

s_div_prevision = {
    "margin-top": "4%",
    "margin-left": "2%",
    "width": "85%",
    "height": "30%",
}

s_div_fiabilidad = {
    "margin-top": "5%",
    "margin-left": "2%",
    "width": "85%",
    "height": "30%",
}

s_loading = {"position": "absolute", "top": "18%", "left": "42%"}

# prevision_user_call

s_panel_titulo = {
    "height": "30%",
    "background": "#4189AD",
    "border-botton": "0.1px solid #4189AD",
    "border-right": "0.1px solid #4189AD",
}

s_div_titulo_pred = {
    "font-size": "1.5vmax",
    "text-align": "center",
    "padding": "1.5%",
    "color": "white",
}
