from os import remove

from dash import callback
from dash.dependencies import Input, Output, State
from pandas import DataFrame, read_csv

from pages.prevision.funciones_modelos import (crear_historico_time_series,
                                               obtener_modelo)
from pages.prevision.tabla_modelos import tabla

id_page = "gemelo_prev_admin"


@callback(Output(f"div_tabla_{id_page}", "children"), Input("url", "pathname"))
def display_output(b_url):
    df_modelos = read_csv(f"Data/modelos/tabla_modelos.csv")
    return tabla(id_page, df_modelos)


# Mostrar/oultar añadir modelo/eliminar modelo
@callback(
    [
        Output(f"panel_add_{id_page}", "hidden"),
        Output(f"panel_sup_{id_page}", "hidden"),
        Output(f"boton_add_{id_page}", "style"),
        Output(f"boton_eliminar_{id_page}", "style"),
        Output(f"boton_add_{id_page}", "n_clicks"),
        Output(f"boton_eliminar_{id_page}", "n_clicks"),
    ],
    [
        Input(f"boton_add_{id_page}", "n_clicks"),
        Input(f"boton_eliminar_{id_page}", "n_clicks"),
    ],
    [
        State(f"panel_add_{id_page}", "hidden"),
        State(f"panel_sup_{id_page}", "hidden"),
        State(f"boton_add_{id_page}", "style"),
        State(f"boton_eliminar_{id_page}", "style"),
    ],
    prevent_initial_call=True,
)
def display_output(b_add, b_eliminar, s_add_hidden, s_eliminar_hidden,
                   s_add_style, s_eliminar_style):

    if b_add:
        s_add_hidden = False
        s_eliminar_hidden = True
        s_add_style["border"] = "3px solid #8380F7"
        s_eliminar_style["border"] = "1px solid #8380F7"

    if b_eliminar:
        s_eliminar_hidden = False
        s_add_hidden = True
        s_add_style["border"] = "1px solid #8380F7"
        s_eliminar_style["border"] = "3px solid #8380F7"

    return s_add_hidden, s_eliminar_hidden, s_add_style, s_eliminar_style, 0, 0


# activar boton guardar
@callback(
    Output(f"bot_guardar_{id_page}", "disabled"),
    [
        Input(f"nombre_modelo_{id_page}", "value"),
        Input(f"id_modelo_{id_page}", "value"),
    ],
    [
        State(f"nombre_modelo_{id_page}", "value"),
        State(f"id_modelo_{id_page}", "value"),
    ],
    prevent_initial_call=True,
)
def display_output(b_add, b_guardar, s_modelo, s_id):
    if s_modelo and s_id:
        return False
    return True


# guardar nuevo modelo
@callback(
    [
        Output(f"bot_guardar_{id_page}", "n_clicks"),
        Output(f"boton_aceptar_eliminar_{id_page}", "n_clicks"),
        Output(f"tabla_{id_page}", "data"),
        Output(f"mensage_pop_up_{id_page}", "children"),
        Output(f"mensage_pop_up_{id_page}", "style"),
        Output(f"nombre_modelo_{id_page}", "value"),
        Output(f"id_modelo_{id_page}", "value"),
        Output(f"prevision_admin_{id_page}", "children"),
    ],
    [
        Input(f"bot_guardar_{id_page}", "n_clicks"),
        Input(f"boton_aceptar_eliminar_{id_page}", "n_clicks"),
    ],
    [
        State(f"nombre_modelo_{id_page}", "value"),
        State(f"id_modelo_{id_page}", "value"),
        State(f"eliminar_{id_page}", "value"),
        State(f"mensage_pop_up_{id_page}", "style"),
    ],
    prevent_initial_call=True,
)
def display_output(b_guardar, b_eliminar, s_mod, s_id, s_value_eliminar,
                   s_pop_up):

    path = f"Data/modelos/tabla_modelos.csv"
    df = read_csv(path)[["Nombre Modelo", "Id Modelo", "Tipo"]]
    mensaje = "Tabla Actualizada"

    if b_guardar:
        modelo = df[df["Nombre Modelo"] == s_mod]

        if modelo.shape[0] == 0:
            respuesta = obtener_modelo(s_id)
            if respuesta == "Modelo no encontrado en IAAAS":
                mensaje = respuesta
                s_pop_up["font-size"] = "1.5vmax"
            else:
                df.loc[df.shape[0]] = [s_mod, s_id, respuesta]
                df.to_csv(path)
                if respuesta in ('Serie Temporal', ):
                    crear_historico_time_series(s_id)
        else:
            mensaje = "Nombre de modelo ya utilizado"
            s_pop_up["font-size"] = "1.5vmax"

    if b_eliminar:
        df_modelos = read_csv(path)
        id_modelo = df_modelos[df_modelos['Nombre Modelo'] == s_value_eliminar]['Id Modelo'].iloc[0]
        remove(f"Data/modelos/json/{id_modelo}.json")
        if df_modelos['Tipo'].iloc[0] == 'Serie Temporal':
            remove(
                f"Data/modelos/previsiones_time_series/{id_modelo}.csv"
            )
        df = df[df["Nombre Modelo"] != s_value_eliminar]
        df_modelos.to_csv(path)

    return [0, 0, df.to_dict("records"), mensaje, s_pop_up, "", "", ""]



@callback(
    [
        Output(f"pop_up_{id_page}", "hidden"),
        Output(f"boton_aceptar_{id_page}", "n_clicks"),
        Output(f"eliminar_{id_page}", "options"),
    ],
    [
        Input(f"tabla_{id_page}", "data"),
        Input(f"tabla_{id_page}", "columns"),
        Input(f"boton_aceptar_{id_page}", "n_clicks"),
        State(f"pop_up_{id_page}", "hidden"),
    ],
    prevent_initial_call=True,
)
def display_output(rows, columns, b_aceptar, s_hidden):

    df = DataFrame(rows, columns=[c["name"] for c in columns])
    df.to_csv(f"Data/modelos/tabla_modelos.csv")
    pop_up_hidden = not s_hidden
    if b_aceptar:
        pop_up_hidden = True

    return [pop_up_hidden, 0, df["Nombre Modelo"]]


@callback(
    Output(f"boton_aceptar_eliminar_{id_page}", "disabled"),
    Input(f"eliminar_{id_page}", "value"),
    prevent_initial_call=True,
)
def display_output(b_value):
    if b_value:
        return False
    return True
