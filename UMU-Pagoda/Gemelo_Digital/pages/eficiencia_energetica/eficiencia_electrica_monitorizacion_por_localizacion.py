from callbacks.eficiencia_energetica.eficiencia_energetica_call import \
    callbacks_eficiencia
from components.funcion_energia_layout import layout_eficiencia
from components.listas import edificios_electrica
from components.templates import div_comun, selector

color_boton = "#add8e6"
s_selector = {
    "float": "left",
    "marginLeft": "1%",
    "margin-right": "1%",
    "marginTop": "2%",
    "width": "25%",
    "height": "7%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.3vmax",
    "background": color_boton,
}

layout = [
    div_comun(
        s_selector,
        f"div_sel_localizacion_electrica_loc",
        selector(
            f"sel_localizacion_electrica_loc",
            {},
            list(edificios_electrica.values()),
            value="Facultad Psicologia",
            placeholder="Seleccione Localización",
        ),
    ),
    layout_eficiencia("electrica_loc"),
]

callbacks_eficiencia(
    "electrica_loc", "j_energia_consumida_2022", "consumida",
    edificios_electrica, True
)
