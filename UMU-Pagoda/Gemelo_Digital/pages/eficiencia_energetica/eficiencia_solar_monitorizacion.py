from callbacks.eficiencia_energetica.eficiencia_energetica_call import \
    callbacks_eficiencia
from components.funcion_energia_layout import layout_eficiencia
from components.listas import edificios_solar

layout = layout_eficiencia("solar")

callbacks_eficiencia(
    "solar", "j_energia_solar_creada_2022", "producida", edificios_solar
)
