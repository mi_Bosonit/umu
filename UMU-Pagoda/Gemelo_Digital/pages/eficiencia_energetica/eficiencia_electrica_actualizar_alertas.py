from components.funciones_actualizar_alertas import layout_actualizar_alertas

columns = [
    dict(id="Localización", name="Localización", editable=False),
    dict(id="Max", name="Max", type="numeric"),
]

style_data_conditional = [
    {
        "if": {"column_id": ["Localización"], "column_id": ["Localización"]},
        "width": "50%",
    }
]
layout = layout_actualizar_alertas(
    "eficiencia_electrica", columns, style_data_conditional
)
