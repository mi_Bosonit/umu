from callbacks.eficiencia_energetica.eficiencia_energetica_call import \
    callbacks_eficiencia
from components.funcion_energia_layout import layout_eficiencia
from components.listas import edificios_electrica

layout = layout_eficiencia("electrica")

callbacks_eficiencia(
    "electrica", "j_energia_consumida_2022", "consumida", edificios_electrica
)
