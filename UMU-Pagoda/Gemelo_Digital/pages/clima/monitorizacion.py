import callbacks.clima.clima_monitorizacion_call
from assets.css_clima_monitorizacion import *
from components.templates import div_comun

layout = [
    div_comun(
        s_panel_general,
        "",
        [
            div_comun(
                {"marginTop": "1%", "marginLeft": "2.7%", "height": "7.5%"},
                "",
                [
                    div_comun(s_selector, "div_selector_monitorizacion_clima"),
                    div_comun(s_selector, "div_selector_valor_clima"),
                ],
            ),
            div_comun(s_panel_mapa, "div_mapa"),
            div_comun(s_panel_grafico, "div_valores"),
        ],
    )
]
