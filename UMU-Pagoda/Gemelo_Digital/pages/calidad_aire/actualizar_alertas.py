from components.funciones_actualizar_alertas import layout_actualizar_alertas

columns = [
    dict(id="Localización", name="Localización", editable=False),
    dict(id="Aula", name="Aula", editable=False),
    dict(id="Temp Min (ºC)", name="Temp Min (ºC)", type="numeric"),
    dict(id="Temp Max (ºC)", name="Temp Max (ºC)", type="numeric"),
    dict(id="Co2 Max (ppm)", name="Co2 Max (ppm)", type="numeric"),
    dict(id="Humedad Min (%H)", name="Humedad Min (%H)", type="numeric"),
    dict(id="Humedad Max (%H)", name="Humedad Max (%H)", type="numeric"),
]

style_data_conditional = [
    {
        "if": {"column_id": ["Localización"], "column_id": ["Localización"]},
        "width": "25%",
    },
    {"if": {"column_id": ["Aula"], "column_id": ["Aula"]}, "width": "20%"},
    {
        "if": {"column_id": ["Temp Min (ºC)"], "column_id": ["Temp Min (ºC)"]},
        "width": "10%",
    },
    {
        "if": {"column_id": ["Temp Max (ºC)"], "column_id": ["Temp Max (ºC)"]},
        "width": "10%",
    },
    {
        "if": {"column_id": ["Co2 Max (ppm)"], "column_id": ["Co2 Max (ppm)"]},
        "width": "10%",
    },
    {
        "if": {"column_id": ["Humedad(%H)"], "column_id": ["Humedad (%H)"]},
        "width": "10%",
    },
    {
        "if": {"column_id": ["Humedad (%H)"], "column_id": ["Humedad (%H)"]},
        "width": "10%",
    },
]
layout = layout_actualizar_alertas("calidad_aire", columns,
                                   style_data_conditional)
