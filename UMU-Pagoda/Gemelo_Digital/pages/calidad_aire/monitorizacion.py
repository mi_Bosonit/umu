import callbacks.calidad_aire.calidad_aire_monitorizacion_call
from components.templates import div_comun

s_panel_general = {
    "marginTop": "2%",
    "marginLeft": "2%",
    "width": "95%",
    "height": "98%",
}


layout = div_comun(
    s_panel_general,
    "div_panel_monitorizacion_aire",
    [div_comun({}, "tabla_aire"), div_comun({"marginTop": "2%"}, "mapa_aire")],
)
