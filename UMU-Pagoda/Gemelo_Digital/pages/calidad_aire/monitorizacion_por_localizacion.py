from dash import dcc

import callbacks.calidad_aire.calidad_aire_monitorizacion_por_localizacion_call
from assets.css_calidad_aire_monitorizacion_por_loc import *
from components.listas import edificios_calidad_aire
from components.templates import div_comun, selector

lista_de_valores = ["Temperatura", "Humedad Relativa",
                    "Presión Atmosférica", "Co2"]


layout = [
    div_comun(
        s_div_selectores,
        "",
        [
            div_comun(
                s_selector,
                "",
                selector(
                    "sel_loc_aire",
                    {},
                    edificios_calidad_aire,
                    value="Aulario Norte",
                    placeholder="Seleccione Localización",
                ),
            ),
            div_comun(
                s_selector2,
                "",
                selector("sel_aula_aire", {}, [],
                         placeholder="Seleccione Aula"),
            ),
            div_comun(
                s_selector2,
                "",
                selector(
                    "sel_valor_aire",
                    {},
                    lista_de_valores,
                    value="Temperatura",
                    placeholder="Seleccione Valor",
                ),
            ),
        ],
    ),
    div_comun(
        s_loading,
        "",
        dcc.Loading(
            id="loading-2",
            type="default",
            fullscreen=False,
            children=div_comun(s_div_loading, "loading_calidad_aire_loc"),
        ),
    ),
    div_comun(
        s_panel_graficos,
        "",
        [
            div_comun(s_ult_reg, "ultimo_reg", ""),
            div_comun(s_24_horas, "graficos_aire_loc", ""),
        ],
    ),
]
