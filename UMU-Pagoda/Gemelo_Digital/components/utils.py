from datetime import datetime, timezone

from sqlalchemy import create_engine

from configuracion import bbdd, bbdd2


def conection():
    return create_engine(bbdd)


def conection_2():
    return create_engine(bbdd2)


def time_instant_to_date(df, col_to, col_from):

    df[col_to] = list(
        map(
            lambda x: datetime.fromtimestamp(x / 1000, timezone.utc).replace(
                tzinfo=None
            ),
            df[col_from],
        )
    )
    return df


def time_instant_to_date_sin_utc(df, col_to, col_from):

    df[col_to] = list(
        map(
            lambda x: datetime.fromtimestamp(x / 1000).replace(tzinfo=None),
            df[col_from],
        )
    )
    return df


def mes_dia_hora(df, col_from):

    df["Mes"] = list(map(lambda x: x.date().month, df[col_from]))

    df["Dia"] = list(map(lambda x: x.date().day, df[col_from]))

    df["Hora"] = list(map(lambda x: x.time(), df[col_from]))

    return df
