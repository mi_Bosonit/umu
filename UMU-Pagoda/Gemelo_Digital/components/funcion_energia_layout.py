from dash import dcc

from components.listas import meses
from components.templates import boton, div_comun, selector

color_boton = "#add8e6"
s_panel_general = {
    "marginTop": "2%",
    "marginLeft": "3%",
    "width": "95%",
    "height": "94%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}
s_boton = {
    "float": "left",
    "width": "20%",
    "height": "98%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-size": "1.5vmax",
    "border": "1px solid #8380F7",
}
s_selector = {
    "float": "left",
    "marginLeft": "1%",
    "width": "15%",
    "height": "95%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1.4vmax",
    "background": color_boton,
}
s_selector2 = {
    "float": "left",
    "marginLeft": "1%",
    "width": "20%",
    "height": "95%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1.4vmax",
    "background": color_boton,
}
s_grafico = {"marginTop": "2%", "width": "95%"}
s_loading = {"position": "absolute", "top": "18%", "left": "42%"}
s_div_loading = {
  "marginTop": "4%",
  "marginLeft": "2%",
  "width": "85%",
  "height": "30%"
}


def layout_eficiencia(id):
    return div_comun(
        s_panel_general,
        "",
        [
            div_comun(
                {"height": "8%"},
                "",
                [
                    boton(
                        f"boton_esta_semana_{id}",
                        "Semana en Curso",
                        s_boton,
                        n_clicks=1,
                    ),
                    div_comun(
                        s_selector,
                        "",
                        selector(
                            f"sel_mes_{id}", {}, meses,
                            placeholder="Seleccione Mes"
                        ),
                    ),
                    div_comun(
                        s_selector2,
                        f"div_sel_dia_tramo_{id}",
                        selector(
                            f"sel_dia_tramo_{id}",
                            {},
                            ["Por Día", "Por Tramo Horario"],
                            placeholder="Seleccione Período",
                        ),
                        True,
                    ),
                ],
            ),
            div_comun(
                s_loading,
                "",
                dcc.Loading(
                    id="loading-2",
                    type="default",
                    fullscreen=False,
                    children=div_comun(s_div_loading, f"user_loading_{id}"),
                ),
            ),
            div_comun(s_grafico, f"panel_esta_semana_{id}"),
            div_comun(s_grafico, f"mes_por_dias_{id}"),
            div_comun(s_grafico, f"mes_por_tramo_{id}"),
        ],
    )
