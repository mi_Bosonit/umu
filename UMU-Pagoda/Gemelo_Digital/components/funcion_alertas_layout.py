from components.templates import div_comun

s_panel_alertas = {"float": "left", "marginLeft": "2%", "width": "70%"}
s_div = {"float": "left", "marginTop": "2%", "width": "33%", "height": "100%"}
s_div2 = {
    "width": "99%",
    "height": "95%",
    "border-radius": "35px 35px 5px 5px",
    "background": "#F1F5F7",
}
s_div_mapa = {
    "float": "left",
    "marginLeft": "1%",
    "marginTop": "1.5%",
    "width": "25%",
    "height": "65%",
    "border-radius": "35px 35px 5px 5px",
    "background": "#F1F5F7",
}
s_mapa = {
    "marginLeft": "5%",
    "marginTop": "2%",
    "width": "90%",
    "height": "90%"
}


def alertas_layout(id):
    return [
        div_comun(
            s_panel_alertas,
            "",
            [
                div_comun(s_div, "",
                          div_comun(s_div2, f"alertas_{id}_tiempo")),
                div_comun(s_div, "",
                          div_comun(s_div2, f"alertas_{id}_distance")),
                div_comun(s_div, "",
                          div_comun(s_div2, f"no_alertas_{id}")),
            ],
        ),
        div_comun(s_div_mapa, "", div_comun(s_mapa, f"mapa_alertas_{id}")),
    ]
