import glob
from datetime import datetime

from pandas import concat, read_csv

from components.listas import meses
from configuracion import tabla_bbdd


def obtener_df(paths):
    path = paths[0]
    files = glob.glob(path + "/*.csv")
    content = [read_csv(filename, index_col=None) for filename in files]
    df = concat(content).reset_index(drop=True).drop(["Unnamed: 0"], axis=1)

    for path in paths[1:]:
        files = glob.glob(path + "/*.csv")
        content = [read_csv(filename, index_col=None) for filename in files]
        df_2 =\
            concat(content).reset_index(drop=True).drop(["Unnamed: 0"], axis=1)
        df = concat([df, df_2]).reset_index(drop=True)

    df.columns = [
        "mes",
        "dia_semana",
        "tramo",
        "hora",
        "energia",
        "p",
        "controlled_asset",
    ]
    df["hora"] =\
        [datetime.strptime(fecha, "%Y-%m-%d %H:%M:%S") for fecha in df["hora"]]
    return df


# Eficiencia Energetica
# Solar
def df_solar():
    paths = [
        "Data/energia_solar/building-laib-departamental",
        "Data/energia_solar/building-granja-veterinaria",
        "Data/energia_solar/offstreetparking-campus-espinardo-zona-norte",
        "Data/energia_solar/building-pleiades",
    ]
    return obtener_df(paths)


# Electrica
def df_electrica():
    paths = [
        "Data/energia_electrica/device-powermeter-facultad-psicologia",
        "Data/energia_electrica/device-powermeter-facultad-quimica",
        "Data/energia_electrica/device-powermeter-facultad-veterinaria1",
        "Data/energia_electrica/device-powermeter-facultad-veterinaria2",
    ]
    return obtener_df(paths)


def df_alertas_electrica():

    paths = [
        "device-powermeter-facultad-quimica",
        "device-powermeter-facultad-veterinaria1",
        "device-powermeter-facultad-veterinaria2",
    ]

    df = read_csv(
        f"""Data/energia_electrica/device-powermeter-facultad-psicologia/{
            meses[datetime.now().date().month - 1]}.csv"""
    )
    df = df.drop_duplicates(subset=["ocb_id"], keep="first")

    for path in paths:
        path = f"""Data/energia_electrica/{path}/{
            meses[datetime.now().date().month - 1]}.csv"""
        df_2 = read_csv(path)
        df_2 = df_2.drop_duplicates(subset=["ocb_id"], keep="first")
        df = concat([df, df_2]).reset_index(drop=True).drop(["Unnamed: 0"],
                                                            axis=1)

    df.columns = [
        "mes",
        "dia_semana",
        "tramo",
        "time_instant",
        "energia",
        "p",
        "description",
    ]
    df["time_instant"] = [
        datetime.strptime(fecha, "%Y-%m-%d %H:%M:%S")
        for fecha in df["time_instant"]
    ]

    return df


buildind = f"""SELECT description, lat, long FROM {tabla_bbdd}.building"""

# Calidad Aire
latitudes = f"""SELECT ocb_id AS controlled_asset, description , lat, long
                FROM {tabla_bbdd}.building """

ambientmonitoring_cometsensor_ultimo_reg =\
    f"""SELECT DISTINCT ON (ocb_id) ocb_id, time_instant,
               controlled_asset, area_served AS "Aula",
               temperature AS "Temperatura",
               relative_humidity AS "Humedad Relativa",
               barometric_pressure AS "Presión Atmosférica",
               co2 AS "Co2"
        FROM {tabla_bbdd}.ambientmonitoring_cometsensor
        WHERE time_instant > '2022-07-13 07:30:00'
        ORDER BY ocb_id, time_instant DESC """
ambientmonitoring_cometsensor =\
    f"""SELECT time_instant, controlled_asset, area_served AS "Aula",
               temperature AS "Temperatura",
               relative_humidity AS "Humedad Relativa",
               barometric_pressure AS "Presión Atmosférica",
               co2 AS "Co2"
        FROM {tabla_bbdd}.ambientmonitoring_cometsensor
        WHERE time_instant > Now() - '24 hours'::interval
        ORDER BY time_instant asc """

# Clima
weathermonitoring =\
    f"""SELECT time_instant, air_temperature,
               relative_humidity, solar_radiation,
               precipitation, atmospheric_pressure,
               controlled_asset
        FROM {tabla_bbdd}.weathermonitoring
        WHERE time_instant > Now() - '12 hours'::interval
        ORDER BY time_instant """
