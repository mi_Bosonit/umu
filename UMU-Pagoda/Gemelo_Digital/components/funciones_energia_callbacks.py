from datetime import datetime

from dash import dcc

from components.templates import bar, div_comun
from components.utils import mes_dia_hora, time_instant_to_date

s_titulo = {
    "height": "10%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "2vmax",
}


# Funciones Auxiliares
def tratar_df(df):

    df["Localización"] = df["controlled_asset"]
    df["Energía"] = df["energia"]
    df["Período"] = df["p"]

    return df


def esta_semana(df, titulo):

    df = tratar_df(df)

    df["semana"] = list(map(lambda x: x.isocalendar()[1], df["hora"]))

    df = df[df["semana"] == datetime.now().isocalendar()[1]]
    df["Fecha"] = df["hora"]

    fig = bar(
        df,
        x="Fecha",
        y="Energía",
        title="",
        yaxis_title=f"Energía {titulo} en Kilo Watio/hora",
        height=400,
        color="Período",
        hover_data=["Fecha", "Localización"],
    )

    return div_comun(
        {},
        "",
        [div_comun(s_titulo, "", f"Energía {titulo} por hora"),
         dcc.Graph(figure=fig)],
    )


def seleccionar_periodo(df, mes, titulo, periodo):

    df = df[df["mes"] == mes]
    df = tratar_df(df)
    df = mes_dia_hora(df, "hora")
    df["Tramo"] = df["tramo"]

    if periodo in ("Por Día",):

        fig = bar(
            df,
            x="Dia",
            y="Energía",
            title="",
            yaxis_title=f"Energía {titulo} en Kilo Watio/hora",
            xaxis_title="Día",
            height=400,
            color="Período",
            hover_data=["Hora", "Período", "Localización"],
        )
        titulo_div = f"Energía {titulo} por día"

    else:
        df["Energía"] = df["Energía"] / 1000

        fig = bar(
            df,
            x="Tramo",
            y="Energía",
            title=f"",
            yaxis_title=f"Energía {titulo} en Mega Watio/hora",
            xaxis_title="Tramo Horario",
            height=400,
            color="Período",
            hover_data=["Período", "Dia", "Localización"],
        )

        titulo_div = f"Energía {titulo} por tramo horario"

    return div_comun(
        {}, "", [div_comun(s_titulo, "", titulo_div), dcc.Graph(figure=fig)]
    )


# Seleccionar Esta semana o Mes
def cuerpo_semana(
    semana, mes, style_boton, df, lista_edificios, titulo, localizacion=""
):

    style_boton["border"] = "0.5px solid #75ADBC"
    obj = ""
    hidden = True

    for x in lista_edificios.keys():
        df.loc[df["controlled_asset"] == x,
               "controlled_asset"] = lista_edificios[x]

    if len(localizacion) > 1:
        df = df[df["controlled_asset"] == localizacion]

    df = time_instant_to_date(df, "hora", "hora")

    if semana:
        style_boton["border"] = "3px solid #75ADBC"
        obj = div_comun({"height": "98%"}, "", esta_semana(df, titulo))
        mes = ""

    if mes:
        hidden = False

    return [obj, 0, mes, hidden, style_boton]


# Seleccionar periodo
def cuerpo_periodo(sel_dia_tramo, mes, df, lista_edificios, titulo,
                   localizacion=""):
    obj_por_dia = ""
    obj_por_tramo = ""
    if sel_dia_tramo and mes:

        for x in lista_edificios.keys():
            df.loc[df["controlled_asset"] == x,
                   "controlled_asset"] = lista_edificios[x]

        if len(localizacion) > 1:
            df = df[df["controlled_asset"] == localizacion]

        df = time_instant_to_date(df, "hora", "hora")

        if sel_dia_tramo == "Por Día":
            obj_por_dia = div_comun(
                {"height": "98%"},
                "",
                seleccionar_periodo(df, mes, titulo, sel_dia_tramo),
            )
        if sel_dia_tramo == "Por Tramo Horario":
            obj_por_tramo = div_comun(
                {"height": "98%"},
                "",
                seleccionar_periodo(df, mes, titulo, sel_dia_tramo),
            )

    return [obj_por_dia, obj_por_tramo]
