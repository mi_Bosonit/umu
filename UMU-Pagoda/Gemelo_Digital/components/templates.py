import dash_bootstrap_components as dbc
import plotly.express as px
from dash import dash_table, dcc, html

color_boton = "#add8e6"
style_div_barra_boton = {
    "marginTop": "10px",
    "width": "100%",
    "height": "33px",
    "background": "#EDFAFE",
}

s_boton = {
    "width": "95%",
    "height": "100%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": "1px solid #8380F7",
}


def div_comun(style, id="", children="", hidden=False):
    return html.Div(style=style, children=children, id=id, hidden=hidden)


def boton(id, nombre, style, hidden=False, disabled=False, n_clicks=0):
    return html.Button(
        style=style,
        id=id,
        n_clicks=n_clicks,
        hidden=hidden,
        children=nombre,
        disabled=disabled,
    )


def selector(id, style, options=[], value="", placeholder=""):
    return dcc.Dropdown(
        style=style, id=id, options=options, value=value,
        placeholder=placeholder
    )


def bloque_panel_derecho(id_barra, nombre_barra, id_panel, children):
    return div_comun(
        {"width": "98%", "marginTop": "0.5%", "marginLeft": "3%"},
        "",
        [
            div_comun({"height": "30px"}, "",
                      boton(id_barra, nombre_barra, s_boton)),
            div_comun({}, id_panel, children),
        ],
    )


# Tablas
def tabla(
    id, df, columnas_tabla, s_data_conditional, page_size=18,
    row_selectable="multi"
):

    s_table = {
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "border": "0.1px solid white",
        "overflow-x": "auto",
    }
    s_data = {
        "padding": "6px",
        "align-items": "center",
        "width": "8vmax",
        "min-width": "8vmax",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.15vmax",
    }
    s_header = {
        "padding": "6px",
        "color": "#143CB8",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.25vmax",
        "border": "0.1px solid white",
    }
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columnas_tabla,
                id=id,
                row_selectable=row_selectable,
                page_size=page_size,
                selected_rows=[],
                style_table=s_table,
                style_data=s_data,
                style_header=s_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=s_data_conditional,
            )
        ]
    )


def tabla_alertas(id, df, columns, s_data_conditional):
    s_table = {"box-sizing": "inherit"}
    s_data = {
        "width": "33%",
        "padding": "6px",
        "align-items": "center",
        "border-right": "0.1px solid white",
        "border-left": "0.1px solid white",
        "height": "50px",
        "color": "#44535A",
        "font-size": "1rem",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
    }
    s_header = {
        "padding": "6px",
        "color": "white",
        "background": "#4189AD",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1rem",
        "height": "55px",
        "border-botton": "0.1px solid #4189AD",
        "border-right": "0.1px solid #4189AD",
    }
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columns,
                id=f"tabla_{id}",
                page_size=6,
                style_table=s_table,
                style_data=s_data,
                style_header=s_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=s_data_conditional,
                editable=True,
            )
        ]
    )


# Plotly
# bar
def bar(
    df,
    x,
    y,
    text=None,
    title="",
    yaxis_title="",
    xaxis_title="",
    height=240,
    color=None,
    hover_data=[],
):

    fig = px.bar(
        df,
        x=x,
        y=y,
        text=text,
        title=title,
        color=color,
        hover_data=hover_data,
        color_discrete_sequence=["#CE7B54", "#88dd44", "#5D7297"],
    )
    fig.update_layout(
        title={
            "font": {
                "family": "Roboto, Helvetica, Arial, sans-serif",
                "color": "black",
                "size": 23,
            }
        },
        autosize=True,
        height=height,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        xaxis={"showgrid": False},
        yaxis={"showgrid": False},
        plot_bgcolor="white",
        margin=dict(t=35, l=0, b=30, r=30),
    )
    fig.update_traces(textposition="outside", cliponaxis=False,
                      textfont_size=14)
    return fig


# line
def line(df, x, y, title="", yaxis_title="", xaxis_title="", mode="lines"):
    fig = px.line(
        df,
        x=x,
        y=y,
        title=title,
        color_discrete_sequence=["#4189AD", "#88dd44", "#CE7B54"],
    )
    fig.update_layout(
        title={
            "font": {
                "family": "Roboto, Helvetica, Arial, sans-serif",
                "color": "black",
                "size": 25,
            }
        },
        autosize=True,
        yaxis_title=yaxis_title,
        xaxis_title=xaxis_title,
        xaxis={"showgrid": False},
        yaxis={"showgrid": False},
        plot_bgcolor="white",
        margin=dict(t=60, l=0, b=0, r=0),
    )
    fig.update_traces(mode=mode)
    return fig
