from datetime import datetime, timedelta

from dash import dash_table

percentage = dash_table.FormatTemplate.percentage(2)

meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
]

edificios_solar = {
    "building-laib-departamental": "Laib Departamental",
    "parking-campus-espinardo-zona-norte":
        "Parking Campus Espinardo Zona Norte",
    "offstreetparking-campus-espinardo-zona-norte":
        "Parking Campus Espinardo Zona Norte",
    "building-granja-veterinaria": "Granja Veterinaria",
    "building-pleiades": "Pleiades",
}

edificios_electrica = {
    "device-powermeter-facultad-quimica": "Facultad Quimica",
    "device-powermeter-facultad-veterinaria1": "Facultad Veterinaria 1",
    "device-powermeter-facultad-veterinaria2": "Facultad Veterinaria 2",
    "device-powermeter-facultad-psicologia": "Facultad Psicologia",
}
edificios_electrica_mapa = {
    "device-powermeter-facultad-quimica": "Facultad de Química",
    "device-powermeter-facultad-veterinaria1": "Facultad de Veterinaria",
    "device-powermeter-facultad-veterinaria2": "Facultad de Veterinaria",
    "device-powermeter-facultad-psicologia": "Facultad de Psicología",
}

edificios_clima = [
    "Edificio LAIB/Departamental",
    "Aulario de la Merced",
    "Centro de Investigación de Carácter Mixto. CIAVyS-VITALYS",
]

unidades_clima = {
    "Temperatura": ["air_temperature", "ºC"],
    "Radiación Solar": ["solar_radiation", "W/m^2"],
    "Precipitación": ["precipitation", "mm"],
    "Humedad Relativa": ["relative_humidity", "%H"],
    "Presión Atmosférica": ["atmospheric_pressure", "kPa"],
}

unidades_calidad_aire = {
    "Temperatura": ["air_temperature", "ºC"],
    "Radiación Solar": ["solar_radiation", "W/m^2"],
    "Precipitación": ["precipitation", "mm"],
    "Humedad Relativa": ["relative_humidity", "%H"],
    "Presión Atmosférica": ["atmospheric_pressure", "kPa"],
    "Co2": ["co2", "PPM"],
}

columnas_moni_aire = [
    dict(id="description", name="Localización"),
    dict(id="Aula", name="Aula"),
    dict(id="Temperatura", name="Temperatura"),
    dict(
        id="Humedad Relativa",
        name="Humedad Relativa",
        type="numeric",
        format=percentage,
    ),
    dict(id="Presión Atmosférica", name="Presión Atmosférica"),
    dict(id="Co2", name="Co2"),
    dict(id="Fecha", name="Fecha"),
    dict(id="Hora", name="Hora"),
]

style_data_conditional_monitorizacion_aire = [
    {
        "if": {"column_id": ["description"], "column_id": ["description"]},
        "width": "13vmax",
        "min-width": "13vmax",
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "color": "white",
        "background": "linear-gradient(100deg, #394F93, #052073) padding-box",
    },
    {"if": {"filter_query": "{Co2} > 600",
            "column_id": ["Co2"]},
     "color": "orange"},
    {"if": {"filter_query": "{Co2} > 700",
            "column_id": ["Co2"]},
     "color": "red"},
    {
        "if": {"filter_query": "{Temperatura} > 25",
               "column_id": ["Temperatura"]},
        "color": "orange",
    },
    {
        "if": {"filter_query": "{Temperatura} > 26",
               "column_id": ["Temperatura"]},
        "color": "red",
    },
    {
        "if": {"filter_query": "{Temperatura} < 23",
               "column_id": ["Temperatura"]},
        "color": "red",
    },
    {
        "if": {"filter_query": "{Humedad} < 30",
               "column_id": ["Humedad Relativa"]},
        "color": "red",
    },
    {
        "if": {"filter_query": "{Humedad} > 80",
               "column_id": ["Humedad Relativa"]},
        "color": "red",
    },
    {
        "if": {
            "filter_query": "{{Hora}} < {}".format(
                (datetime.now() - timedelta(hours=5)).time()
            ),
            "column_id": ["Hora"],
        },
        "color": "#FD0703",
    },
    {
        "if": {
            "filter_query": "{{Fecha}} != {}".format(datetime.now().date()),
            "column_id": ["Fecha", "Hora"],
        },
        "color": "#FD0703",
    },
]

edificios_calidad_aire = [
    "Aulario General",
    "Aulario Norte",
    "Biblioteca Antonio de Nebrija",
    "Biblioteca General",
    "Edificio B - Campus de Lorca",
    "Edificio C - Campus de Lorca",
    "Edificio D",
    "Edificio Rector Sabater",
    "Edificio Saavedra Fajardo",
    "Facultad de Bellas Artes",
    "Facultad de Biología",
    "Facultad de Ciencias del Deporte",
    "Facultad de Economía y Empresa",
    "Facultad de Matemáticas",
    "Facultad de Química",
    "Facultad de Veterinaria",
    "Facultad de Óptica y Optometría",
]

calidad_aire_por_dispositivo = {
    "device-20280196": ["Biblioteca General 1ª Planta", "c1"],
    "device-20280197": ["Facultad de Ciencias del Deporte B1.0.026", "c2"],
    "device-20280198": ["Aulario Norte B1.0.010", "c3"],
    "device-20280199": ["Edificio Saavedra Fajardo B1.0.007", "c4"],
    "device-20280200": ["Facultad de Bellas Artes B2.0.002", "c5"],
    "device-20280273": ["Facultad de Óptica y Optometría B1.1.022", "c6"],
    "device-20280275": ["Facultad de Ciencias del Deporte B1.0.020", "c7"],
    "device-20280276": ["Aulario Norte B1.0.012", "c8"],
    "device-20280277": ["Facultad de Economía y Empresa B1.4.010", "c9"],
    "device-20280279": ["Facultad de Matemáticas B1.2.024", "c10"],
    "device-20280280": ["Facultad de Veterinaria B1.0.081", "c11"],
    "device-20280281": ["Aulario Norte B1.0.027", "c12"],
    "device-20280282": ["Facultad de Química B1.0.046", "c13"],
    "device-20280283": ["Biblioteca Antonio de Nebrija B1.1.011", "c14"],
    "device-20280284": ["Facultad de Economía y Empresa B1.4.019", "c15"],
    "device-20280289": ["Aulario Norte B1.0.025", "c16"],
    "device-20280292": ["Edificio C - Campus de Lorca B1.3.003", "c17"],
    "device-20280293": ["Aulario Norte B1.0.007", "c18"],
    "device-20280294": ["Edificio D - Campus de Lorca B1.0.028", "c19"],
    "device-20280295": ["Edificio Rector Sabater B1.0.028", "c20"],
    "device-20280296": ["Facultad de Economía y Empresa B1.4.020", "c21"],
    "device-20280297": ["Aulario General B1.0.008", "c22"],
    "device-20280298": ["Edificio B - Campus de Lorca B1.0.004", "c23"],
    "device-20280299": ["Facultad de Biología B1.0.021", "c24"],
}
# Calidad Agua
# Depuradora
depuradora_dispositivo = {"device-IC0104E17000800056": "Depuradora"}
