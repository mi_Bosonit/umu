from dash import callback
from dash.dependencies import Input, Output
from pandas import DataFrame, concat, read_csv

from components.templates import boton, div_comun, selector, tabla_alertas

color_boton = "#add8e6"
off = "1px solid #8380F7"
s_panel = {
    "marginTop": "3%",
    "marginLeft": "2%",
    "width": "90%",
    "height": "90%"
}
s_boton = {
    "marginLeft": "40%",
    "marginTop": "5%",
    "width": "25%",
    "height": "20%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": off,
}

s_div_selectores = {
    "height": "10%",
    "width": "98%",
    "marginTop": "1%",
    "marginLeft": "0%",
}

s_selector = {
    "marginLeft": "0%",
    "width": "34%",
    "height": "80%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "background": color_boton,
}

s_panel_pop_up = {
    "position": "absolute",
    "left": "40%",
    "top": "30%",
    "width": "20%",
    "height": "20%",
    "background": "#F1F5F7",
    "border": "0.001px solid lightgrey",
    "border-radius": "50px 50px 50px 50px",
}
s_div_pop_up = {
    "marginTop": "10%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.2rem",
    "text-align": "center",
}


def layout_actualizar_alertas(id, columns, style_data_conditional):

    layout = div_comun(
        s_panel,
        "",
        [
            div_comun(
                s_div_selectores,
                "",
                div_comun(
                    s_selector,
                    "",
                    selector(
                        f"selector_alertas_{id}",
                        {},
                        [],
                        placeholder="Seleccione Localización",
                    ),
                ),
            ),
            tabla_alertas(
                id,
                read_csv(f"Data/valores_alertas/{id}.csv"),
                columns,
                style_data_conditional,
            ),
            div_comun(
                s_panel_pop_up,
                f"pop_up_{id}",
                [
                    div_comun(s_div_pop_up, "", "Tabla actualizada"),
                    boton(f"boton_aceptar_{id}", "Aceptar", s_boton),
                ],
                True,
            ),
        ],
    )

    @callback(Output(f"selector_alertas_{id}", "options"),
              Input(f"url", "pathname"))
    def display_output(url):
        df = read_csv(f"Data/valores_alertas/{id}.csv")
        return sorted(list(set(df["Localización"])))

    @callback(Output(f"tabla_{id}", "data"),
              Input(f"selector_alertas_{id}", "value"))
    def display_output(b_selector):
        df = read_csv(f"Data/valores_alertas/{id}.csv")

        if b_selector:
            df = df[df["Localización"] == b_selector]

        return df.to_dict("records")

    @callback(
        [
            Output(f"pop_up_{id}", "hidden"),
            Output(f"boton_aceptar_{id}", "n_clicks")
        ],
        [
            Input(f"tabla_{id}", "data"),
            Input(f"tabla_{id}", "columns"),
            Input(f"boton_aceptar_{id}", "n_clicks"),
        ],
        prevent_initial_call=True,
    )
    def display_output(rows, columns, b_aceptar):

        if b_aceptar:
            return [True, 0]

        df = read_csv(f"Data/valores_alertas/{id}.csv").drop(["Unnamed: 0"],
                                                             axis=1)
        df2 = DataFrame(rows, columns=[c["name"] for c in columns])

        for row in df2.itertuples():
            localizacion = row[1]
            aula = row[2]

            if id == "calidad_aire":
                df_diff = concat(
                    [
                        df[(df["Localización"] == localizacion)
                            & (df["Aula"] == aula)],
                        df2[
                            (df2["Localización"] == localizacion)
                            & (df2["Aula"] == aula)
                        ],
                    ]
                ).drop_duplicates(keep=False)
            if id == "eficiencia_electrica":
                df_diff = concat(
                    [
                        df[df["Localización"] == localizacion],
                        df2[df2["Localización"] == localizacion],
                    ]
                ).drop_duplicates(keep=False)

            if df_diff.shape[0] > 0:     
                df2.to_csv(f"Data/valores_alertas/{id}.csv")
                return [False, 0]

        return [True, 0]

    return layout
