from datetime import datetime

from pandas import read_csv

from components.templates import boton, div_comun

color_boton = "#add8e6"
s_sin_datos = {
    "float": "left",
    "marginLeft": "2%",
    "color": "#FE0804",
    "margin-right": "3px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
}
s_aviso = {
    "marginLeft": "2%",
    "marginTop": "5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "color": "red",
}
s_titulo_alerta = {
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.5rem",
    "text-align": "center",
}
s_boton = {
    "marginLeft": "2%",
    "width": "95%",
    "height": "95%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1rem",
    "border": f"0.1px solid {color_boton}",
}


def alertas_tiempo(id, df, lista_dispositivos, obj_tiempo):

    dispositivos_a_borrar = []

    for i in range(len(df)):
        aviso = ""
        if id == "calidad_aire":
            dispositivo = lista_dispositivos[df.iloc[i]["description"]][0]
            dispositivo_id = lista_dispositivos[df.iloc[i]["description"]][1]
        if id == "electrica":
            dispositivo =\
                dispositivo_id = lista_dispositivos[df.iloc[i]["description"]]

        periodo = datetime.now() - df.iloc[i]["time_instant"]
        dias = periodo.days
        horas = int(periodo.total_seconds() / 3600)
        if dias > 30:
            meses = int(dias / 30)
            aviso = f"for {periodo} mes"
            if meses > 1:
                aviso = f" por {meses} meses"
        elif dias > 0:
            aviso = f" por {dias} día"
            if dias > 1:
                aviso = f"por {dias} días"
        elif dias == 0 and horas > 6:
            horas = int(periodo.total_seconds() / 3600)
            aviso = f"por {horas} horas"

        if aviso != "":
            dispositivos_a_borrar.append(i)
            obj_tiempo.append(
                div_comun(
                    {"marginTop": "10px"},
                    "",
                    [
                        boton(
                            f"alerta_dispositivo_{dispositivo_id}_{id}",
                            dispositivo,
                            s_boton,
                        ),
                        div_comun(
                            {"marginLeft": "6px", "marginTop": "0px"},
                            "",
                            [
                                div_comun(s_sin_datos, "", "Sin Datos "),
                                div_comun(s_aviso, "", aviso),
                            ],
                        ),
                    ],
                )
            )
    df = df.drop(dispositivos_a_borrar, axis=0)

    return df, obj_tiempo


def alertas_valores(id, df, lista_dispositivos, obj_valores, obj_no_alerta):

    lista_de_alertas = []
    for i in range(len(df)):
        aviso = []
        if id == "calidad_aire":
            dispositivo_id = lista_dispositivos[df.iloc[i]["description"]][1]
            dispositivo = lista_dispositivos[df.iloc[i]["description"]][0]
            aula = dispositivo.split(" ")[-1]
            dispositivo_aux = dispositivo.replace(aula, "").strip()

            df_valores_alertas =\
                read_csv("Data/valores_alertas/calidad_aire.csv")

            valores = df_valores_alertas[
                (df_valores_alertas["Localización"] == dispositivo_aux)
                & (df_valores_alertas["Aula"] == aula)
            ]

            if valores.shape[0] > 0:
                if df.iloc[i]["Co2"] > valores.iloc[0]["Co2 Max (ppm)"]:
                    aviso.append(
                        div_comun(
                            s_aviso,
                            "",
                            f"""Co2 por encima de {
                                    valores.iloc[0]['Co2 Max (ppm)']} ppm""",
                        )
                    )
                    lista_de_alertas.append(
                        [
                            "Co2",
                            dispositivo,
                            f"""Alcanza {df.iloc[i]['Co2']}ppm. Límite en {
                            valores.iloc[0]['Co2 Max (ppm)']} ppm""",
                        ]
                    )
                if (df.iloc[i]["Temperatura"] <
                        valores.iloc[0]["Temp Min (ºC)"]):
                    aviso.append(
                        div_comun(
                            s_aviso,
                            "",
                            f"""Temperatura por debajo de {
                                    valores.iloc[0]['Temp Min (ºC)']} ºC""",
                        )
                    )
                    lista_de_alertas.append(
                        [
                            "Temperatura",
                            dispositivo,
                            f"""Alcanza {
                            df.iloc[i]['Temperatura']}ºC. Temp. Mínima: {
                                valores.iloc[0]['Temp Min (ºC)']}ºC""",
                        ]
                    )
                if (df.iloc[i]["Temperatura"] >
                        valores.iloc[0]["Temp Max (ºC)"]):
                    aviso.append(
                        div_comun(
                            s_aviso,
                            "",
                            f"""Temperatura por encima de {
                                    valores.iloc[0]['Temp Max (ºC)']} ºC""",
                        )
                    )
                    lista_de_alertas.append(
                        [
                            "Temperatura",
                            dispositivo,
                            f"""Alcanza {
                            df.iloc[i]['Temperatura']}ºC. Temp. Máxima: {
                                valores.iloc[0]['Temp Max (ºC)']}ºC""",
                        ]
                    )
                if (df.iloc[i]["Humedad Relativa"] <
                        valores.iloc[0]["Humedad Min (%H)"]):
                    aviso.append(
                        div_comun(
                            s_aviso,
                            "",
                            f"""Humedad por debajo de {
                                valores.iloc[0]['Humedad Min (%H)']} %H""",
                        )
                    )
                    lista_de_alertas.append(
                        [
                            "Humedad Relativa",
                            dispositivo,
                            f"""Alcanza {
                            df.iloc[i]['Humedad Relativa']}%H.  Mínimo: {
                                valores.iloc[0]['Humedad Min (%H)']}%H""",
                        ]
                    )
                if (df.iloc[i]["Humedad Relativa"] >
                        valores.iloc[0]["Humedad Max (%H)"]):
                    aviso.append(
                        div_comun(
                            s_aviso,
                            "",
                            f"""Humedad por encima de {
                                valores.iloc[0]['Humedad Max (%H)']} %H""",
                        )
                    )
                    lista_de_alertas.append(
                        [
                            "Humedad Relativa",
                            dispositivo,
                            f"""Alcanza {
                            df.iloc[i]['Humedad Relativa']}%H.  Máximo: {
                                valores.iloc[0]['Humedad Max (%H)']}%H""",
                        ]
                    )

        if id == "electrica":
            dispositivo_id = lista_dispositivos[df.iloc[i]["description"]]
            dispositivo = dispositivo_id
            valores = read_csv("Data/valores_alertas/eficiencia_electrica.csv")

            if df.iloc[i]["energia"] > valores.iloc[0]["Max"]:
                aviso.append(
                    div_comun(
                        s_aviso,
                        "",
                        f"""Consumo de energía por encima de {
                                valores.iloc[0]['Max']} KW/h""",
                    )
                )
                lista_de_alertas.append(
                    [
                        "Consumo Energía",
                        dispositivo,
                        f"""Alcanza {df.iloc[i]['energia']}KW/h.  Máximo: {
                        valores.iloc[0]['Max']}KW/h""",
                    ]
                )

        if len(aviso) > 0:
            obj_valores.append(
                div_comun(
                    {"marginTop": "10px"},
                    "",
                    [
                        boton(
                            f"alerta_dispositivo_{dispositivo_id}_{id}",
                            dispositivo,
                            s_boton,
                        ),
                        div_comun({"marginLeft": "6px", "marginTop": "5px"},
                                  "",
                                  aviso),
                    ],
                )
            )
        else:
            obj_no_alerta.append(
                div_comun(
                    {"marginTop": "10px"},
                    "",
                    boton(
                        f"alerta_dispositivo_{dispositivo_id}_{id}",
                        dispositivo,
                        s_boton,
                    ),
                )
            )

    return obj_valores, obj_no_alerta, lista_de_alertas
