# Caso de uso Gemelo digital para la mejora de la eficiencia energética

## Introducción

En este caso de uso, se ha pretendido lograr diferentes objetivos con los que obtener una gestión eficiente del consumo energético: Dentro de los objetivos que contemplamos en esta gestión eficiente están la reducción del consumo, el ahorro en el coste energético, la reducción de huella de CO2 y el uso de energías renovables y la simulación de resultados mediante los modelos de Machine Learning alojados en IAaaS.


## Despliegue


## Funcionamiento

#### Interfaz para monitorización

Los paneles de monitorización de eficiencia energética se dividen en cuatro bloques: 
Eficiencia Solar, Eficiencia Energética, Calidad del Aire y Clima.

__Eficiencia Solar.__
Para monitorizar la producción de energía solar tenemos sensores en cuatro granjas de placas solares. Estas granjas están ubicadas en el Centro de investigación multidisciplinar Pleiades, en la granja veterinaria, en el campus de Ciencias de la Salud y en el campus de Espinardo 
Se dispone la información del estado de placas solares en dos paneles, uno de información general (Monitorización Solar) y otro en el que podemos ver esta información para cada instalación de paneles (Monitorización Solar por localización).

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/eficiencia_solar_monitorizacion

En Monitorización Solar podemos ver los siguientes gráficos:
* Energía producida en esta semana:  En este gráfico se monitoriza la energía producida por todas las instalaciones durante la semana en curso (Ilustración 1).
* Energía producida en el mes seleccionado: En este caso se monitoriza la energía producida por todas las instalaciones durante el mes seleccionado por días (Ilustración 2) o por tramo horario (Ilustración 3)

Ilustración 1
![image.png](assets/imagenes/image.png)

Ilustración 2
![image2.png](assets/imagenes/image2.png)

Ilustración 3 
![image3.png](assets/imagenes/image3.png)

En Monitorización Solar por localización podemos ver los mismos gráficos que en el panel anterior pero con los valores de un dispositivo que sera seleccionado por el usuario (Ilustraciones 4-6)

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/eficiencia_solar_monitorizacion_por_localizacion

Ilustración 4 
![image4.png](assets/imagenes/image4.png)
Ilustración 5
![image5.png](assets/imagenes/image5.png)
Ilustración 6
![image6.png](assets/imagenes/image6.png)


__Eficiencia Eléctrica__
Para la monitorización de la energía consumida se han colocado sensores en tres facultades: facultad de química, facultad de veterinaria y facultad de psicología.

De nuevo la información viene separada en dos paneles, uno con información general y otro con información detallada por localización.
Tenemos los mismos gráficos que en Eficiencia Solar pero, en este caso, referidos a la energía consumida.

Al panel de energía consumida accedemos accedemos a traves de la ruta:

http://localhost:8050/dash/eficiencia_electrica_monitorizacion

Y al panel energía consumida por localización a través de la ruta:

http://localhost:8050/dash/eficiencia_electrica_monitorizacion_por_localizacion


__Se incluyen dos paneles auxiliares.__ para poder monitorizar y configurar alertas para el consumo de energía.

Al panel para configurar las alertas consumo (Ilustración 7) accederemos a través de la siguiente ruta

http://localhost:8050/dash/eficiencia_electrica_actualizar_alertas

Al panel para monitorizar las alertas de consumo (Ilustración 8) accederemos a través de la siguiente ruta

http://localhost:8050/dash/eficiencia_electrica_alertas

Ilustración 7
![image7.png](assets/imagenes/image7.png)

Ilustración 8
![image8.png](assets/imagenes/image8.png)

__Calidad del Aire__

Los datos para la calidad del aire se obtienen de sensores ubicados en diferentes edificios (Aulario General, Aulario Norte, Biblioteca Antonio Nebrija, Biblioteca general, Campus Lorca edificio B, Campus Lorca edificio C, Edificio D, Edificio Rector Sabater, Edificio Saavedra Fajardo, Facultad de Bellas Artes, Facultad de Biología, Facultad de Ciencias Deporte, Facultad de Económicas, Facultad de Matemáticas, Facultad de Óptica, Facultad de Química, Facultad de Veterinaria)

Aquí también tenemos la información separada en dos paneles, uno con información general (Monitorización calidad del aire) y otro con información detallada por localización (Monitorización calidad del aire por localización).

__En Monitorización calidad del aire__ tenemos una tabla (Ilustración 9) con los datos de temperatura, humedad relativa, Presión atmosférica y CO2 en el interior de las instalaciones y cuando fueron recogidos esos datos.
También se muestra un mapa con la localización de las instalaciones recogidas en la tabla anterior 

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/calidad_aire_monitorizacion

Ilustración 9
![image9.png](assets/imagenes/image9.png)

__En Monitorización calidad del aire por localización__ tenemos diferentes gráficos para monitorizar la temperatura, la humedad y el CO2 en la instalación que seleccionemos (Ilustración 10).

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/calidad_aire_monitorizacion_por_localizacion

Ilustración 10 
![image10.png](assets/imagenes/image10.png)

__Se incluyen dos paneles auxiliares.__ para poder monitorizar y configurar alertas para la calidad del aire.

Al panel para configurar las alertas de calidad del aire (Ilustración 11) accederemos a través de la siguiente ruta

http://localhost:8050/dash/calidad_aire_actualizar_alertas

Al panel para monitorizar las alertas de consumo (Ilustración 12) accederemos a través de la siguiente ruta

http://localhost:8050/dash/calidad_aire_alertas

Ilustración 11
![image11.png](assets/imagenes/image11.png)

Ilustración 12
![image12.png](assets/imagenes/image12.png)

__Clima__
Los datos referidos al clima se obtienen desde tres observatorios: Edificio LAIB/Departamental,
Aulario de la Merced y Centro de Investigación de Carácter Mixto. CIAVyS-VITALYS.
En este caso tenemos la información en un solo panel: 

A este panel accedemos a traves de la ruta

http://localhost:8050/dash/clima_monitorizacion

Ilustración 13
![image13.png](assets/imagenes/image13.png)

Como vemos en la Ilustración 13, podemos elegir el observatorio desde el que recibir los datos y el campo del que queremos esos datos.
En el panel se mostrará el valor actual y un gráfico con los valores recibidos en las últimas 12 horas.

#### Interfaz para predicción
En este caso tambien disponemos de 2 paneles.

__Al primer panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_admin

Desde este panel podemos importar los modelos creados en la Iaaas (Ilustración 14)

Ilustracion 14
![image14.png](assets/imagenes/image14.png)

__Al segundo panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_user

Desde este panel podemos monitorizar los modelos que hayamos importado (Ilustración 15)

Ilustración 15
![image15.png](assets/imagenes/image15.png)
 

