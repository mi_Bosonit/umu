from datetime import datetime

from pandas import concat, read_csv, read_sql
from sqlalchemy import create_engine

from configuracion import bbdd, tabla_bbdd


def conection():
    return create_engine(bbdd)


engine = conection()

meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
]


def transform_df_por_tabla_mensual(df):
    def tramo_horario(row):
        if row.hour >= 22:
            return "22-0"
        elif row.hour >= 18:
            return "18-22"
        elif row.hour >= 14:
            return "14-18"
        elif row.hour >= 9:
            return "9-14"
        elif row.hour >= 8:
            return "8-9"
        return "0-8"

    def mes(row):
        if row.month == 1:
            return "Enero"
        elif row.month == 2:
            return "Febrero"
        elif row.month == 3:
            return "Marzo"
        elif row.month == 4:
            return "Abril"
        elif row.month == 5:
            return "Mayo"
        elif row.month == 6:
            return "Junio"
        elif row.month == 7:
            return "Julio"
        elif row.month == 8:
            return "Agosto"
        elif row.month == 9:
            return "Septiembre"
        elif row.month == 10:
            return "Octubre"
        elif row.month == 11:
            return "Noviembre"
        return "Diciembre"

    def potencia(tramo, dia, mes):

        if dia > 5:
            return "P6"
        path = "scripts/energia_electrica/Data/"
        df1 = read_csv(path + "tarifa_por_mes.csv")
        df2 = read_csv(path + "potencia_por_tramo_horario_y_tarifa.csv")
        tarifa = df1[df1["Mes"] == mes]["Tarifa"].iloc[0]
        return df2[df2["Tramo"] == tramo][tarifa].iloc[0]

    df["tramo"] = df["hora"].apply(tramo_horario)
    df["mes"] = df["hora"].apply(mes)
    df["dia_semana"] = df.apply(lambda row: row.hora.weekday() + 1, axis=1)
    df = (
        df[["mes", "dia_semana", "tramo", "hora", "energia"]]
        .groupby(["mes", "dia_semana", "tramo", "hora"])
        .sum()
    )
    df.reset_index(inplace=True)
    df["p"] = df.apply(lambda row: potencia(row.tramo,
                                            row.dia_semana,
                                            row.mes), axis=1)
    df = df.sort_values(by=["hora"], ascending=False)
    return df


def obtener_df_solar(sensor):

    try:
        df = read_csv(
            f"""Data/energia_solar/{sensor}/{
                meses[datetime.now().date().month - 1]}.csv"""
        )
        hora = df.iloc[0]["hora"]
    except Exception:
        hora = f"{datetime.now().year}-{datetime.now().month}-01 00:00:00"

    return read_sql(
        f""" SELECT time_instant at time zone ('cet') as hora,
                    MAX(total_aparent_energy * 0.8) - lag(MAX(total_aparent_energy * 0.8), -1)
                    OVER( ORDER BY time_instant DESC) AS energia,
                    controlled_asset
                    FROM(
                         SELECT date_trunc('hour', time_instant) time_instant,
                                total_aparent_energy,
                                three_phase_aparent_power,
                                controlled_asset
                            FROM  {tabla_bbdd}.solarpanelmonitoring
                            WHERE controlled_asset = '{sensor}'
                        ) AS A
                    WHERE time_instant >= '{hora}'
                    GROUP BY time_instant, controlled_asset
                    ORDER BY time_instant DESC""",
        engine,
    )


def obtener_df_acumulado(sensor):

    try:
        df = read_csv(
            f"""Data/energia_electrica/{sensor}/{
                meses[datetime.now().date().month - 1]}.csv"""
        )
        hora = df.iloc[0]["hora"]
    except Exception:
        hora = f"{datetime.now().year}-{datetime.now().month}-01 00:00:00"

    return read_sql(
        f""" SELECT time_instant at time zone ('cet') as hora,
                    MAX(total_active_energy) - lag( MAX(total_active_energy), -1)
                    OVER( ORDER BY time_instant DESC) AS energia, ocb_id
            FROM(
                SELECT date_trunc( 'hour', time_instant) as time_instant,
                       total_active_energy, ocb_id
                FROM {tabla_bbdd}.buildingmonitoring_powermeter
                WHERE ocb_id = '{sensor}'
                ) AS A
            WHERE time_instant >= '{hora}'
            GROUP BY time_instant, ocb_id
            ORDER BY time_instant DESC """,
        engine,
    )


def actualizar_energia_creada():

    sensores = [
        "building-laib-departamental",
        "building-granja-veterinaria",
        "offstreetparking-campus-espinardo-zona-norte",
        "building-pleiades",
    ]

    for sensor in sensores:
        df_solar = obtener_df_solar(sensor)

        if df_solar.shape[0] > 0:
            df_solar = transform_df_por_tabla_mensual(df_solar)
            df_solar["controlled_asset"] = sensor
            df = read_csv(
                f"""Data/energia_solar/{sensor}/{
                    meses[datetime.now().date().month - 1]}.csv"""
            )
            df = df.drop(["Unnamed: 0"], axis=1)
            df_solar = concat([df_solar[:-1], df])
            df_solar.to_csv(
                f"""Data/energia_solar/{sensor}/{
                    meses[datetime.now().date().month - 1]}.csv"""
            )


def actualizar_energia_acumulado():

    sensores = [
        "device-powermeter-facultad-psicologia",
        "device-powermeter-facultad-veterinaria1",
        "device-powermeter-facultad-veterinaria2",
        "device-powermeter-facultad-quimica",
    ]

    for sensor in sensores:
        df_consumo = obtener_df_acumulado(sensor)
        if df_consumo.shape[0] > 0:
            df_consumo = transform_df_por_tabla_mensual(df_consumo)
            df_consumo["ocb_id"] = sensor
            df = read_csv(
                f"""Data/energia_electrica/{sensor}/{
                    meses[datetime.now().date().month - 1]}.csv"""
            )
            df = df.drop(["Unnamed: 0"], axis=1)
            df_consumo = concat([df_consumo[:-1], df])
            df_consumo.to_csv(
                f"""Data/energia_electrica/{sensor}/{
                    meses[datetime.now().date().month - 1]}.csv"""
            )
