# layout
color_boton = "#add8e6"
s_div_selectores = {
    "height": "8%",
    "width": "98%",
    "marginTop": "1%",
    "marginLeft": "2%",
    "font-size": "1.3vmax",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_selector = {
    "float": "left",
    "marginLeft": "1%",
    "width": "34%",
    "height": "90%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "background": color_boton,
}
s_selector2 = {
    "float": "left",
    "marginLeft": "1%",
    "width": "15%",
    "height": "90%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "background": color_boton,
}

s_div_loading = {
    "marginTop": "4%",
    "marginLeft": "2%",
    "width": "85%",
    "height": "30%"
}
s_loading = {"position": "absolute", "top": "18%", "left": "42%"}

s_panel_graficos = {
    "marginTop": "1%",
    "marginLeft": "2%",
    "width": "98%",
    "height": "80%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}
s_ult_reg = {
    "float": "left",
    "marginLeft": "2%",
    "width": "15%",
    "height": "30%",
    "border-radius": "30px",
    "background": "#add8e6",
}
s_24_horas = {
    "float": "left",
    "marginLeft": "2%",
    "marginTop": "0.5%",
    "width": "70%"
}

# callbacks
s_titulo = {"marginLeft": "6%", "marginTop": "7%", "font-size": "1.9vmax"}
s_hora = {"marginLeft": "24%", "marginTop": "2%", "font-size": "1.7vmax"}
s_valor = {
    "marginLeft": "20%",
    "float": "left",
    "marginTop": "7%",
    "font-size": "2.5vmax",
}
s_unidad = {
    "marginLeft": "2%",
    "float": "left",
    "marginTop": "10%",
    "font-size": "1.9vmax",
}
s_no_hay_registro = {"font-size": "1.6vmax"}
