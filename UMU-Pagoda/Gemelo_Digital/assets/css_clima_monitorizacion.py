# Monitorizacion
color_boton = "#add8e6"
s_panel_general = {
    "width": "98%",
    "height": "95%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}

s_selector = {
    "float": "left",
    "marginLeft": "1%",
    "width": "34%",
    "height": "100%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1rem",
    "background": color_boton,
}
s_panel_grafico = {
    "float": "left",
    "marginTop": "3%",
    "marginLeft": "3.7%",
    "width": "98%",
}
s_panel_mapa = {
    "float": "left",
    "marginTop": "3%",
    "marginLeft": "3.7%",
    "width": "68%",
}

# Monitorizacion callbacks
s_div_grafico = {
    "float": "left",
    "marginLeft": "3%",
    "height": "40%",
    "width": "65%"
}
s_div_actual = {
    "float": "left",
    "marginTop": "0.4%",
    "width": "15%",
    "background": "#add8e6",
    "border-radius": "30px",
}
s_titulo = {
    "marginLeft": "8%",
    "marginTop": "1%",
    "width": "91%",
    "font-size": "2.2vmax",
}
s_valor = {
    "float": "left",
    "marginLeft": "20%",
    "marginTop": "2%",
    "font-size": "2.3vmax",
}
s_unidad = {
    "float": "left",
    "marginLeft": "5%",
    "marginTop": "5.5%",
    "font-size": "1.7vmax",
}
