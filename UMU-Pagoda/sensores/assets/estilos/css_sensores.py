s_panel = {
    "margin-top": "0.4%",
    "margin-left": "2%",
    "width": "97.9%",
    "height": "99%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif"
}

s_titulo = {
    "width": "80%",
    "height": "7%",
    "text-align": "center",
    "padding-top": "2%",
    "font-size": "2vmax"
}

s_subtitulos = {"margin-top": '2%', 'width': '100%', 'font-size': '1.3vmax'}
s_sub1 = {'float': 'left', 'margin-left': '3.5%', 'width': '25vmax'}
s_sub2 = {'float': 'left', 'margin-left': '1.5%', 'width': '13vmax'}
s_sub3 = {'float': 'left', 'margin-left': '3%', 'width': '13vmax'}

div_selector = {
    "margin-top": '3.8%',
    "width": "100%",
    "height": "10vmin"
}

s_selector = {
    "float": "left",
    "margin-left": "3%",
    "margin-top": "0%",
    "width": "25vmax",
    "height": "2.35rem",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1vmax",
    "background": "#3dfbc2"
}

s_selector2 = {
    "float": "left",
    "margin-left": "1.2%",
    "margin-top": "0%",
    "width": "14vmax",
    "height": "2.35rem",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1rem",
    "background": "#3dfbc2"
}

s_selector3 = {
    "float": "left",
    "margin-left": "1.5%",
    "margin-top": "0%",
    "width": "10.7vmax",
    "height": "2.35rem",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px 2px 0px 2px",
    "font-size": "1rem",
    "background": "#3dfbc2"
}

s_sel = {'height': '2.4vmax'}

div_tabla = {
    "width": '100%',
    "height": '80%',
    "margin-top": "0.1%"
}

s_div_tabla = {
    "margin-left": "3%",
    "width": "80%"
}
