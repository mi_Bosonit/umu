from assets.estilos.css_sensores import *
from callbacks.federado_call import *
from components.templates import div_comun, selector

layout = div_comun(s_panel, '',
                   [div_comun(s_titulo, '', 'Monitorización de anomalías en equipos'),
                    div_comun(s_subtitulos, '',
                              div_comun(s_sub1, '', 'Nodos')),
                    div_comun(div_selector, '',
                              div_comun(s_selector, "",
                                        selector("selector_federado",
                                                 s_sel,
                                                 ['Central', 'Mec1', 'Mec2'],
                                                 value='Central',
                                                 placeholder="Seleccione Tabla",
                                                 multi=False
                                                 ))),
                    div_comun(div_tabla, '',
                              div_comun(s_div_tabla, "tabla_federado"))])
