from assets.estilos.css_sensores import *
from callbacks.sensores_call import *
from components.templates import div_comun, selector

layout = div_comun(s_panel, '',
                   [div_comun(s_titulo, '', 'Monitorización de sensores y detección de anomalías'),
                    div_comun(s_subtitulos, '',
                              [div_comun(s_sub1, '', 'Familia de sensores'),
                               div_comun(s_sub2, '', 'Estado del dispositivo'),
                               div_comun(s_sub3, '', 'Día')]
                              ),
                    div_comun(div_selector, '',
                              [div_comun(s_selector, "",
                                         selector("selector_tabla",
                                                  s_sel,
                                                  [],
                                                  value=['Todas'],
                                                  placeholder="Seleccione Tabla",
                                                  multi=True
                                                  ),
                                         ),
                               div_comun(s_selector2, "",
                                         selector("selector_estado",
                                                  s_sel,
                                                  ['Todos', 'OK', 'Fallo'],
                                                  value='Todos',
                                                  placeholder="Seleccione estado dispositivo",
                                                  multi=False
                                                  ),
                                         ),
                               div_comun(s_selector3, "",
                                         selector("selector_dia",
                                                  s_sel,
                                                  [],
                                                  value='',
                                                  placeholder="Seleccione fecha",
                                                  multi=False
                                                  ),
                                         )]
                              ),
                    div_comun(div_tabla, '',
                              [div_comun(s_div_tabla, "tabla_sensores")]
                              )
                    ]
                   )
