import os

from dash import callback
from dash.dependencies import Input, Output, State
from pandas import DataFrame, concat, read_csv

from components.templates import tabla

global lista_de_tablas


@callback([Output('selector_dia', 'options'),
           Output('selector_dia', 'value')],
          Input('url', 'pathname'))
def display_page(pathname):
    lista_dias = os.listdir('data/tablas')
    return [lista_dias, lista_dias[-1]]


@callback([Output('selector_tabla', 'options'),
           Output('selector_tabla', 'value')],
          Input('selector_dia', 'value'),
          State('selector_tabla', 'value'))
def display_page(dia, tabla):

    global lista_de_tablas
    lista_de_tablas = [tabla.replace('.csv', '') for tabla in os.listdir(
        f"data/tablas/{os.listdir('data/tablas')[0]}")]
    lista_de_tablas.remove('sensores.json')
    return [['Todas'] + lista_de_tablas, tabla]


# Tabla
@callback(
    Output("tabla_sensores", "children"),
    [Input('selector_tabla', 'value'),
     Input('selector_estado', 'value'),
     Input('selector_dia', 'value')],
    prevent_initial_call=True,
)
def desplegar_tabla(tablas, estado, dia):

    df = DataFrame(columns=['device', 'status'])
    columnas = [
        dict(id="device", name="Dispositivo"),
        dict(id='status', name='Estado'),
    ]

    if 'Todas' in tablas:
        global lista_de_tablas
        tablas = lista_de_tablas

    if len(tablas) > 1:
        df = DataFrame(columns=['device', 'status', 'tabla'])
        columnas.append(dict(id='tabla', name='Tabla'))

    if dia and tablas:
        for tabla_ in tablas:
            df_temp = read_csv(f'data/tablas/{dia}/{tabla_}.csv')
            if len(tablas) > 1:
                df_temp['tabla'] = tabla_
            df = concat([df, df_temp])

        if estado == 'Fallo':
            df = df[df['status'] == 'Failed']
        if estado == 'OK':
            df = df[df['status'] == 'Passed']

        df['status'] = df['status'].apply(lambda x:
                                          '✔' if x == 'Passed' else '❌')
        df['valores constantes'] = df['valores constantes'].apply(lambda x:
                                                                  'No' if x == 'None' else x)
        df['valores perdidos'] = df['valores perdidos'].fillna(0)
        df['valores esperados'] = df['valores esperados'].fillna(144)
        df['valores perdidos'] = df['valores perdidos'].apply(lambda x: int(x))
        df['valores esperados'] = df['valores esperados'].apply(
            lambda x: int(x))
        df['valores obtenidos'] = df['valores obtenidos'].apply(
            lambda x: int(x))

    tabla_cont = tabla(
        "tabla_monitorizacion_contenedores_loc",
        df,
        columnas,
    )
    return tabla_cont
