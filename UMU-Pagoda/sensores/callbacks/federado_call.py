import requests
from dash import callback, dcc
from dash.dependencies import Input, Output
from pandas import DataFrame, to_datetime

from components.templates import line

global lista_de_tablas


# Tabla
@callback(
    Output("tabla_federado", "children"),
    [Input('selector_federado', 'value'),
     Input('url', 'pathname')],
)
def desplegar_tabla(federado, estado):

    respuesta = 'No hay datos'

    if federado:
        url = ''
        if federado == 'Central':
            url = 'http://federadocentral.pagoda.local:3000/monitoring'
        if federado == 'Mec1':
            url = 'http://federadomec1.pagoda.local:3000/monitoring'
        if federado == 'Mec2':
            url = 'http://federadomec2.pagoda.local:3000/monitoring'

        try:
            a = requests.get(url).json()            
            threshold = a['threshold']
        except:
            return respuesta
                
        df = DataFrame(a['DataFrame'])
        if df.shape[0] > 0: 
            df['time_instant'] = to_datetime(df['time_instant'])
            max = df['mse'].max() + 0.2
            limite_sup = max
            if threshold > max:
                limite_sup = threshold + 0.2
             
            fig = line(df,
                    'time_instant',
                    'mse',
                    threshold,
                    range_y=[0, limite_sup])
            fig.add_hrect(y0=0, y1=threshold, line_width=0,
                        fillcolor="green", opacity=0.3)
            fig.add_hrect(y0=threshold, y1=limite_sup, line_width=0,
                        fillcolor="red", opacity=0.3)
            respuesta = dcc.Graph(figure=fig)

    return respuesta
