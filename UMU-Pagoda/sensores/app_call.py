from dash import callback
from dash.dependencies import Input, Output

import pages
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/sensores":
    pages.sensores.layout,
    f"/{raiz}/federado":
    pages.federado.layout,
}

# Update page content


@callback(Output('page_content', 'children'), Input('url', 'pathname'))
def display_page(pathname):
    pagina = ''
    if pathname in lista_de_paginas:
        pagina = lista_de_paginas.get(pathname)
    return pagina
