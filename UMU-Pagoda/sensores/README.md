# Caso de uso Intervertical

## Introducción

El desarrollo de la aplicación intervertical combina los casos de uso para conformar uno de mayor alcance, lo que sería difícil de conseguir de contemplar éstos únicamente por separado.
En el entorno de un edificio cualquiera de la Universidad, puede considerarse como elementos dependientes la ocupación del edificio y los aparcamientos cercanos y la calidad del aire. Los casos de uso encargados de cada uno podrían combinarse en una intervertical, con el objetivo de modelizar -de manera más precisa- todas las partes intervinientes que tienen como efecto una gestión de ocupación ineficiente y una calidad de aire no adecuada.

El objetivo de este nuevo caso de uso es el de evitar la disminución de la calidad del aire (aumento del CO2) en el interior de edificios teniendo en cuenta el impacto en el gasto energético y su ocupación. Para esto se proponen una serie de modelos anidados, en los que las entradas de unos se componen de las salidas de otros, dotando a este nuevo modelo de la robustez y precisión combinada de cada una de las partes. 

## Despliegue


## Funcionamiento

#### Interfaz para monitorización

__La monitorización de la calidad del aire__ a tiempo real se proporciona mediante un panel al que accederemos a través de la ruta 

http://localhost:8050/dash/monitorizacion_co2

Este panel ofrece una vista general sobre el nivel de co2 registrado en el edificio (Ilustración 1).

Ilustración 1
![image.png](assets/imagenes/image.png)

En el panel se muestra el valor actual de co2 y los valores mínimo y máximo recogidos en las últimas 12 horas.
Se indica también el máximo recomendado en espacios cerrados y el valor al que se ha establecido la alerta en el panel de alertas (Ilustración 2).
También se muestra una gráfica con los valores recogidos en las últimas 12 horas.

__Se incluyen dos paneles auxiliares.__ para poder monitorizar y configurar alertas para la calidad del aire.

Al panel para configurar las alertas (Ilustración 2) accederemos a través de la siguiente ruta

http://localhost:8050/dash/monitorizacion_co2_actualizar_alertas

Al panel para monitorizar las alertas (Ilustración 3) accederemos a través de la siguiente ruta

http://localhost:8050/dash/monitorizacion_co2_alertas

Ilustración 2
![image2.png](assets/imagenes/image2.png)

Ilustración 3
![image3.png](assets/imagenes/image3.png)


__Para la monitorización de la ocupación del edificio__ también tendremos un único panel al que accederemos a traves de la ruta:

http://localhost:8050/dash/monitorizacion_usuarios

En el panel (Ilustración 4) se muestra la capacidad del edificio, el total de personas en el mismo y el nivel de ocupación y aforo disponible.
Además podemos ver una gráfica que muestra la ocupación del edificio por horas en el día actual.

Ilustración 4
![image4.png](assets/imagenes/image4.png)

__Se incluyen dos paneles auxiliares.__ para poder monitorizar y configurar alertas para ocupación del edificio.

Al panel para configurar las alertas (Ilustración 5) accederemos a través de la siguiente ruta

http://localhost:8050/dash/monitorizacion_usuarios_actualizar_alertas

Al panel para monitorizar las alertas (Ilustración 6) accederemos a través de la siguiente ruta

http://localhost:8050/dash/monitorizacion_usuarios_alertas

Ilustración 5
![image5.png](assets/imagenes/image5.png)

Ilustración 6
![image6.png](assets/imagenes/image6.png)



#### Interfaz para predicción
En este caso disponemos de dos paneles: 

__Desde el primer panel__ al que accederemos a través de la ruta

http://localhost:8050/dash/prevision_co2

podemos ver la previsión de ocupación de los aparcamientos cercanos y del edificio de la biblioteca.

Además vemos la previsión para el nivel de co2 en el edificio en función a las previsiones indicadas anteriormente.
En la parte superior podemos ver estas predicciones para dentro de 15 minutos (Ilustración 7.1).

Ilustracion 7.1
![image7.png](assets/imagenes/image7.png)

En la parte inferior podemos ver las predicciones para dentro de 30, 45 o 60 minutos. Estos valores podran ser seleccionados por el usuario (Ilustración 7.2).

Ilustracion 7.2
![image7_2.png](assets/imagenes/image7_2.png)

__Desde el segundo panel__ al que accederemos a través de la ruta

http://localhost:8050/dash/monitorizacion_modelos

podemos acceder a la monitorización del funcionamiento de cada modelo. En cada gráfica se muestran las previsiones realizadas a 15, además de los registros reales obtenidos de la base de datos (Ilustración 8). También tenemos un indicador que marca la precisión del modelo en base al inverso del porcentaje de error medio absoluto.

Ilustracion 8
![image8.png](assets/imagenes/image8.png)
![image9.png](assets/imagenes/image9.png)
![image10.png](assets/imagenes/image10.png)
![image11.png](assets/imagenes/image11.png)

Para el administrador tenemos un tercer panel desde el que podrá reentrenar los modelos en caso de que lo considere necesario.

Ilustracion 12
![image8.png](assets/imagenes/image12.png)