import json
import os
from datetime import datetime, timedelta

import requests
from pandas import read_csv, read_json


def guardar_csv():

    dia = (datetime.now() - timedelta(days=1)).date()
    headers = {
        'tolerance': 0.75,
        'ndays_timerange': 1,
        'verbose': 0
    }
    lista = os.listdir('data/tablas')

    if dia not in lista:

        try:
            url = 'http://iot-db.pagoda.local:3000/api/v1/general/'
            respuesta = requests.get(url, params=headers).json()
            os.mkdir(f'data/tablas/{dia}')
        except Exception:
            return ''

        with open(f'data/tablas/{dia}/sensores.json', 'w') as file:
            json.dump(respuesta, file, indent=4)

        df = read_json(f'data/tablas/{dia}/sensores.json')
        lista_de_tablas = list(df.columns)

        for tabla in lista_de_tablas:
            df1 = df[tabla].dropna().reset_index()
            df1['device'] = df1['index']
            df1['status'] = df1[tabla].apply(lambda x: x[0]['status'])
            df1 = df1[['device', 'status']]
            df1.to_csv(f'data/tablas/{dia}/{tabla}.csv', index=False)

        headers = {
            'tolerance': 0.75,
            'ndays_timerange': 1,
            'verbose': 1
        }
    respuesta = requests.get(url, params=headers).json()

    with open(f'data/tablas/{dia}/valores.json', 'w') as file:
        json.dump(respuesta, file, indent=4)

    df_valores = read_json(f'data/tablas/{dia}/valores.json')
    lista_de_tablas = list(df_valores.columns)

    for tabla in lista_de_tablas:
        df = df_valores[tabla].dropna().reset_index()
        df['valores constantes'] = df[tabla].apply(
            lambda x: x['constant_values'])
        df['valores esperados'] = df[tabla].apply(
            lambda x: x['lost_values'][0]['expected_entries'])
        df['valores obtenidos'] = df[tabla].apply(
            lambda x: x['lost_values'][0]['total_entries'])
        df['valores perdidos'] = df[tabla].apply(
            lambda x: x['lost_values'][0]['missing'])
        df.drop([tabla], axis=1, inplace=True)
        df_temp = read_csv(f'data/tablas/{dia}/{tabla}.csv')
        try:
            df = df_temp.merge(df, left_on='device',
                               right_on='index').drop(['index'], axis=1)
        except:
            df_temp['device'] = df_temp['device'].apply(lambda x: str(x))

            df = df_temp.merge(df, left_on='device',
                               right_on='index').drop(['index'], axis=1)
        df.to_csv(f'data/tablas/{dia}/{tabla}.csv', index=False)
