import dash_bootstrap_components as dbc
import plotly.express as px
from dash import dash_table, dcc, html


def div_comun(style, id='', children='', hidden=False):
    return html.Div(style=style, children=children, id=id, hidden=hidden)


def selector(id, style, options=[], value='', placeholder='', multi=True):
    return dcc.Dropdown(style=style,
                        id=id,
                        options=options,
                        value=value,
                        placeholder=placeholder,
                        multi=multi,
                        maxHeight=100)
# Tablas


def tabla(
    id, df, columnas_tabla
):
    s_table = {
        "box-sizing": "inherit",
        "letter-spacing": "0.0001em",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.2vmax",
    }
    s_data = {
        "width": "33%",
        "padding": "6px",
        "align-items": "center",
        "border-right": "0.1px solid white",
        "border-left": "0.1px solid white",
        "height": "50px",
        "color": "#44535A",
    }
    s_header = {
        "padding": "6px",
        "color": "white",
        "background": "#5134DE",  # 24e0a7",
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "font-size": "1.4vmax",
        "height": "55px",
        "border-botton": "0.1px solid #4189AD",
        "border-right": "0.1px solid #24e0a7",
    }
    style_conditional = [
        {
            "if": {"column_id": ["Dispositivo"], "column_id": ["Dispositivo"]},
            "font-size": "1rem",
            "width": "50%",
        },
        {
            "if": {
                "filter_query":
                    "{{status}} = {}".format('❌'),
                "column_id": ["status"],
            },
            "color": 'red',

        },
        {
            "if": {
                "filter_query":
                    "{{status}} = {}".format('✔'),
                "column_id": ["status"],
            },
            "color": 'green',
        },
    ]

    tooltip_data = [
        {
            'status': {
                'value': """Columnas con valores constantes: {}\n\nRegistros esperados por día: {}\n\nRegistros obtenidos: {}\n\nRegistros nulos: {}"""
                .format(
                    df['valores constantes'].iloc[i],
                    df['valores esperados'].iloc[i],
                    df['valores obtenidos'].iloc[i],
                    df['valores perdidos'].iloc[i]), 'type': 'markdown'}
        } for i, row in enumerate(df.to_dict('records'))
    ]

    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columnas_tabla,
                id=id,
                tooltip_data=tooltip_data,
                page_size=12,
                selected_rows=[],
                style_table=s_table,
                style_data=s_data,
                style_header=s_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=style_conditional,
            )
        ]
    )


def line(df, x, y, y0, range_y=[]):
    fig = px.line(df, x, y, range_y=range_y,)
    fig.update_layout(yaxis_title=y,
                      xaxis_title='',
                      xaxis={'showgrid': False},
                      yaxis={'showgrid': False},
                      plot_bgcolor='#f5fcfa',
                      paper_bgcolor='white',
                      autosize=True,
                      height=400,
                      margin=dict(t=0, l=0, b=0, r=0))
    fig.update_traces(mode='markers + lines',
                      line_color='black',
                      marker=dict(color="#4B4238", size=4))
    fig.add_shape(  # add a horizontal "target" line
        type="line",
        line_color="green",
        line_width=3,
        opacity=0.3,
        line_dash="dot",
        x0=0,
        x1=1,
        xref="paper",
        y0=y0,
        y1=y0,
        yref="y")
    fig.add_annotation(  # add a text callout with arrow
        text="Normal",
        x=0.05,
        y=y0 - 0.05,
        arrowhead=2,
        showarrow=False,
        textangle=0,
        xanchor='center',
        xref="paper",
    )

    return fig
