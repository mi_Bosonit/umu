from datetime import datetime

import plotly.express as px
from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import DataFrame, read_json

from assets.estilos.css_monitorizacion_por_loc import *
from components.funciones.funciones_mapas import mapa
from components.templates import div_comun

global df_map


# Mapa
@callback(
    Output("mapa_monitorizacion_por_loc", "children"),
    [
        Input("selector_localizacion_parking", "value"),
        Input("store", "data")
    ],
    State("store", "data"),
)
def sel_ho(b_selector, b_data, data):

    map = ""

    if b_selector:
        global df_map
        df_map = read_json(data["parking"])
        df_map = df_map[df_map["description"] == b_selector]
        map = dcc.Graph(figure=mapa(df_map, "markers", 300, 13,
                                    "description", 14))
    return map


# Tarta
@callback(
    Output("tarta_monitorizacion_por_loc", "children"),
    Input("mapa_monitorizacion_por_loc", "children"),
    prevent_initial_call=True,
)
def sel_ho(b_mapa):

    tarta = ""

    if b_mapa:
        global df_map
        while True:
            try:
               plazas_totales = df_map.iloc[0]["total_spot_number"]
               plazas_libres = df_map.iloc[0]["available_spot_number"]
               break
            except Exception:
                pass        

        ocupacion = (plazas_totales
                     - plazas_libres) / (plazas_totales) * 100

        df_tarta = DataFrame(
            {
                "Estado": ["Libre", "Ocupado"],
                "Ocupacion": [100 - ocupacion, ocupacion]
            }
        )

        tarta = dcc.Graph(
            figure=px.pie(
                df_tarta,
                values="Ocupacion",
                names=["Libre", "Ocupado"],
                title="",
                color="Estado",
                hole=0.05,
                color_discrete_sequence=["#6EC15F", "#CD774F"],
            ).update_layout(height=280, margin=dict(t=0, l=0, b=0, r=230))
        )
    return tarta


@callback(
    [
        Output("estado", "children"),
        Output("barrera", "children"),
        Output("acceso", "children"),
        Output("miembros", "children"),
        Output("empleados", "children"),
        Output("minusvalidos", "children"),
        Output("ubicacion", "children"),
        Output("direccion", "children"),
        Output("actualizado", "children"),
    ],
    Input("mapa_monitorizacion_por_loc", "children"),
    [
        State("selector_localizacion_parking", "value"),
        State("store", "data")
    ],
    prevent_initial_call=True,
)
def sel_ho(b_mapa, s_selector, data):

    footer = ["", "", ""]
    kpis = ["", "", "", "", "", ""]
    s_divs = [
        s_div_kpi_status,
        s_div_kpi_barrera,
        s_div_kpi_acceso,
        s_div_kpi_miembros,
        s_div_kpi_empleados,
        s_div_kpi_minusvalidos,
    ]
    estados = {
        "open": "Abierto",
        "disabled": "Cerrado",
        True: "Si",
        False: "No"
        }

    if b_mapa:
        df_inf = read_json(data["informacion_parking"])
        df_inf = df_inf[df_inf["description"] == s_selector]
        valores = list(
            df_inf.iloc[0][
                [
                    "status",
                    "barrier_access",
                    "free_access",
                    "for_members",
                    "for_employees",
                    "for_disabled",
                ]
            ]
        )

        # kpis
        for i, valor in enumerate(valores):
            kpi = estados[valor]
            if kpi in ("Abierto", "Si"):
                s_divs[i]["background"] =\
                    "linear-gradient(50deg, #2FAC4E, #96CD8B)"
            else:
                s_divs[i]["background"] =\
                    "linear-gradient(50deg, #EA3E17, #CD9D8B)"
            kpis[i] = div_comun(s_divs[i], "", f"{kpi}")

        # footer
        footer = [
            s_selector,
            df_inf["street_address"].iloc[0],
            str(datetime.fromtimestamp(
                df_inf["time_instant"].iloc[0] / 1000
                )).split("T")[0],
        ]
        
    return kpis + footer
