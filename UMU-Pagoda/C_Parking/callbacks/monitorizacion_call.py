from contextlib import suppress
from datetime import datetime

from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_json, Series

from components.funciones.funciones_mapas import mapa
from components.listas import (
    columnas_monitorizacion_parking,
    style_data_conditional_monitorizacion_parking,
)
from components.templates import tabla


@callback(
    Output("parking_tabla", "children"),
    Input("store", "data"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(selector, data):
    
    df = read_json(data["parking"])  
    df["actualizado"] = df["actualizado"].apply(
        lambda x: datetime.fromtimestamp(x / 1000)
    )
    df["Hora"] = df["actualizado"].apply(lambda x: x.time())
    df["Fecha"] = df["actualizado"].apply(lambda x: x.date())
    df = df.assign(
        porcentaje_ocupacion=lambda x: (
            (x.total_spot_number
             - x.available_spot_number) / (x.total_spot_number)
        )
    )
    df["porcentaje"] = df["porcentaje_ocupacion"].apply(
        lambda x: int(x * 100)
        )
    df["status"] = df["status"].apply(
        lambda x: "Abierto" if x == "open" else "Cerrado"
        )
    df["actualizado"] = df["actualizado"].apply(
        lambda x: " ".join(str(x).split("T"))
        )

    return tabla(
        "tab_parking",
        df,
        columnas_monitorizacion_parking,
        style_data_conditional_monitorizacion_parking,
        13,
    )


@callback(
    [
        Output("parking_mapa", "children"),
        Output("tab_parking", "selected_rows")
    ],
    Input("tab_parking", "selected_rows"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(selected_rows, data):

    df = read_json(data["parking"])
    zoom = 10

    if len(selected_rows) > 0:
        df = df.iloc[selected_rows]

        if len(selected_rows) == 1:
            zoom = 14

    return [
        dcc.Graph(figure=mapa(df, "markers", 450, 13, "description", zoom)),
        selected_rows,
    ]
