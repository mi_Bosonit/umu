import configparser

config = configparser.ConfigParser()
config.read('config.ini')

# BBDD
user = config['BBDD']['USER']
password = config['BBDD']['PASSWORD']
host = config['BBDD']['HOST']
port = config['BBDD']['PORT']
database = config['BBDD']['DATABASE']

bbdd = f'''postgresql://{user}:{password}@{host}:{port}/{database}'''

tabla_bbdd = config['BBDD']['TABLE']

# IAAAS
host = config['IAAAS']['HOST']
URL = f'''http://{host}/api/v1'''

# pagina
raiz = config['PAGINAS']['RAIZ']
