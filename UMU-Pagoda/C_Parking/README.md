# Caso de uso Parkings

## Introducción

Este caso de uso se ha orientado a conseguir una mejora en la movilidad dentro del campus, así como obtener una reducción en las emisiones de CO2, haciendo un uso eficiente de las zonas de aparcamiento.

Para conseguir estos objetivos, se ha hecho uso de la información recogida de las barreras de los parkings que permiten determinar el nivel de ocupación de cada uno de ellos. 

Además de analizar la ocupación en tiempo real, existe la posibilidad de importar modelos desde la Iaaas para realizar y monitorizar predicciones.


## Despliegue


## Funcionamiento

#### Interfaz para monitorización
La monitorización de datos a tiempo real sobre el estado de parkings se proporciona mediante dos paneles de información.

__Al primer panel__ accederemos a través de la ruta 

http://localhost:8050/dash/parking_monitorizacion 

Este panel ofrece una vista general sobre el último estado proporcionado por los sensores en una tabla con la información correspondiente a cada parking (Ilustración 1).  Además, podemos ver un mapa con la ubicación de estos aparcamientos.
En este panel se nos ofrece la posibilidad de seleccionar los parkings que queramos que aparezcan en el mapa.

Ilustración 1
![image.png](assets/imagenes/image.png)

Panel Aparcamientos:
* Localización: Identificador del aparcamiento.
* Estado: Estados actuales del aparcamiento; Abierto o Cerrado.
* Plazas: Número total de plazas del aparcamiento.
* Libres: Total de plazas disponibles en el aparcamiento.
* Porcentaje de ocupación: Indica el porcentaje de ocupación del aparcamiento.
* Última actualización: Último registro en la base de datos

__Al segundo panel__ accederemos a traves de la ruta:

http://localhost:8050/dash/parking_monitorizacion_por_localizacion

Este panel se encarga del detalle para cada parking que podremos seleccionar a través del selector (Ilustración 2).

Ilustración 2
![image2.png](assets/imagenes/image2.png)

Monitorización por Aparcamiento:
* Estado: Estados actuales del aparcamiento; Abierto o Cerrado.
* Barrera : Indica si el aparcamiento dispone o no de barrera.
* Acceso libre: Indica si el acceso es restringido o no.
* Plazas para miembros: Plazas reservadas para miembros de la Universidad.
* Plazas para empleados: Plazas reservadas para empleados de la Universidad.
* Plazas para minusválidos: Plazas reservadas para personas con movilidad reducida.

#### Interfaz para predicción
En este caso tambien disponemos de 2 paneles.

__Al primer panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_admin

Desde este panel podemos importar los modelos creados en la Iaaas (Ilustración 3)

Ilustracion 3
![image3.png](assets/imagenes/image3.png)

__Al segundo panel__ accederemos a través de la ruta

http://localhost:8050/dash/prevision_user

Desde este panel podemos monitorizar los modelos que hayamos importado (Ilustración 4)

Ilustración 4
![image4.png](assets/imagenes/image4.png)


