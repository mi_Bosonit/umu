color_boton = "#add8e6"
s_panel_graficos = {"margin-left": "2%", "margin-top": "2%", "height": "95%"}
s_tabla = {"float": "left", "height": "90%", "width": "70%"}
s_mapa = {
    "float": "left",
    "margin-left": "3%",
    "margin-top": "2%",
    "height": "85%",
    "width": "23%",
}
s_boton_todos = {
    "width": "100%",
    "height": "100%",
    "background": color_boton,
    "padding": "2px 2px 0px 2px",
    "border-radius": "7px 7px 5px 5px",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
    "font-size": "1.5vmax",
    "border": "1px solid #8380F7",
}
