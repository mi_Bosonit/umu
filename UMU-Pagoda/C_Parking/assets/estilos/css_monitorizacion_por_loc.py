# monitorizacion
color_boton = "#add8e6"
s_panel_10 = {"width": "100%", "height": "10%"}
s_panel_40 = {"width": "100%", "height": "40%"}
s_panel_50 = {"width": "100%", "height": "50%"}

s_panel_general = {
    "margin": "2%",
    "width": "96%",
    "height": "95%",
    "font-family": "Roboto, Helvetica, Arial, sans-serif",
}
s_panel_selector = {"width": "100%", "height": "7%"}

s_selector = {
    "width": "40%",
    "height": "100%",
    "border-radius": "7px 7px 5px 5px",
    "padding": "2px",
    "font-size": "1.4vmax",
    "background": color_boton,
}

s_div_plazas = {"margin-left": "67%", "width": "20%", "font-size": "1.5vmax"}

s_div_kpi = {
    "float": "left",
    "width": "15%",
    "font-size": "1.5vmax",
    "text-align": "center",
}

s_div_kpi_der_margen = {
    "float": "left",
    "margin-left": "6%",
    "margin-top": "1%",
    "width": "15%",
    "font-size": "1vmax",
}

s_div_kpi_der = {
    "float": "left",
    "margin-top": "1%",
    "width": "15%",
    "font-size": "1vmax",
}

s_div_res_kpi = {"float": "left", "width": "15%", "font-size": "1.5vmax"}

s_div_res_kpi_margen = {
    "float": "left",
    "margin-left": "3%",
    "width": "15%",
    "font-size": "1.5vmax",
}

s_panel_graficos = {"margin-top": "2%", "width": "100%", "height": "62%"}

s_panel_mapa = {"float": "left", "margin-top": "2%", "width": "44%"}

s_panel_tarta = {"float": "left", "margin-left": "2%", "width": "50%"}

s_titulo_tarta = {
    "margin-top": "2%",
    "height": "10%",
    "font-size": "1.8vmax",
    "text-align": "center",
}

s_tarta = {"margin-top": "1%", "height": "90%", "width": "100%"}

s_panel_footer = {"width": "93%", "margin-top": "1.5%", "height": "4%"}

s_div = {
    "float": "left",
    "width": "33%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "background": "linear-gradient(50deg, #3858B2, #8BA7CD)",
}

s_footer = {
    "float": "left",
    "width": "33%",
    "font-size": "1.5vmax",
    "color": "#3858B2",
    "text-align": "center",
}

# callbacks
s_div_kpi_status = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
}

s_div_kpi_barrera = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "background": "#2FAC4E",
}

s_div_kpi_acceso = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
}

s_div_kpi_miembros = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
}

s_div_kpi_empleados = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
}

s_div_kpi_minusvalidos = {
    "float": "left",
    "width": "97%",
    "font-size": "1.5vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
}
