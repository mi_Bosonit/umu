from dash import callback
from dash.dependencies import Input, Output
from pandas import read_sql

import pages
from components.queries import *
from components.utils import conection
from configuracion import raiz

lista_de_paginas = {
    f"/{raiz}/parking_monitorizacion":
    pages.monitorizacion.layout,
    f"/{raiz}/parking_monitorizacion_por_localizacion":
    pages.monitorizacion_por_localizacion.layout,
    f"/{raiz}/prevision_admin":
    pages.prevision.prevision_admin.layout,
    f"/{raiz}/prevision_user":
    pages.prevision.prevision_user.layout,
}


# Update page content
@callback(Output("page_content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname in lista_de_paginas:
        return lista_de_paginas[pathname]


# Guardar query en Store
@callback(
    [Output("store", "data"),
     Output(f"loading_parking", "children")],
    Input("url", "pathname"),
)
def guardar_datos(pathname):
    data = {}
    engine = conection()
    if pathname in (
            f"/{raiz}/parking_monitorizacion",
            f"/{raiz}/parking_monitorizacion_por_localizacion",
    ):
        data["parking"] = read_sql(parking, engine).to_json(orient="columns")
    if pathname in (f"/{raiz}/parking_monitorizacion_por_localizacion", ):
        data["informacion_parking"] =\
            read_sql(informacion_parking, engine).to_json(orient="columns")
    engine.dispose()
    return [data, ""]
