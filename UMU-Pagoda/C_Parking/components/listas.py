from datetime import datetime, timedelta

from dash import dash_table

percentage = dash_table.FormatTemplate.percentage(2)

meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
]

columnas_monitorizacion_parking = [
    dict(id="description", name="Localizacion"),
    dict(id="status", name="Estado"),
    dict(id="total_spot_number", name="Plazas"),
    dict(id="available_spot_number", name="Libres"),
    dict(
        id="porcentaje_ocupacion",
        name="% de ocupacion",
        type="numeric",
        format=percentage,
    ),
    dict(id="actualizado", name="Última Actualización"),
]

# Nivel de llenado
llenado = [
    {
        "if": {
            "column_id": ["porcentaje_ocupacion"],
            "column_id": ["porcentaje_ocupacion"],
        },
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "textAlign": "right",
    }
]

for x in range(81, 101):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "font-weight": "600",
            "background": """linear-gradient(80deg, #3ed21d 0%,
                          #3ed21d 59%, #e8a809 61%,
                          #e8a809 80%,
                          red {max_bound_percentage}%,
                          white {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )
for x in range(61, 81):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "background": """linear-gradient(80deg,
                          #3ed21d 0%,
                          #3ed21d 59%,
                          #e8a809 61%,
                          #e8a809 {max_bound_percentage}%,
                          white {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )
for x in range(61):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "background": """linear-gradient(80deg,
                              #3ed21d 0%,
                              #3ed21d {max_bound_percentage}%,
                              white {white}%)
                           """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )

# Localizacion
localizacion = [
    {
        "if": {"column_id": ["description"], "column_id": ["description"]},
        "width": "13vmax",
        "min-width": "13vmax",
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "color": "white",
        "background": """linear-gradient(100deg,
                                                  #394F93,
                                                  #052073)
                                  padding-box""",
    }
]

# Fecha
fecha = [
    {
        "if": {
            "filter_query": "{{Hora}} < {}".format(
                (datetime.now() - timedelta(hours=3)).time()
            ),
            "column_id": ["actualizado"],
        },
        "color": "#FDA203",
    },
    {
        "if": {
            "filter_query": "{{Hora}} >= {}".format(
                (datetime.now() - timedelta(hours=3)).time()
            ),
            "column_id": ["actualizado"],
        },
        "color": "#3ed21d",
    },
    {
        "if": {
            "filter_query": "{{Fecha}} != {}".format(datetime.now().date()),
            "column_id": ["actualizado"],
        },
        "color": "#FD0703",
    },
]

# Estado
estado = [
    {
        "if": {
            "filter_query": "{{status}}={}".format("Abierto"),
            "column_id": ["status"],
        },
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "color": "#3ed21d",
    },
    {
        "if": {
            "filter_query": "{{status}} != {}".format("Abierto"),
            "column_id": ["status"],
        },
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "color": "#FD0703",
    },
]

style_data_conditional_monitorizacion_parking =\
     localizacion + llenado + fecha + estado


lista_de_localizaciones = [
    "Aulario Giner de los Ríos",
    "Biblioteca General",
    "Biblioteca General / AURED",
    "Campus de Ciencias de la Salud",
    "Centro Social Universitario",
    "Edificio LAIB/Departamental",
    "Edificio Rector Soler",
    "Edificio Saavedra Fajardo",
    "Facultad de Biología",
    "Facultad de Ciencias del Trabajo",
    "Facultad de Medicina",
    "Facultad de Química",
    "Hospital Clínico Veterinario",
    "Pabellón Docente Virgen de la Arrixaca",
    "Recinto deportivo",
    "Unidad Técnica - habitual",
    "Unidad Técnica - redonda",
]
