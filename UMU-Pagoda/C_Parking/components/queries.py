from configuracion import tabla_bbdd

parking = f"""SELECT distinct on( description) description, status,
                     available_spot_number, total_spot_number,
                     time_instant as actualizado, lat, long
              from {tabla_bbdd}.parkingmonitoring a
              left join {tabla_bbdd}.parking b on a.ocb_id = b.id
              where time_instant > '2021-10-04 00:00:00'
              order by description, time_instant desc"""

informacion_parking = f"""select DISTINCT ON (description) description,
                                 time_instant, status, barrier_access,
                                 free_access, for_members, for_employees,
                                 for_disabled, street_address
                          from {tabla_bbdd}.parking a
                          left join {tabla_bbdd}.parkingmonitoring b
                          on a.id = b.ocb_id
                          where time_instant > '2021-10-04 00:00:00'
                          order by description, time_instant desc"""
