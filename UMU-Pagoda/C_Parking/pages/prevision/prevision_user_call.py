# Dash
import json
from datetime import datetime

from dash import callback, dcc, html
from dash.dependencies import Input, Output
# Python
from pandas import read_csv

# Aplicacion
from components.templates import div_comun, line
from pages.prevision.funciones_modelos import (obtener_historico,
                                               sm_get_pred_supervised)
from pages.prevision.prevision_css import *
from pages.prevision.tabla_modelos import tabla

id_page = "parking_prev_user"

columns = [
    dict(id="Fecha Predicción", name="Fecha Predicción", editable=False),
    dict(id="Variable", name="Variable", editable=False),
    dict(id="Predicción", name="Predicción", editable=False),
    dict(id="Localización", name="Localización", editable=False),
]


@callback(Output(f"selector_modelos_{id_page}", "options"),
          Input("url", "pathname"))
def display_output(b_selector):
    path = f"Data/modelos/tabla_modelos.csv"
    return list(read_csv(path)["Nombre Modelo"])


@callback(
    [
        Output(f"prevision_user_{id_page}", "children"),
        Output(f"fiabilidad_user_{id_page}", "children"),
        Output(f"user_loading_{id_page}", "children"),
    ],
    Input(f"selector_modelos_{id_page}", "value"),
    prevent_initial_call=True,
)
def display_output(b_modelos):
    path = f"Data/modelos/tabla_modelos.csv"
    df_modelos = read_csv(path)[["Nombre Modelo", "Id Modelo"]]
    obj, obj2 = [], []

    if b_modelos:

        id_modelo = list(
            df_modelos[df_modelos["Nombre Modelo"] == b_modelos]["Id Modelo"]
        )[0]

        with open(f"Data/modelos/json/{id_modelo}.json") as file:
            data = json.load(file)

        if data["type"] == "supervised":
            # prevision
            response_df = sm_get_pred_supervised(data, id_modelo)
            obj = tabla(id_page, response_df, columns, [])
            # fiabilidad
            titulo = "Fiabilidad Previsión"
            df_fiab = obtener_historico(id_modelo)
            fig = line(
                df_fiab,
                "Fecha",
                ["Predicción", "Real"],
                title="",
                yaxis_title="",
                xaxis_title="",
                mode="markers + lines",
            )
        else:
            # prevision
            path = f"Data/modelos/previsiones_time_series/{id_modelo}.csv"
            df = read_csv(path)

            if df.shape[0] > 0:
                df_prev = df[df["predicted_date"] > str(datetime.now())]
                titulo = f"""Previsión {
                    df['variable'].iloc[0]} en {df['localizacion'].iloc[0]}"""

                obj = [
                    div_comun(
                        s_panel_titulo,
                        "",
                        html.P(style=s_div_titulo_pred, children=titulo),
                    ),
                    dcc.Graph(
                        figure=line(
                            df_prev,
                            "predicted_date",
                            "predicted_value",
                            title="",
                            yaxis_title="",
                            xaxis_title="",
                            mode="markers + lines",
                        )
                    ),
                ]
                # fiabilidad
                titulo = "Fiabilidad Previsión"
                df_fiab = obtener_historico(id_modelo)
                fig = line(
                    df_fiab,
                    "Fecha",
                    ["Predicción", "Real"],
                    title="",
                    yaxis_title="",
                    xaxis_title="",
                    mode="markers + lines",
                )
            else:
                return ["No hay Previsiones", "", ""]

        obj2 = [
            div_comun(
                s_panel_titulo, "", html.P(style=s_div_titulo_pred,
                                           children=titulo)
            ),
            dcc.Graph(figure=fig),
        ]

    return [obj, obj2, ""]
