import json

from dash import callback
from dash.dependencies import Input, Output
from pandas import read_csv

from pages.prevision.funciones_modelos import (crear_historico_supervisado,
                                               crear_historico_time_series)


@callback(
    Output("interval_modelos_parking", "n_intervals"),
    Input("interval_modelos_parking", "n_intervals"),
    prevent_initial_call=True,
)
def display_page(interval):

    path = "Data/modelos/tabla_modelos.csv"

    for modelo in list(read_csv(path)["Id Modelo"]):

        with open(f"Data/modelos/json/{modelo}.json") as file:
            data = json.load(file)

        if data["type"] == "supervised":
            crear_historico_supervisado(data, modelo)
        else:
            crear_historico_time_series(modelo)
    return 0
