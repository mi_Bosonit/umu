import callbacks.monitorizacion_por_localizacion_call
from assets.estilos.css_monitorizacion_por_loc import *
from components.listas import lista_de_localizaciones
from components.templates import div_comun, selector

layout = [
    div_comun(
        s_panel_general,
        "",
        [
            div_comun(
                s_panel_selector,
                "",
                div_comun(
                    s_selector,
                    "",
                    selector(
                        "selector_localizacion_parking",
                        {},
                        lista_de_localizaciones,
                        value="Biblioteca General",
                        placeholder="Seleccione localización",
                    ),
                ),
            ),
            div_comun(
                s_panel_10,
                "",
                [
                    div_comun(
                        s_panel_40,
                        "",
                        div_comun(s_div_plazas, "", "Plazas")
                    ),
                    div_comun(
                        s_panel_50,
                        "",
                        [
                            div_comun(s_div_kpi, "", "Estado"),
                            div_comun(s_div_kpi, "", "Barrera"),
                            div_comun(s_div_kpi, "", "Acceso Libre"),
                            div_comun(s_div_kpi_der_margen, "",
                                      "Para Miembros"),
                            div_comun(s_div_kpi_der, "", "Para Empleados"),
                            div_comun(s_div_kpi_der, "", "Para Minusválidos"),
                        ],
                    ),
                    div_comun(
                        s_panel_50,
                        "",
                        [
                            div_comun(s_div_res_kpi, "estado"),
                            div_comun(s_div_res_kpi, "barrera"),
                            div_comun(s_div_res_kpi, "acceso"),
                            div_comun(s_div_res_kpi_margen, "miembros"),
                            div_comun(s_div_res_kpi, "empleados"),
                            div_comun(s_div_res_kpi, "minusvalidos"),
                        ],
                    ),
                ],
            ),
            div_comun(
                s_panel_graficos,
                "",
                [
                    div_comun(s_panel_mapa, "mapa_monitorizacion_por_loc"),
                    div_comun(
                        s_panel_tarta,
                        "",
                        [
                            div_comun(s_titulo_tarta, "", "Ocupación"),
                            div_comun(s_tarta, "tarta_monitorizacion_por_loc"),
                        ],
                    ),
                ],
            ),
            div_comun(
                s_panel_footer,
                "",
                [
                    div_comun(
                        s_panel_50,
                        "",
                        [
                            div_comun(s_footer, "", "Ubicación"),
                            div_comun(s_footer, "", "Dirección"),
                            div_comun(s_footer, "", "Actualizado"),
                        ],
                    ),
                    div_comun(
                        s_panel_50,
                        "",
                        [
                            div_comun(s_div, "ubicacion"),
                            div_comun(s_div, "direccion"),
                            div_comun(s_div, "actualizado"),
                        ],
                    ),
                ],
            ),
        ],
    )
]
