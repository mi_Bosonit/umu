import callbacks.monitorizacion_call
from assets.estilos.css_monitorizacion import *
from components.templates import div_comun

layout = [
    div_comun(
        s_panel_graficos,
        "",
        [div_comun(
            s_tabla,
            "parking_tabla"
          ),
         div_comun(
            s_mapa,
            "parking_mapa"
          )
         ],
    )
]
